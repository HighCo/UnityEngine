﻿using UnityEngine;

public class Sound
{
	AudioClip audioClip;

	static Vector3 defaultPosition = Camera.main.transform.position;

	public Sound(string name)
	{
		this.audioClip = Resources.Load<AudioClip>(name);
	}

	public void Play()
	{
		AudioSource.PlayClipAtPoint(this.audioClip, defaultPosition);
	}

	public void Play(Vector3 position)
	{
		AudioSource.PlayClipAtPoint(this.audioClip, position);
	}

	public static implicit operator AudioClip(Sound value) => value.audioClip;
}
