#if Controller
using UnityEngine;

namespace PuzzleCore
{
	public class AnyControllerDevice
	{
		public Vector2 LeftStick
		{
			get
			{
				Vector2 vector = Vector2.zero;
				foreach(ControllerDevice c in ControllerInput.controllers)
					vector += c.LeftStick;
				return vector;
			}
		}

		public Direction LeftStickDirection
		{
			get
			{
				Vector2 vector = LeftStick;
				if(Mathf.Abs(vector.x) >= Mathf.Abs(vector.y))
				{
					if(vector.x < -ControllerDevice.activAxisThreshold) return Direction.Left;
					if(vector.x >  ControllerDevice.activAxisThreshold) return Direction.Right;
				}
				else
				{
					if(vector.y < -ControllerDevice.activAxisThreshold) return Direction.Down;
					if(vector.y >  ControllerDevice.activAxisThreshold) return Direction.Up;
				}
				return Direction.None;
			}
		}

		public bool Action1Down
		{
			get
			{
				foreach(ControllerDevice c in ControllerInput.controllers)
					if(c.Action1Down)
						return true;
				return false;
			}
		}

		public bool Action2Down
		{
			get
			{
				foreach(ControllerDevice c in ControllerInput.controllers)
					if(c.Action2Down)
						return true;
				return false;
			}
		}

		public bool StartDown
		{
			get
			{
				foreach(ControllerDevice c in ControllerInput.controllers)
					if(c.StartDown)
						return true;
				return false;
			}
		}

		public bool BackDown
		{
			get
			{
				foreach(ControllerDevice c in ControllerInput.controllers)
					if(c.BackDown)
						return true;
				return false;
			}
		}
	}
}
#endif
