﻿using System.IO;
using UnityEngine;

namespace PuzzleCore
{
	abstract class EventStreamInput
	{
		protected Engine engine;

		public EventStreamInput()
		{
			engine = GameObject.FindObjectOfType<Engine>();
		}

		public virtual void Clear() { }
		public abstract void Record(BinaryWriter stream);
		public abstract void Playback(EventType type, int index, BinaryReader stream);
		public virtual void Close() { }

		protected void WriteEventHeader(BinaryWriter stream, int frame, EventType type, int index)
		{
			stream.Write(float.MinValue);
			stream.Write((byte)(type + (index << 4)));
		}
	}
}
