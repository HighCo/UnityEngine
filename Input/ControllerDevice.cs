#if Controller
using InControl;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace PuzzleCore
{
	public class ControllerDevice
	{
		int index;
		UnityInputDeviceProfile profile;
		List<TrackedControl> analogControls=new List<TrackedControl>();
		List<TrackedControl> buttonControls=new List<TrackedControl>();
		float lowerDeadZone, upperDeadZone;
		KeyCode[] keyCodes;
		//enum DPadMappingMode {None, AnalogToDigital};
		//DPadMappingMode dpadMappingMode;

		public ControllerDevice(int index)
		{
			this.index = index;
		}

		public int Index {get{ return index; }}

		public void SetKeyControlls(params KeyCode[] keyCodes)
		{
			this.keyCodes=keyCodes;
		}

		public void ConnectJoystick(int index, UnityInputDeviceProfile profile, List<Control> controls)
		{
			this.index = index;
			this.profile = profile;
			lowerDeadZone = profile.LowerDeadZone;
			upperDeadZone = profile.UpperDeadZone;
			//dpadMappingMode = DPadMappingMode.None;

			int streamPosition=0;
			foreach(var map in profile.AnalogMappings)
			{
				if(   ((map.Target == InputControlType.DPadLeft || map.Target == InputControlType.DPadRight || map.Target == InputControlType.DPadUp || map.Target == InputControlType.DPadDown) && (controls.Contains(Control.DPad) || controls.Contains(Control.AnalogLeftStickOrDPad) || controls.Contains(Control.DigitalLeftStickOrDPad)))
				   || ((map.Target == InputControlType.LeftStickX || map.Target == InputControlType.LeftStickY) && (controls.Contains(Control.LeftStick) || controls.Contains(Control.AnalogLeftStickOrDPad) || controls.Contains(Control.DigitalLeftStickOrDPad)))
				   || ((map.Target == InputControlType.RightStickX || map.Target == InputControlType.RightStickY) && controls.Contains(Control.RightStick))
				){
					analogControls.Add(new TrackedControl(){ index=(short)map.Target, streamPosition=(short)(streamPosition++), mapping=map, name = "joystick " + (index+1) + map.Source });
					//bool analogDPad = (map.Target == InputControlType.DPadLeft || map.Target == InputControlType.DPadRight || map.Target == InputControlType.DPadUp || map.Target == InputControlType.DPadDown);
					//analogControls.Add(new TrackedControl(){ index=(short)map.Target, streamPosition=(short)(analogDPad ? -1 : streamPosition++), mapping=map, name = "joystick " + (index+1) + map.Source });
					//if(analogDPad) dpadMappingMode = DPadMappingMode.AnalogToDigital;
				}
			}

			streamPosition=0;
			foreach(var map in profile.ButtonMappings)
			{
				if(	(map.Target == InputControlType.Action1 && controls.Contains(Control.Action1))
					|| (map.Target == InputControlType.Action2 && controls.Contains(Control.Action2))
					|| (map.Target == InputControlType.Action3 && controls.Contains(Control.Action3))
					|| (map.Target == InputControlType.Action4 && controls.Contains(Control.Action4))
					|| (map.Target == InputControlType.Start   && controls.Contains(Control.Start))
					|| (map.Target == InputControlType.Back	&& controls.Contains(Control.Back))
				   || ((map.Target == InputControlType.DPadLeft || map.Target == InputControlType.DPadRight || map.Target == InputControlType.DPadUp || map.Target == InputControlType.DPadDown) && (controls.Contains(Control.DPad) || controls.Contains(Control.AnalogLeftStickOrDPad) || controls.Contains(Control.DigitalLeftStickOrDPad)))
				){
					buttonControls.Add(new TrackedControl(){ index=(short)map.Target, streamPosition=(short)(1 << (streamPosition++)), mapping=map, name = "joystick " + (index+1) + map.Source });
				}
			}
		}

		public void ConnectJoystick(int index, BinaryReader stream)
		{
			this.index = index;

			int analogCount=stream.ReadInt32();
			for(int i=0; i < analogCount; i++)
				analogControls.Add(new TrackedControl(stream));

			int buttonCount=stream.ReadInt32();
			for(int i=0; i < buttonCount; i++)
				buttonControls.Add(new TrackedControl(stream));
		}

		public void WriteAddController(BinaryWriter stream)
		{
			stream.Write(analogControls.Count(a => a.streamPosition > -1));
			foreach(TrackedControl control in analogControls)
				if(control.streamPosition > -1)
					control.Write(stream);

			stream.Write(buttonControls.Count(a => a.streamPosition > -1));
			foreach(TrackedControl control in buttonControls)
				if(control.streamPosition > -1)
					control.Write(stream);
		}

		bool[] buttonIsPressed=new bool[19];
		bool[] buttonWasPressed=new bool[19];
		float[] analog=new float[18];
		//Vector2 dpadVector;

		public bool UpdateFromController()
		{
			bool joystickInput = false;
			bool changed = false;
			foreach(TrackedControl control in analogControls)
			{
				float value = Input.GetAxisRaw(control.name);

				if (!control.mapping.Raw)
				{
					/*
					if (control.mapping.TargetRangeIsNotComplete &&
						Mathf.Abs(value) < Mathf.Epsilon &&
						Analogs[i].UpdateTime < Mathf.Epsilon)
					{
						// Ignore initial input stream for triggers, because they report
						// zero incorrectly until the value changes for the first time.
						// Example: wired Xbox controller on Mac.
						continue;
					}
					*/

					value = Mathf.InverseLerp(lowerDeadZone, upperDeadZone, Mathf.Abs(value)) * Mathf.Sign(value);
					value = control.mapping.MapValue(value);
					//value = SmoothAnalogValue( value, Analogs[i].LastValue, deltaTime );
				}
				if(value != 0) joystickInput=true;

				if(!Mathf.Approximately(analog[control.index], value))
				{
					analog[control.index] = value;
					changed = true;
				}
			}

			foreach(TrackedControl control in buttonControls)
			{
				bool isPressed = Input.GetKey(control.name);
				if(isPressed != buttonIsPressed[control.index])
				{
					if(isPressed) buttonWasPressed[control.index] = true;
					changed = true;
				}
				buttonIsPressed[control.index] = isPressed;
			}

			/*
			if(!joystickInput)
			{
				float x,y;
				if(Input.GetKey(keyCodes[1])) x= 1; else
				if(Input.GetKey(keyCodes[3])) x=-1; else
											  x= 0;
				if(analog[(int)InputControlType.LeftStickX] != x)
				{
					analog[(int)InputControlType.LeftStickX]=x;
					changed=true;
				}

				if(Input.GetKey(keyCodes[0])) y= 1; else
				if(Input.GetKey(keyCodes[2])) y=-1; else
											  y= 0;
				if(analog[(int)InputControlType.LeftStickY] != y)
				{
					analog[(int)InputControlType.LeftStickY]=y;
					changed=true;
				}
			}
			*/
			/*
			if(dpadMappingMode == DPadMappingMode.AnalogToDigital)
			{
				float threshold = .2f;
				buttonIsPressed[(int)InputControlType.DPadLeft]  = analog[(int)InputControlType.DPadRight] < -threshold;
				buttonIsPressed[(int)InputControlType.DPadRight] = analog[(int)InputControlType.DPadRight] >  threshold;
				buttonIsPressed[(int)InputControlType.DPadDown]  = analog[(int)InputControlType.DPadUp]	< -threshold;
				buttonIsPressed[(int)InputControlType.DPadUp]	= analog[(int)InputControlType.DPadUp]	>  threshold;

			}

			if(buttonIsPressed[(int)InputControlType.DPadLeft])  dpadVector.x = -1; else
			if(buttonIsPressed[(int)InputControlType.DPadRight]) dpadVector.x =  1; else
																 dpadVector.x =  0;

			if(buttonIsPressed[(int)InputControlType.DPadDown])  dpadVector.y = -1; else
			if(buttonIsPressed[(int)InputControlType.DPadUp])	dpadVector.y =  1; else
																 dpadVector.y =  0;
			*/
			return changed;
		}

		public void Write(BinaryWriter stream)
		{
			foreach(TrackedControl control in analogControls)
				if(control.streamPosition > -1)
					stream.Write(analog[control.index]);

			short bits=0;
			foreach(TrackedControl control in buttonControls)
				if(control.streamPosition > -1)
					if(buttonIsPressed[control.index])
						bits |= control.streamPosition;
			stream.Write(bits);
		}

		public void Read(BinaryReader stream)
		{
			foreach(TrackedControl control in analogControls)
				analog[control.index] = stream.ReadSingle();

			short bits=stream.ReadInt16();
			foreach(TrackedControl control in buttonControls)
			{
				bool isPressed = (bits & control.streamPosition) != 0;
				if(isPressed && !buttonIsPressed[control.index])
					buttonWasPressed[control.index] = true;
				buttonIsPressed[control.index] = isPressed;
			}

			//dpadVector = new Vector2(analog[(int)InputControlType.DPadLeft] + analog[(int)InputControlType.DPadRight], analog[(int)InputControlType.DPadDown] + analog[(int)InputControlType.DPadUp]);
		}

		public void StartFrame()
		{
			for(int i=0; i<buttonWasPressed.Length; i++)
				buttonWasPressed[i] = false;
		}

		//const float activAxisThreshold=.8f;
		//const float inactiveAxisThreshold=.5f;
		public const float activAxisThreshold=.5f;
		public const float inactiveAxisThreshold=.5f;

		public Direction LeftStickDirection
		{
			get
			{
				Vector2 vector = LeftStick;
				if(Mathf.Abs(vector.x) >= Mathf.Abs(vector.y))
				{
					if(vector.x < -activAxisThreshold) return Direction.Left;
					if(vector.x >  activAxisThreshold) return Direction.Right;
				}
				else
				{
					if(vector.y < -activAxisThreshold) return Direction.Down;
					if(vector.y >  activAxisThreshold) return Direction.Up;
				}
				return Direction.None;
			}
		}

		public Direction LeftStickSecondaryDirection
		{
			get
			{
				Vector2 vector = LeftStick;
				if(Mathf.Abs(vector.x) < Mathf.Abs(vector.y))
				{
					if(vector.x < -activAxisThreshold) return Direction.Left;
					if(vector.x >  activAxisThreshold) return Direction.Right;
				}
				else
				{
					if(vector.y < -activAxisThreshold) return Direction.Down;
					if(vector.y >  activAxisThreshold) return Direction.Up;
				}
				return Direction.None;
			}
		}

		public Vector2 LeftStick {get{ return new Vector2(analog[(int)InputControlType.LeftStickX], analog[(int)InputControlType.LeftStickY]); }}
		public Vector2 RightStick {get{ return new Vector2(analog[(int)InputControlType.RightStickX], analog[(int)InputControlType.RightStickY]); }}
		public Vector2 DPad
		{
			get
			{
				Vector2 dpadVector;
				float threshold = .5f;
				if(buttonIsPressed[(int)InputControlType.DPadLeft]  || analog[(int)InputControlType.DPadRight] < -threshold) dpadVector.x = -1; else
				if(buttonIsPressed[(int)InputControlType.DPadRight] || analog[(int)InputControlType.DPadRight] >  threshold) dpadVector.x =  1; else
																															 dpadVector.x =  0;

				if(buttonIsPressed[(int)InputControlType.DPadDown]  || analog[(int)InputControlType.DPadUp]	< -threshold) dpadVector.y = -1; else
				if(buttonIsPressed[(int)InputControlType.DPadUp]	|| analog[(int)InputControlType.DPadUp]	>  threshold) dpadVector.y =  1; else
																															 dpadVector.y =  0;
				return dpadVector;
			}
		}
		public bool Action1Down {get{ return buttonWasPressed[(int)InputControlType.Action1]; }}
		public bool Action2Down {get{ return buttonWasPressed[(int)InputControlType.Action2]; }}
		public bool Action3Down {get{ return buttonWasPressed[(int)InputControlType.Action3]; }}
		public bool Action4Down {get{ return buttonWasPressed[(int)InputControlType.Action4]; }}
		public bool StartDown   {get{ return buttonWasPressed[(int)InputControlType.Start]; }}
		public bool BackDown	{get{ return buttonWasPressed[(int)InputControlType.Back]; }}
		public bool Action1 {get{ return buttonIsPressed[(int)InputControlType.Action1]; }}
		public bool Action2 {get{ return buttonIsPressed[(int)InputControlType.Action2]; }}
		public bool Action3 {get{ return buttonIsPressed[(int)InputControlType.Action3]; }}
		public bool Action4 {get{ return buttonIsPressed[(int)InputControlType.Action4]; }}
		public bool Start   {get{ return buttonIsPressed[(int)InputControlType.Start]; }}
		public bool Back	{get{ return buttonIsPressed[(int)InputControlType.Back]; }}
	}

	public struct TrackedControl
	{
		public short index;
		public short streamPosition;
		public string name;
		public InputControlMapping mapping;

		public TrackedControl(BinaryReader stream)
		{
			index = stream.ReadInt16();
			streamPosition = stream.ReadInt16();
			name = "";
			mapping = null;
		}

		public void Write(BinaryWriter stream)
		{
			stream.Write(index);
			stream.Write(streamPosition);
		}

		void OnApplicationPause(bool pauseStatus)
		{
			if(pauseStatus) Engine.goToBackgroundTime = System.DateTime.Now;
		}
	}
}
#endif
