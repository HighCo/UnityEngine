#if Controller
using InControl;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace PuzzleCore
{
	class ControllerInput : EventStreamInput
	{
		public static List<ControllerDevice> controllers=new List<ControllerDevice>();
		public static AnyControllerDevice anyController=new AnyControllerDevice();

		List<Control> controls = new List<Control> { Control.LeftStick, Control.Action1, Control.Action2 };

		List<UnityInputDeviceProfile> deviceProfiles = new List<UnityInputDeviceProfile>()
		{
			new Xbox360WinProfile(),
		};
		int connectedJoysticks=0;

		public ControllerInput()
		{
			for(int i=0; i<8; i++)
				controllers.Add(new ControllerDevice(i));
			controllers[0].SetKeyControlls(KeyCode.UpArrow, KeyCode.RightArrow, KeyCode.DownArrow, KeyCode.LeftArrow, KeyCode.Space, KeyCode.LeftAlt);
		}

		public override  void Record(BinaryWriter stream)
		{
			RecordAddController(stream);
			RecordControllers(stream);
		}

		void RecordControllers(BinaryWriter stream)
		{
			foreach (ControllerDevice controller in controllers)
			{
				controller.StartFrame();
				bool changed = controller.UpdateFromController();
				if (changed)
				{
					WriteEventHeader(stream, Engine.frame, EventType.Controller, controller.Index);
					controller.Write(stream);
				}
			}
		}

		void RecordAddController(BinaryWriter stream)
		{
			var names = Input.GetJoystickNames();
			for (int i = connectedJoysticks; i < names.Length; i++)
			{
				string name = names[i];
				if(!name.StartsWith("OpenVR"))
				{
					UnityInputDeviceProfile profile = deviceProfiles.Find(a => a.HasJoystickName(name));
					if (profile == null) profile = deviceProfiles.Find(a => a.HasRegexName(name));
					if (profile == null) profile = new UnityUnknownDeviceProfile(name);
					//var controller = new Controller(i, profile, controls);
					//controllers.Add(controller);
					controllers[i].ConnectJoystick(i, profile, controls);

					WriteEventHeader(stream, Engine.frame, EventType.AddController, i);
					controllers[i].WriteAddController(stream);
				}
			}
			connectedJoysticks = names.Length;
		}

		public override void Playback(EventType type, int index, BinaryReader stream)
		{
			foreach(ControllerDevice c in controllers)
				c.StartFrame();

			switch(type)
			{
				case EventType.AddController:
					if(index >= controllers.Count)
						Debug.LogError(string.Format("AddController event for controller {0}, but only {1} controllers are present", index, controllers.Count));
					controllers[index].ConnectJoystick(index, stream);
					break;
				case EventType.Controller:
					if(index >= controllers.Count)
						Debug.LogError(string.Format("Controller event of controller {0}, but only {1} controllers are present", index, controllers.Count));
					var controller = controllers[index];
					controller.Read(stream);
					break;
			}
		}
	}
}
#endif
