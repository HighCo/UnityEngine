#if VR
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using Valve.VR;

namespace PuzzleCore
{
	class VRInput : EventStreamInput
	{
		public static List<VRDevice> devices = new List<VRDevice>();
		public static VRDevice head = new VRDevice();

		public VRInput()
		{
			devices.Clear();
			for(int i=0; i<2; i++)
				devices.Add(new VRDevice());
		}

		public override void Clear()
		{
			foreach(var device in devices)
				device.prevState = device.state;
		}

		public override void Record(BinaryWriter stream)
		{
			var indices = new[] {
				OpenVR.System.GetTrackedDeviceIndexForControllerRole(ETrackedControllerRole.RightHand),
				OpenVR.System.GetTrackedDeviceIndexForControllerRole(ETrackedControllerRole.LeftHand),
			};

			var compositor = OpenVR.Compositor;
			if (compositor != null)
			{
				var render = SteamVR_Render.instance;
				compositor.GetLastPoses(render.poses, render.gamePoses);
				var poses = render.poses;

				// Write head
				var eyeTransform = new SteamVR_Utils.RigidTransform(poses[OpenVR.k_unTrackedDeviceIndex_Hmd].mDeviceToAbsoluteTracking);
				head.position = eyeTransform.pos;
				head.rotation = eyeTransform.rot;
				WriteEventHeader(stream, Engine.frame, EventType.VRHead, 0);
				WriteTransform(stream, head);

				// Write controllers
				for(int i=0; i<indices.Length; i++)
				{
					var deviceIndex = indices[i];
					var device = devices[i];
					device.active = deviceIndex < poses.Length && poses[deviceIndex].bDeviceIsConnected && poses[deviceIndex].bPoseIsValid;

					if(device.active)
					{
						// Position and rotation
						var rigidTransform = new SteamVR_Utils.RigidTransform(poses[deviceIndex].mDeviceToAbsoluteTracking);
						device.position = rigidTransform.pos;
						device.rotation = rigidTransform.rot;

						// Buttons
						device.device = SteamVR_Controller.Input((int)deviceIndex);
						device.state = device.device.GetState();

						// Write event stream
						WriteEventHeader(stream, Engine.frame, EventType.VRController, i);
						WriteTransform(stream, device);
						WriteButtons(stream, device);
					}
					else
					if(device.prevActive)
					{
						// Deactivate
						WriteEventHeader(stream, Engine.frame, EventType.VRController, i);
						WriteTransform(stream, device);
						WriteButtons(stream, device);
					}
					device.prevActive = device.active;
				}
			}
		}

		public override void Playback(EventType type, int index, BinaryReader stream)
		{
			switch(type)
			{
				case EventType.VRHead:
					ReadTransform(stream, head);
					break;

				case EventType.VRController:
					var device = devices[index];
					device.active = true;
					ReadTransform(stream, device);
					ReadButtons(stream, device);
					break;
			}
		}

		void WriteTransform(BinaryWriter stream, VRDevice device)
		{
			stream.Write(device.position.x);
			stream.Write(device.position.y);
			stream.Write(device.position.z);

			stream.Write(device.rotation.x);
			stream.Write(device.rotation.y);
			stream.Write(device.rotation.z);
			stream.Write(device.rotation.w);
		}

		void WriteButtons(BinaryWriter stream, VRDevice device)
		{
			stream.Write(device.state.ulButtonPressed);
			stream.Write(device.state.ulButtonTouched);
			stream.Write(device.state.rAxis0.x);
			stream.Write(device.state.rAxis0.y);
		}

		void ReadTransform(BinaryReader stream, VRDevice device)
		{
			device.position = new Vector3(stream.ReadSingle(), stream.ReadSingle(), stream.ReadSingle());
			device.rotation = new Quaternion(stream.ReadSingle(), stream.ReadSingle(), stream.ReadSingle(), stream.ReadSingle());
		}

		void ReadButtons(BinaryReader stream, VRDevice device)
		{
			device.state.ulButtonPressed = stream.ReadUInt64();
			device.state.ulButtonTouched = stream.ReadUInt64();
			device.state.rAxis0.x = stream.ReadSingle();
			device.state.rAxis0.y = stream.ReadSingle();
		}
	}
}
#endif
