﻿using System;
using System.IO;
using UnityEngine;

namespace PuzzleCore
{
	abstract class PointerHandler : View
	{
		public virtual void OnPointerEnter() {}
		public virtual void OnPointerExit() {}
		public virtual void OnPointerDown(Vector3 position) {}
		public virtual void OnPointerDrag(Vector3 position) {}
		public virtual void OnPointerUp(Vector3 position) {}
	}

	class PointerInput : EventStreamInput
	{
		public const int buttonCount = 3;

		public static bool controllerConnected;
		public static Quaternion rotation;
		public static Vector3 position;

		public static PointerHandler target;
		public static Vector3 targetPoint;

		static bool[] button = new bool[buttonCount];
		static bool[] prevButton = new bool[buttonCount];

		Ray ray;
		RaycastHit hit;

		public override void Record(BinaryWriter stream)
		{
			bool pointerAvailable = false;

			for(int i = 0; i < buttonCount; i++)
				prevButton[i] = button[i];

			#if Pointer
			OVRInput.Controller controller = OVRInput.GetConnectedControllers () & (OVRInput.Controller.LTrackedRemote | OVRInput.Controller.RTrackedRemote);
			if (controller != OVRInput.Controller.None)
			{
				// Create ray from GearVR Controller
				controller = ((controller & OVRInput.Controller.LTrackedRemote) != OVRInput.Controller.None) ? OVRInput.Controller.LTrackedRemote : OVRInput.Controller.RTrackedRemote;
				rotation = OVRInput.GetLocalControllerRotation(controller);
				position = OVRInput.GetLocalControllerPosition(controller);
				ray = new Ray(position, rotation * Vector3.forward);
				controllerConnected = true;
				pointerAvailable = true;

				button[0] = OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger, controller);
				button[1] = Input.GetKey(KeyCode.Escape);
				button[2] = OVRInput.Get(OVRInput.Button.PrimaryTouchpad, controller);
			}
			#endif

			#if Mouse
			if(!pointerAvailable)
			{
				// Create ray from mouse
				ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				controllerConnected = false;

				for(int i = 0; i < buttonCount; i++)
					button[i] = Input.GetMouseButton(i);
			}
			#endif

			// Raycast
			PointerHandler prevTarget = target;
			Vector3 prevTargetPoint = targetPoint;
			if (Physics.Raycast(ray, out hit))
			{
				target = hit.collider.GetComponentInParent<PointerHandler>();
				targetPoint = hit.point;
			}
			else
			{
				target = null;
				targetPoint = position + rotation * (Vector3.forward * 100);
			}

			// Send pointer events
			if(target != prevTarget)
			{
				try                { prevTarget?.OnPointerExit(); }
				catch(Exception e) { Debug.LogException(e); }

				try                { target?.OnPointerEnter(); }
				catch(Exception e) { Debug.LogException(e); }
			}

			if(target != null)
			{
				if(button[0] && targetPoint != prevTargetPoint)
				{
					try                { target.OnPointerDrag(targetPoint); }
					catch(Exception e) { Debug.LogException(e); }
				}
				if(button[0] && !prevButton[0])
				{
					try                { target.OnPointerDown(targetPoint); }
					catch(Exception e) { Debug.LogException(e); }
				}
				if(!button[0] && prevButton[0])
				{
					try                { target.OnPointerUp(targetPoint); }
					catch(Exception e) { Debug.LogException(e); }
				}
			}
		}

		public override void Playback(EventType type, int index, BinaryReader stream)
		{
		}

		public static bool Trigger => button[0];
		public static bool Back => button[1];
		public static bool TouchpadButton => button[2];

		public static bool TriggerDown => button[0] && !prevButton[0];
		public static bool BackDown => button[1] && !prevButton[1];
		public static bool TouchpadButtonDown => button[2] && !prevButton[2];

		public static void HandleTriggerDown() { prevButton[0] = button[0]; }
		public static void HandleBackDown() { prevButton[1] = button[1]; }
		public static void HandleTouchpadButtonDown() { prevButton[2] = button[2]; }
	}
}
