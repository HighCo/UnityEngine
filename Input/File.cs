using System;
using System.Collections.Generic;
using UnityEngine;

namespace PuzzleCore
{
	public class File
	{
		public static List<string> content = new List<string>();

		public static string ReadAllText(string path)
		{
			if (Engine.instance == null)
				return System.IO.File.ReadAllText(path);

			string content = "";
			if(Engine.instance.writer != null)
			{
				try
				{
					content = System.IO.File.ReadAllText(path);
				}
				catch(Exception e)
				{
					content = "";
					Debug.LogWarning("Failed to load "+path+" "+e);
				}

				Engine.instance.writer.Write(content);

			}
			else
			if(Engine.instance.reader != null)
			{
				content = Engine.instance.reader.ReadString();
			}
			return content;
		}

		public static void WriteAllText(string path, string content)
		{
			System.IO.File.WriteAllText(path, content);
		}

		public static void Delete(string path)
		{
			System.IO.File.Delete(path);
		}
	}
}