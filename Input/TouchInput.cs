#if Touch
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace PuzzleCore
{
	public class TouchItem
	{
		public int id;
		public Vector2 position;
		public Vector2 scrollDelta;
		public TouchPhase phase;
		public override string ToString() { return string.Format("{0} {1} ({2})", phase, position, id); }
	}

	class TouchInput : EventStreamInput
	{
		public static List<TouchItem> touches = new List<TouchItem>();
		RectTransform touchView;
		Vector3 prevMousePosition;
		bool mouseDown = false;

		public TouchInput()
		{
			this.mouseDown = false;
			this.prevMousePosition = Vector2.zero;
			var touchViewObject = GameObject.Find("TouchView");
			if(touchViewObject != null)
			{
				this.touchView = touchViewObject.GetComponent<RectTransform>();
				touchViewObject.SetActive(false);
			}
		}

		public override void Clear()
		{
			touches.Clear();
		}

		public override void Record(BinaryWriter stream)
		{
			if (Input.mousePresent)
			{
				RecordMouse(stream);
			}
			else
			{
				RecordTouches(stream);
			}
		}

		void RecordTouches (BinaryWriter stream)
		{
			for (int i = 0; i < Input.touchCount; i++)
			{
				var t = Input.GetTouch(i);
				WriteTouchItem(stream, new TouchItem() { id = i, position = t.position, phase = t.phase});
			}
		}

		void RecordMouse(BinaryWriter stream)
		{
			Vector3 pos = Input.mousePosition;

			if(Input.GetMouseButtonDown(0))
				{ WriteTouchItem(stream, new TouchItem { id=0, position=pos, phase=TouchPhase.Began }); mouseDown = true; }

			if(Input.GetMouseButtonUp(0))
				{ WriteTouchItem(stream, new TouchItem { id=0, position=pos, phase=TouchPhase.Ended }); mouseDown = false; }

			if(pos != prevMousePosition && touches.Count == 0 && mouseDown)
				WriteTouchItem(stream, new TouchItem { id=0, position=pos, phase=TouchPhase.Moved });

			if (!Mathf.Approximately(Input.mouseScrollDelta.sqrMagnitude, 0f) && touches.Count == 0)
			{
				WriteMouseScroll(stream, new TouchItem { id=0, position=pos, phase=TouchPhase.Stationary, scrollDelta = Input.mouseScrollDelta });
			}

			prevMousePosition = pos;
		}

		void WriteTouchItem(BinaryWriter stream, TouchItem touch)
		{
			EventType type;

			switch (touch.phase)
			{
				case TouchPhase.Began:
					type = EventType.TouchBegan;
					break;

				case TouchPhase.Moved:
					type = EventType.TouchMoved;
					break;

				case TouchPhase.Stationary:
					type = (touch.scrollDelta != Vector2.zero) ? EventType.MouseScroll : EventType.TouchMoved;
					break;

				case TouchPhase.Ended:
				case TouchPhase.Canceled:
					type = EventType.TouchEnded;
					break;

				default:
					type = EventType.TouchEnded;
					Debug.LogError("Unsupported touch phase: " + touch.phase);
					break;
			}

			WriteEventHeader(stream, Engine.frame, type, 0);
			stream.Write((byte)touch.id);
			stream.Write((short)(touch.position.x / Screen.width * engine.referenceScreenSize.x));
			stream.Write((short)(touch.position.y / Screen.height * engine.referenceScreenSize.y));

			AddTouch(touch);
		}

		void WriteMouseScroll (BinaryWriter stream, TouchItem touch)
		{
			WriteTouchItem(stream, touch);
			stream.Write((short)(touch.scrollDelta.x / Screen.width * engine.referenceScreenSize.x));
			stream.Write((short)(touch.scrollDelta.y / Screen.height * engine.referenceScreenSize.y));
		}

		void AddTouch (TouchItem touch)
		{
			VisualizeInput(touch);
			touches.Add(touch);
		}

		void ReadTouch(TouchPhase phase, BinaryReader stream)
		{
			var touch = new TouchItem {
				id = stream.ReadByte(),
				position = new Vector2(stream.ReadInt16() / engine.referenceScreenSize.x * Screen.width, stream.ReadInt16() / engine.referenceScreenSize.y * Screen.height),
				phase = phase
			};

			AddTouch(touch);
		}

		void ReadMouseScroll (BinaryReader stream)
		{
			var touch = new TouchItem() {
				id = stream.ReadByte(),
				position = new Vector2(stream.ReadInt16() / engine.referenceScreenSize.x * Screen.width, stream.ReadInt16() / engine.referenceScreenSize.y * Screen.height),
				phase = TouchPhase.Stationary,
				scrollDelta = new Vector2(stream.ReadInt16() / engine.referenceScreenSize.x * Screen.width, stream.ReadInt16() / engine.referenceScreenSize.y * Screen.height)
			};

			AddTouch(touch);
		}

		public override void Playback(EventType type, int index, BinaryReader stream)
		{
			switch(type)
			{
				case EventType.TouchBegan:
					ReadTouch(TouchPhase.Began, stream);
					break;
				case EventType.TouchMoved:
					ReadTouch(TouchPhase.Moved, stream);
					break;
				case EventType.TouchEnded:
					ReadTouch(TouchPhase.Ended, stream);
					break;
				case EventType.MouseScroll:
					ReadMouseScroll(stream);
					break;
			}
		}

		void VisualizeInput (TouchItem touch)
		{
			if (touchView)
			{
				touchView.anchoredPosition = touch.position;
				touchView.gameObject.SetActive(touch.phase == TouchPhase.Began && engine.visualizeInput);
			}
		}
	}
}
#endif
