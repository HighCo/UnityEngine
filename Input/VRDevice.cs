﻿#if VR
using UnityEngine;
using Valve.VR;

namespace PuzzleCore
{
	class VRDevice
	{
		public bool active = false;
		public bool prevActive = false;
		public SteamVR_Controller.Device device;
		public Vector3 position;
		public Quaternion rotation;
		public VRControllerState_t state;
		public VRControllerState_t prevState;

		public bool TriggerTouch { get { return (state.ulButtonTouched & SteamVR_Controller.ButtonMask.Trigger) != 0; } }
		public bool TriggerTouchDown { get { return (state.ulButtonTouched & SteamVR_Controller.ButtonMask.Trigger) != 0 && (prevState.ulButtonTouched & SteamVR_Controller.ButtonMask.Trigger) == 0; } }
		public void HandleTriggerTouchDown() { prevState.ulButtonTouched |= SteamVR_Controller.ButtonMask.Trigger;  }

		public bool TriggerPress { get { return (state.ulButtonPressed & SteamVR_Controller.ButtonMask.Trigger) != 0; } }
		public bool TriggerPressDown { get { return (state.ulButtonPressed & SteamVR_Controller.ButtonMask.Trigger) != 0 && (prevState.ulButtonPressed & SteamVR_Controller.ButtonMask.Trigger) == 0; } }
		public void HandleTriggerPressDown() { prevState.ulButtonPressed |= SteamVR_Controller.ButtonMask.Trigger;  }

		public bool TouchpadTouch { get { return (state.ulButtonTouched & SteamVR_Controller.ButtonMask.Touchpad) != 0; } }
		public bool TouchpadTouchDown { get { return (state.ulButtonTouched & SteamVR_Controller.ButtonMask.Touchpad) != 0 && (prevState.ulButtonTouched & SteamVR_Controller.ButtonMask.Touchpad) == 0; } }
		public void HandleTouchpadTouchDown() { prevState.ulButtonTouched |= SteamVR_Controller.ButtonMask.Touchpad;  }

		public bool TouchpadPress { get { return (state.ulButtonPressed & SteamVR_Controller.ButtonMask.Touchpad) != 0; } }
		public bool TouchpadPressDown { get { return (state.ulButtonPressed & SteamVR_Controller.ButtonMask.Touchpad) != 0 && (prevState.ulButtonPressed & SteamVR_Controller.ButtonMask.Touchpad) == 0; } }
		public void HandleTouchpadPressDown() { prevState.ulButtonPressed |= SteamVR_Controller.ButtonMask.Touchpad;  }

		public bool MenuPress { get { return (state.ulButtonPressed & SteamVR_Controller.ButtonMask.ApplicationMenu) != 0; } }
		public bool MenuPressDown { get { return (state.ulButtonPressed & SteamVR_Controller.ButtonMask.ApplicationMenu) != 0 && (prevState.ulButtonPressed & SteamVR_Controller.ButtonMask.ApplicationMenu) == 0; } }
		public void HandleMenuPressDown() { prevState.ulButtonPressed |= SteamVR_Controller.ButtonMask.ApplicationMenu;  }

		public bool GripPress { get { return (state.ulButtonPressed & SteamVR_Controller.ButtonMask.Grip) != 0; } }
		public bool GripPressDown { get { return (state.ulButtonPressed & SteamVR_Controller.ButtonMask.Grip) != 0 && (prevState.ulButtonPressed & SteamVR_Controller.ButtonMask.Grip) == 0; } }
		public void HandleGripPressDown() { prevState.ulButtonPressed |= SteamVR_Controller.ButtonMask.Grip;  }

		public Vector2 TouchpadPosition { get { return new Vector2(state.rAxis0.x, state.rAxis0.y); } }
		public void TriggerHapticPulse(ushort durationInMicroSeconds = 500) { if(device != null) device.TriggerHapticPulse(durationInMicroSeconds); }
	}
}
#endif
