using System.Collections.Generic;

interface IPoolable
{
	void Clear();
}

class Pool<T> where T:IPoolable, new()
{
	static List<T> items = new List<T>();

	static public T Create()
	{
		if(items.Count > 0) return items.Pop();
		else			    return new T();
	}

	static public void Release(T item)
	{
		item.Clear();
		items.Add(item);
	}
}

class ListPool<T>
{
	static List<List<T>> items = new List<List<T>>();

	static public List<T> Create()
	{
		if(items.Count > 0) return items.Pop();
		else			    return new List<T>();
	}

	static public void Release(List<T> item)
	{
		item.Clear();
		items.Add(item);
	}
}
