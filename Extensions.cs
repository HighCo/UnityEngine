using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class Extensions
{
	/// <summary>
	/// Wraps this object instance into an IEnumerable&lt;T&gt;
	/// consisting of a single item.
	/// </summary>
	/// <typeparam name="T"> Type of the object. </typeparam>
	/// <param name="item"> The instance that will be wrapped. </param>
	/// <returns> An IEnumerable&lt;T&gt; consisting of a single item. </returns>
	public static IEnumerable<T> Yield<T>(this T item)
	{
		if (item == null)
		{
			yield break;
		}
		yield return item;
	}

	public static List<T> Shuffle<T>(this List<T> list)
	{
		int n = list.Count;
		while (n > 1)
		{
			n--;
			int k = UnityEngine.Random.Range(0, n + 1);
			T value = list[k];
			list[k] = list[n];
			list[n] = value;
		}
		return list;
	}

	public static float Range(float value, float min, float max)
	{
		if(value < min) return min;
		if(value > max) return max;
			            return value;
	}

	public static int Range(int value, int min, int max)
	{
		if(value < min) return min;
		if(value > max) return max;
			            return value;
	}

	public static Vector3 Range(Vector3 value, Vector3 min, Vector3 max)
	{
		return new Vector3(
			Range(value.x, min.x, max.x),
			Range(value.y, min.y, max.y),
			Range(value.z, min.z, max.z)
		);
	}

	public static float Map(float value, float sourceMin, float sourceMax, float targetMin, float targetMax)
	{
		float factor = (value - sourceMin) / (sourceMax - sourceMin);
		if(factor < 0) factor = 0; else
		if(factor > 1) factor = 1;
		return targetMin + (targetMax - targetMin) * factor;

	}

	public static bool ContainsAll<T>(this IEnumerable<T> superset, IEnumerable<T> subset)
	{
		foreach(var item in subset)
			if(!superset.Contains(item))
				return false;
		return true;
	}

	public static bool ContainsAny<T>(this IEnumerable<T> superset, IEnumerable<T> subset)
	{
		foreach(var item in subset)
			if(superset.Contains(item))
				return true;
		return false;
	}

	public static T Pop<T>(this List<T> list)
	{
		if(list.Count > 0)
		{
			var item = list[0];
			list.RemoveAt(0);
			return item;
		}
		else
			return default(T);
	}

	public static T Random<T>(this List<T> list)
	{
		if(list.Count > 0) return list[UnityEngine.Random.Range(0,list.Count)];
		else               return default(T);
	}

	public static void Ensure<T>(this List<T> list, T item)
	{
		if(!list.Contains(item))
			list.Add(item);
	}

	public static T[,] Copy<T>(this T[,] source)
	{
		var len0 = source.GetLength(0);
		var len1 = source.GetLength(1);
		var target = new T[len0, len1];
		for(var a=0; a<len0; a++)
			for(var b=0; b<len1; b++)
				target[a,b] = source[a,b];
		return target;
	}

	public static List<T> Set<T>(this List<T> target, IEnumerable<T> source)
	{
		target.Clear();
		target.AddRange(source);
		return target;
	}

	public static T[] Clear<T>(this T[] source)
	{
		for(int i=0; i<source.Length; i++)
			source[i] = default(T);
		return source;
	}

	public static T[,] Clear<T>(this T[,] source)
	{
		var len0 = source.GetLength(0);
		var len1 = source.GetLength(1);
		for(var a=0; a<len0; a++)
			for(var b=0; b<len1; b++)
				source[a,b] = default(T);
		return source;
	}

	public static bool All(this bool[,] source)
	{
		var len0 = source.GetLength(0);
		var len1 = source.GetLength(1);
		for(var a=0; a<len0; a++)
			for(var b=0; b<len1; b++)
				if(!source[a,b])
					return false;
		return true;
	}

	public static TSource MinBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> selector) where TKey:IComparable
	{
		using(var sourceIterator = source.GetEnumerator())
		{
			if(!sourceIterator.MoveNext())
				return default(TSource);
			var min = sourceIterator.Current;
			var minProjected = selector(min);
			while(sourceIterator.MoveNext())
			{
				var candidate = sourceIterator.Current;
				var candidateProjected = selector(candidate);
				if(candidateProjected.CompareTo(minProjected) < 0)
				{
					min = candidate;
					minProjected = candidateProjected;
				}
			}
			return min;
		}
	}

	public static TSource MaxBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> selector) where TKey:IComparable
	{
		using(var sourceIterator = source.GetEnumerator())
		{
			if(!sourceIterator.MoveNext())
				return default(TSource);
			var min = sourceIterator.Current;
			var minProjected = selector(min);
			while(sourceIterator.MoveNext())
			{
				var candidate = sourceIterator.Current;
				var candidateProjected = selector(candidate);
				if(candidateProjected.CompareTo(minProjected) > 0)
				{
					min = candidate;
					minProjected = candidateProjected;
				}
			}
			return min;
		}
	}

	public static IEnumerable<T> SliceRow<T>(this T[,] array, int row)
	{
		for (var i = array.GetLowerBound(0); i <= array.GetUpperBound(0); i++)
		{
			yield return array[i, row];
		}
	}

	public static IEnumerable<T> SliceColumn<T>(this T[,] array, int column)
	{
		for (var i = array.GetLowerBound(1); i <= array.GetUpperBound(1); i++)
		{
			yield return array[column, i];
		}
	}

	public static IEnumerable<T> SliceDimension<T>(this T[,] array, bool horizontal, int index)
	{
		if (horizontal)
		{
			for (var i = array.GetLowerBound(0); i <= array.GetUpperBound(0); i++)
			{
				yield return array[i, index];
			}
		}
		else
		{
			for (var i = array.GetLowerBound(1); i <= array.GetUpperBound(1); i++)
			{
				yield return array[index, i];
			}
		}
	}

	public static IEnumerable<T> Flatten<T>(this T[,] array)
	{
		for (var i = array.GetLowerBound(0); i <= array.GetUpperBound(0); i++)
		{
			for (var j = array.GetLowerBound(1); j <= array.GetUpperBound(1); j++)
			{
				yield return array[i, j];
			}
		}
	}

	public static IEnumerable<T> ToEnumerable<T> (this T[,] array)
	{
		foreach (var item in array)
		{
			yield return item;
		}
	}

	public static List<List<T>> Split<T>(this List<T> list, int chunkLength)
	{
		var listOfLists = new List<List<T>>();

		for (int i = 0; i < list.Count; i += chunkLength)
		{
			listOfLists.Add(list.GetRange(i, Math.Min(chunkLength, list.Count - i)));
		}

		return listOfLists;
	}

	public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
	{
		HashSet<TKey> knownKeys = new HashSet<TKey>();
		foreach (TSource element in source)
			if (knownKeys.Add(keySelector(element)))
				yield return element;
	}

	public static bool IsNullOrEmptyCollection(this ICollection collection)
	{
		return collection.Count == 0;
	}
}

static class EnumUtil
{
	public static T Parse<T>(string value, T defaultValue = default(T), bool ignoreCase = false)
	{
	   if (!Enum.IsDefined(typeof(T), value))
		   return defaultValue;

	   return (T)Enum.Parse(typeof(T), value, ignoreCase);
	}
}

