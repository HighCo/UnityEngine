using UnityEditor;
using UnityEngine;
using System.Collections;
using System.IO;

namespace Library
{
	[CustomEditor(typeof(Engine))]
	public class EngineInspector : Editor
	{
		public override void OnInspectorGUI ()
		{
			base.OnInspectorGUI();
			Engine engine = FindObjectOfType<Engine>() ?? target as Engine;

			if(engine.eventStreamAction == EventStreamAction.Playback)
			{
				var value = EditorGUILayout.TextField("Fast-Forward To", engine.fastforwardTo.ToString());
				float.TryParse(value, out engine.fastforwardTo);

				GUILayout.BeginHorizontal();
				GUILayout.Label("Playback Speed", GUILayout.Width(100));
				GUILayout.Label(engine.timeScale.ToString("0.0"), GUILayout.Width(25));
				engine.timeScale = GUILayout.HorizontalSlider(engine.timeScale, .1f, 10);
				GUILayout.EndHorizontal();
			}

			EventStreamAction prevEventStreamAction = engine.eventStreamAction;
			engine.eventStreamAction = (EventStreamAction) GUILayout.SelectionGrid((int)engine.eventStreamAction, new[]{"Record", "Playback"}, 2);
			if(engine.eventStreamAction != prevEventStreamAction)
			{
				if(engine.eventStreamAction == EventStreamAction.Playback)
				{
					engine.playbackPath = EditorUtility.OpenFilePanel("Select EventStream", Application.persistentDataPath, "eventstream");
					if(string.IsNullOrEmpty(engine.playbackPath)) engine.eventStreamAction = EventStreamAction.Record;
				}
				else
				{
					engine.playbackPath = "";
					engine.timeScale = 1;
					engine.fastforwardTo = 0;
				}
			}
			if(!string.IsNullOrEmpty(engine.playbackPath))
				GUILayout.Label(string.Format("EventStream: {0}", Path.GetFileNameWithoutExtension(engine.playbackPath)));
			if(Engine.time > 0) GUILayout.Label(string.Format("Time: {0}", Engine.time));
		}
	}
}
