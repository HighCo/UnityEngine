﻿using System.Runtime.InteropServices;
using  UnityEngine;
using UnityEditor;

public class OpenEventStreamFinder : ScriptableObject 
{

    [MenuItem("Puzzle Core/EventStream/Open Folder")]
    public static void OpenFinderEventStreamFolder()
    {
        EditorUtility.RevealInFinder(Application.persistentDataPath);
    }
}
