﻿using System;
using UnityEditor;
using UnityEngine;

namespace PuzzleCoreEditor
{
    class EnterStringPopup : PopupWindowContent
    {
	    string newString;
	    string infoLabelText;
	    string okButtonText;


	    bool needsFocus = true;
	    Action<string> callback;

        public EnterStringPopup(Action<string> callback, string defaultString, string infoLabelText, string okButtonText)
        {
            this.callback = callback;
            newString = defaultString;
            this.infoLabelText = infoLabelText;
            this.okButtonText = okButtonText;
        }

        public override Vector2 GetWindowSize()
        {
            return new Vector2(400f, 48f);
        }

        public override void OnGUI(Rect windowRect)
        {
            GUILayout.Space(5f);
            Event current = Event.current;
            bool flag = current.type == EventType.KeyDown && (current.keyCode == KeyCode.Return || current.keyCode == KeyCode.KeypadEnter);
            GUI.SetNextControlName("NewString");
            this.newString = EditorGUILayout.TextField(infoLabelText, this.newString, new GUILayoutOption[0]);
            if (needsFocus)
            {
                this.needsFocus = false;
                EditorGUI.FocusTextInControl("NewString");
            }
            GUI.enabled = this.newString.Length != 0;

            if (!GUILayout.Button(okButtonText) && !flag)
                return;

            callback(this.newString);
            editorWindow.Close();
        }

        public delegate void EnterDelegate(string str);
    }

    class EnterStringAndIntPopup : PopupWindowContent
    {
        string newString;
        int newInt;

        string stringLabelText;
        string okButtonText;
        string intLabelText;

        bool needsFocus = true;
        Action<string, int> callback;

        public EnterStringAndIntPopup(Action<string, int> callback, string defaultString, string stringLabelText, string okButtonText, string intLabelText)
        {
            this.callback = callback;
            this.newString = defaultString;
            this.stringLabelText = stringLabelText;
            this.okButtonText = okButtonText;
            this.intLabelText = intLabelText;
        }

        public override Vector2 GetWindowSize()
        {
            return new Vector2(400f, 48f);
        }

        public override void OnGUI(Rect windowRect)
        {
            GUILayout.Space(5f);
            Event current = Event.current;
            bool flag = current.type == EventType.KeyDown && (current.keyCode == KeyCode.Return || current.keyCode == KeyCode.KeypadEnter);
            GUI.SetNextControlName("NewString");

            EditorGUILayout.BeginHorizontal();

            newString = EditorGUILayout.TextField(stringLabelText, newString);

            EditorGUILayout.LabelField(intLabelText, GUILayout.Width(80));
            newInt = EditorGUILayout.IntField("", newInt, GUILayout.Width(20));

            EditorGUILayout.EndHorizontal();

            if (needsFocus)
            {
                needsFocus = false;
                EditorGUI.FocusTextInControl("NewString");
            }
            GUI.enabled = newString.Length != 0 && newInt != 0;

            if (!GUILayout.Button(okButtonText) && !flag)
                return;

            callback(newString, newInt);
            editorWindow.Close();
        }
    }
}