﻿using System;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

[Serializable]
public class InternalClassWrapper : ISerializationCallbackReceiver
{
    readonly BindingFlags BindingFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static;

    [NonSerialized] public object instance;
    [NonSerialized] public Type classType;
    [SerializeField] string serializedInstance;
    [SerializeField] string serializedClassName;

    bool isStatic = false;
    bool loggingEnabled;


    public void Init(string className, params object[] parameters)
    {
        SetClassType(className);

        if (parameters.Length == 0)
            parameters = new object[] { false };

        if (instance != null)
        {
            Debug.LogWarningFormat("{0} already has an instance!", className);
            return;
        }

        instance = Activator.CreateInstance(classType, parameters);
    }

    public void InitStatic(string className)
    {
        SetClassType(className);
        isStatic = true;
    }

    public void OnBeforeSerialize()
    {
        serializedInstance = JsonUtility.ToJson(instance);
    }

    public void OnAfterDeserialize()
    {
        SetClassType(serializedClassName);
        if(!isStatic)
            instance = JsonUtility.FromJson(serializedInstance, classType);
    }

    void SetClassType(string className)
    {
        Assembly design = typeof(EditorGUILayout).Assembly;
        classType = design.GetType(className);
        serializedClassName = className;
    }

    public void EnableErrorLogging()
    {
        loggingEnabled = true;
    }

    /// <summary>
    /// For example in case of expected null results, disable logging
    /// </summary>
    public void DisableErrorLogging()
    {
        loggingEnabled = false;
    }

    public void InvokeMethod(string methodName, params object[] parameters)
    {
        GetMethodInfo(methodName, parameters).Invoke(instance, parameters);
    }

    public T InvokeMethod<T>(string methodName, params object[] parameters)
    {
        return InvokeMethod<T>(GetMethodInfo(methodName, parameters), parameters);
    }

    public T InvokeMethod<T>(MethodInfo methodInfo, params object[] parameters)
    {
        var result = methodInfo.Invoke(instance, parameters);
        return Cast<T>(result, methodInfo.Name);
    }

    public T GetProperty<T>(string propertyName)
    {
        var result = GetProperty(propertyName).GetValue(instance, null);
        return Cast<T>(result, propertyName);
    }

    public void SetProperty(string propertyName, object val)
    {
        GetProperty(propertyName).SetValue(instance, val, null);
    }

    PropertyInfo GetProperty(string propertyName)
    {
        var propertyInfo = classType.GetProperty(propertyName, BindingFlags);
        if(propertyInfo ==null && loggingEnabled)
            Debug.Log(propertyName + " doesn't exist on " + classType);
        return propertyInfo;
    }


    public object GetField(string fieldName)
    {
        var result = GetFieldInfo(fieldName).GetValue(instance);
        if(result == null && loggingEnabled)
            Debug.Log(fieldName + " is null on " + instance);
        return result;
    }

    public T GetField<T>(string fieldName)
    {
        var result = GetFieldInfo(fieldName).GetValue(instance);
        if(result == null && loggingEnabled)
            Debug.Log(fieldName + " is null on " + instance);
        return Cast<T>(result, fieldName);
    }

    public void SetField(string fieldName, object val)
    {
        GetFieldInfo(fieldName).SetValue(instance, val);
    }

    public FieldInfo GetFieldInfo(string fieldName)
    {
        var fieldInfo = classType.GetField(fieldName, BindingFlags);
        if(fieldInfo ==null && loggingEnabled)
            Debug.Log(fieldName + " doesn't exist on " + classType);
        return fieldInfo;
    }

    public MethodInfo GetMethodInfo(string methodName, params object[] parameters)
    {
        foreach (var parameter in parameters)
        {
            if (parameter == null && loggingEnabled)
            {
                Debug.LogError("Passed null parameter in " + methodName);
                return null;
            }
        }

        var types = GetTypes(parameters);

        var method = types.Length == 0  ? classType.GetMethod(methodName, BindingFlags)
                                    : classType.GetMethod(methodName, BindingFlags, null, GetTypes(parameters), null);

        if(method == null && loggingEnabled)
            Debug.LogError(string.Format("Method {0} with types {1} doesn't exist", methodName, types.Length == 0 ? "none" : types.Select(x => x.ToString()).Aggregate((x, y) => x + ", " + y)));

        return method;
    }

    Type[] GetTypes(object[] parameters)
    {
        var types = new Type[parameters.Length];

        for (var i = 0; i < parameters.Length; i++)
            types[i] = parameters[i].GetType();

        return types;
    }

    T Cast<T>(object obj, string name)
    {
        var resultCasted = (T) obj;
        if (resultCasted == null && loggingEnabled)
            Debug.LogError(string.Format("{0} doesn't return a {1}, it returns a {2}", name, typeof(T), obj.GetType()));

        return resultCasted;
    }


}