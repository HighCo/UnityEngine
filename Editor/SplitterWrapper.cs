﻿using System;
using UnityEngine;

namespace PuzzleCoreEditor
{
    [Serializable]
    public class SplitterWrapper
    {
        public InternalClassWrapper splitterState;

        [SerializeField]
        InternalClassWrapper splitterLayout;

        float[] relativeSizes;
        int[] minSizes;


        public void Init(float[] relativeSizes, int[] minSizes)
        {
            this.relativeSizes = relativeSizes;
            this.minSizes = minSizes;

            splitterState = new InternalClassWrapper();

            splitterLayout = new InternalClassWrapper();
            splitterLayout.InitStatic("UnityEditor.SplitterGUILayout");
        }

        public void RefreshState()
        {
            if (splitterState.instance == null)
                splitterState.Init("UnityEditor.SplitterState", relativeSizes, minSizes, null);
        }

        public void SetHeights(float top, float bottom)
        {
            splitterState.SetField("relativeSizes", new float[] {top, bottom});
        }

        public void EnforceRelativeSizes(int totalSpace)
        {
            splitterState.InvokeMethod("RelativeToRealSizes", totalSpace);
        }

        public void SetMinHeights(int top, int bottom)
        {
            splitterState.SetField("minSizes", new int[] {top, bottom});
        }

        public int[] GetSplittedSizes()
        {
            return splitterState.GetField<int[]>("realSizes");
        }

        public void BeginVertical()
        {
            IgnoreException(() => splitterLayout.InvokeMethod("BeginVerticalSplit", splitterState.instance, new GUILayoutOption[0]));
        }

        public void EndVertical()
        {
            IgnoreException(() => splitterLayout.InvokeMethod("EndVerticalSplit"));
        }

        public void BeginHorizontal()
        {
            IgnoreException(() => splitterLayout.InvokeMethod("BeginHorizontalSplit", splitterState.instance, new GUILayoutOption[0]));
        }

        public void EndHorizontal()
        {
            IgnoreException(() => splitterLayout.InvokeMethod("EndHorizontalSplit"));
        }

        void IgnoreException(Action call)
        {
            try{
                call();
            } catch(Exception){ /*ignored*/ }
        }
    }
}