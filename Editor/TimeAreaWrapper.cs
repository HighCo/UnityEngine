﻿using System;
using System.Reflection;
using UnityEngine;

namespace PuzzleCoreEditor
{
	[Serializable]
	public class TimeAreaWrapper
	{
		public InternalClassWrapper timeArea;

		[SerializeField] float hScaleMin;
		[SerializeField] float hScaleMax;

		Rect visualArea;

		MethodInfo pixelToTime;
		MethodInfo timeToPixel;

		public void RefreshState ()
		{
			timeArea.SetProperty("vRangeMin", 0);

			timeArea.SetProperty("vScaleMin", 1);
			timeArea.SetProperty("vScaleMax", 1);

			// These two aren't serialized in the time area, no idea why
			timeArea.SetProperty("hScaleMin", hScaleMin);
			timeArea.SetProperty("hScaleMax", hScaleMax);

			timeArea.SetProperty("upDirection", 1);
		}

		public void Init (float hRangeMin, float hRangeMax, float hScaleMin, float hScaleMax)
		{
			timeArea = new InternalClassWrapper();

			timeArea.Init("UnityEditor.TimeArea", true);

			timeArea.SetProperty("hRangeLocked", false);
			timeArea.SetProperty("hAllowExceedBaseRangeMin", false);
			timeArea.SetProperty("hSlider", true);
			timeArea.SetProperty("hRangeMin", hRangeMin);
			timeArea.SetProperty("hRangeMax", hRangeMin + hRangeMax);
			timeArea.InvokeMethod("SetShownHRangeInsideMargins", hRangeMin, hRangeMax);

			timeArea.SetProperty("vRangeLocked", false);
			timeArea.SetProperty("vAllowExceedBaseRangeMin", false);
			timeArea.SetProperty("vSlider", true);

			this.hScaleMin = hScaleMin;
			this.hScaleMax = hScaleMax;
		}

		public void SetTransform (Vector2 translation, Vector2 scale)
		{
			timeArea.InvokeMethod("SetTransform", translation, scale);
		}

		/// <summary>
		/// Should be set whenever the actual visual area differs from the TimeArea rect
		/// </summary>
		public void SetVisualArea (Rect rect)
		{
			visualArea = rect;
		}

		public void UpdateAvailableHorizontalRange(float value)
		{
			timeArea.SetProperty("hBaseRangeMax", value);
		}

		public void UpdateAvailableVerticalRange(float value)
		{
			timeArea.SetProperty("vBaseRangeMin", 0);
			timeArea.SetProperty("vBaseRangeMax", value);
			timeArea.SetProperty("vRangeMax", value);
		}

		public void SetRange(float from, float to)
		{
			timeArea.InvokeMethod("SetShownHRange", from, to);
		}

		public float PixelToTime(float pixel)
		{
			if(pixelToTime == null) pixelToTime = timeArea.GetMethodInfo("PixelToTime", pixel, visualArea);
			return timeArea.InvokeMethod<float>(pixelToTime, pixel, visualArea);
		}

		public float TimeToPixel(float time)
		{
			if(timeToPixel == null) timeToPixel = timeArea.GetMethodInfo("TimeToPixel", time, visualArea);
			return timeArea.InvokeMethod<float>(timeToPixel, time, visualArea);
		}

		public void BeginViewGUI ()
		{
			timeArea.InvokeMethod("BeginViewGUI");
		}

		public void EndViewGUI ()
		{
			timeArea.InvokeMethod("EndViewGUI");
		}

		public void EnableSlider ()
		{
			timeArea.InvokeMethod("EndViewGUI");
		}

		public void DrawMajorTicks (Rect rect, float framesPerSecond)
		{
			timeArea.InvokeMethod("DrawMajorTicks", rect, framesPerSecond);
		}

		public void TimeRuler (Rect rect, float framesPerSecond)
		{
			timeArea.InvokeMethod("TimeRuler", rect, framesPerSecond);
		}

		public Rect rect
		{
			get { return timeArea.GetProperty<Rect>("rect"); }
			set { timeArea.SetProperty("rect", value); }
		}

		public Vector2 translation
		{
			get { return timeArea.GetProperty<Vector2>("translation"); }
		}

		public Vector2 scale
		{
			get { return timeArea.GetProperty<Vector2>("scale"); }
		}
	}
}