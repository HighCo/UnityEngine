using UnityEngine;

namespace Library
{
	public class CreateEngine : MonoBehaviour
	{
		void Awake()
		{
			if(!FindObjectOfType<Engine>())
			{
				Instantiate(Resources.Load<GameObject>("Engine"));
			}
		}
	}
}
