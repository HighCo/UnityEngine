using System;
using UnityEngine;
using Object = UnityEngine.Object;

static class Obj
{
	public static T Load<T>(string name) where T:UnityEngine.Object
	{
		return Resources.Load<T>(name);
	}

	public static GameObject Load(string name)
	{
		return Resources.Load<GameObject>(name);
	}

	public static GameObject LoadModel(string name)
	{
		return Resources.Load<GameObject>("Models/"+name);
	}

	public static GameObject CreateGameObject(GameObject prefab, GameObject parent)
	{
		var item = GameObject.Instantiate(prefab) as GameObject;
		item.transform.SetParent(parent.transform, worldPositionStays:false);
		return item;
	}

	public static GameObject CreateGameObject(string prefabPath, GameObject parent)
	{
		try
		{
			var prefab = Resources.Load<GameObject>(prefabPath);
			var item = GameObject.Instantiate(prefab) as GameObject;
			item.transform.SetParent(parent.transform, worldPositionStays:false);
			return item;
		}
		catch(Exception e)
		{
			Debug.LogWarning("Can't open prefab " + prefabPath);
			return null;
		}
	}

	public static GameObject CreateGameObject(GameObject prefab, GameObject parent, Vector3 localPosition, Quaternion localRotation = default(Quaternion))
	{
		var item = GameObject.Instantiate(prefab) as GameObject;
		item.transform.SetParent(parent.transform, worldPositionStays:false);
		item.transform.localPosition = localPosition;
		item.transform.localRotation = localRotation;
		return item;
	}

	public static GameObject CreateGameObject(string prefabPath, GameObject parent, Vector3 localPosition, Quaternion localRotation = default(Quaternion))
	{
		var prefab = Resources.Load<GameObject>(prefabPath);
		var item = GameObject.Instantiate<GameObject>(prefab);
		item.transform.SetParent(parent.transform, worldPositionStays:false);
		item.transform.localPosition = localPosition;
		item.transform.localRotation = localRotation;
		return item;
	}

	public static T Create<T>(T prefab, GameObject parent) where T:MonoBehaviour
	{
		var item = GameObject.Instantiate<T>(prefab);
		item.transform.SetParent(parent.transform, worldPositionStays:false);
		return item;
	}

	public static T Create<T>(T prefab, MonoBehaviour parent) where T:MonoBehaviour
	{
		var item = GameObject.Instantiate<T>(prefab);
		item.transform.SetParent(parent.transform, worldPositionStays:false);
		return item;
	}

	public static T Create<T>(string prefabPath, GameObject parent) where T : MonoBehaviour
	{
		try
		{
			var prefab = Resources.Load<T>(prefabPath);
			var item = GameObject.Instantiate<T>(prefab);
			item.transform.SetParent(parent.transform, worldPositionStays: false);
			return item;
		}
		catch
		{
			Debug.LogError("Failed to load prefab " + prefabPath);
			return null;
		}
	}

	public static T Create<T>(string prefabPath, MonoBehaviour parent) where T : MonoBehaviour
	{
		try
		{
			var prefab = Resources.Load<T>(prefabPath);
			var item = GameObject.Instantiate<T>(prefab);
			item.transform.SetParent(parent.transform, worldPositionStays: false);
			return item;
		}
		catch
		{
			Debug.LogError("Failed to load prefab " + prefabPath);
			return null;
		}
	}

    public static T Create<T>(string prefabPath, GameObject parent, Vector3 localPosition, Quaternion localRotation = default(Quaternion)) where T : MonoBehaviour
    {
        try
        {
            var prefab = Resources.Load<T>(prefabPath);
            var item = GameObject.Instantiate<T>(prefab);
            item.transform.SetParent(parent.transform, worldPositionStays: false);
            item.transform.localPosition = localPosition;
            item.transform.localRotation = localRotation;
            return item;
        }
        catch
        {
            Debug.LogError("Failed to load prefab " + prefabPath);
            return null;
        }
    }

    public static T Create<T>(string prefabPath, MonoBehaviour parent, Vector3 localPosition, Quaternion localRotation = default(Quaternion)) where T : MonoBehaviour
    {
        try
        {
            var prefab = Resources.Load<T>(prefabPath);
            var item = GameObject.Instantiate<T>(prefab);
            item.transform.SetParent(parent.transform, worldPositionStays: false);
            item.transform.localPosition = localPosition;
            item.transform.localRotation = localRotation;
            return item;
        }
        catch
        {
            Debug.LogError("Failed to load prefab " + prefabPath);
            return null;
        }
    }

    public static T Create<T>(string prefabPath, string defaultPrefabPath, GameObject parent, Vector3 localPosition, Quaternion localRotation = default(Quaternion)) where T : MonoBehaviour
    {
        T prefab;
        try
        {
            prefab = Resources.Load<T>(prefabPath);
        }
        catch
        {
            prefab = Resources.Load<T>(defaultPrefabPath);
        }
        var item = GameObject.Instantiate<T>(prefab);
        item.transform.SetParent(parent.transform, worldPositionStays: false);
        item.transform.localPosition = localPosition;
        item.transform.localRotation = localRotation;
        return item;
    }

   public static T Create<T>(string prefabPath, string defaultPrefabPath, MonoBehaviour parent, Vector3 localPosition, Quaternion localRotation = default(Quaternion)) where T : MonoBehaviour
    {
        T prefab;
        try
        {
            prefab = Resources.Load<T>(prefabPath);
        }
        catch
        {
            prefab = Resources.Load<T>(defaultPrefabPath);
        }
        var item = GameObject.Instantiate<T>(prefab);
        item.transform.SetParent(parent.transform, worldPositionStays: false);
        item.transform.localPosition = localPosition;
        item.transform.localRotation = localRotation;
        return item;
    }

    public static T Create<T>(T prefab, GameObject parent, Vector3 localPosition, Quaternion localRotation = default(Quaternion)) where T : MonoBehaviour
    {
        var item = GameObject.Instantiate<T>(prefab);
        item.transform.SetParent(parent.transform, worldPositionStays: false);
        item.transform.localPosition = localPosition;
        item.transform.localRotation = localRotation;
        return item;
    }

    public static T Create<T>(T prefab, MonoBehaviour parent, Vector3 localPosition, Quaternion localRotation = default(Quaternion)) where T : MonoBehaviour
    {
        var item = GameObject.Instantiate<T>(prefab);
        item.transform.SetParent(parent.transform, worldPositionStays: false);
        item.transform.localPosition = localPosition;
        item.transform.localRotation = localRotation;
        return item;
    }

    public static void DestroyChildren(GameObject gameObject)
	{
		foreach(Transform child in gameObject.transform)
			Object.Destroy(child.gameObject);
	}

	public static void DestroyChildren(MonoBehaviour view)
	{
		foreach(Transform child in view.transform)
			Object.Destroy(child.gameObject);
	}

	public static void Destroy(GameObject gameObject)
	{
		Object.Destroy(gameObject);
	}

	public static void Destroy(MonoBehaviour view)
	{
		Object.Destroy(view.gameObject);
	}

    public static void DeleteChildren(GameObject parent)
    {
        foreach (Transform child in parent.transform)
            GameObject.Destroy(child.gameObject);
    }
}
