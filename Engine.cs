using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using InControl;
using UnityEngine.SceneManagement;

namespace Library
{
	public enum Control {None, LeftStick, RightStick, DPad, DigitalLeftStickOrDPad, AnalogLeftStickOrDPad, Action1, Action2, Action3, Action4, LeftTrigger, RightTrigger, LeftBumper, RightBumper, Start, Back };
	public enum EventStreamAction {Record, Playback};
	public enum Direction {None, Left, Right, Up, Down};
	enum EventType {Controller, Acceleration, Key, AddController, TouchDown, TouchMove, TouchUp, TouchCancel, VRController, VRCamera};

	public class Engine : MonoBehaviour
	{
		static public float fixedDeltaTime;
		static public double epochTime;
		static public float time;
		static public int frame = 0;
		static public Engine instance;

		public bool visualizeInput;
		public string parameters;

		[HideInInspector] public string playbackPath;
		[HideInInspector] public EventStreamAction eventStreamAction;
		[HideInInspector] public Vector2 referenceScreenSize;
		[HideInInspector] public float fastforwardTo = 0;
		[HideInInspector] public float timeScale = 1f;

		public static List<IUpdatable> updatables=new List<IUpdatable>();
		public static System.DateTime goToBackgroundTime;

		static public bool isPlayback { get { return instance.eventStreamAction == EventStreamAction.Playback; } }

		public BinaryReader reader;
        public BinaryWriter writer;
		System.DateTime startTime;
		string startScene;
		int exceptionCount=0, warningCount=0;
		bool initialStatup = true;

		float lastTime;
		double epochTimeOffset;
		int formatVersion = 1;
		List<EventStreamInput> inputs;
		#if Touch
			EventStreamInputModule inputModule;
		#endif


		static readonly System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Local);

		public void Awake()
		{
			DontDestroyOnLoad(this);
			instance = this;

			inputs = new List<EventStreamInput>();
			#if Touch
				inputs.Add(new TouchInput());
				inputModule = FindObjectOfType<EventStreamInputModule>();
			#endif
			#if Controller
				inputs.Add(new ControllerInput());
			#endif
			#if VR
				inputs.Add(new VRInput());
			#endif
			StartStream();
		}

		public void CreateNewStream(ref int parameter)
		{
			SaveStream();
			if (!initialStatup || string.IsNullOrEmpty(this.parameters))
				this.parameters = parameter.ToString();
			initialStatup = false;
			StartStream();
			parameter = !string.IsNullOrEmpty(this.parameters) ? int.Parse(this.parameters) : 0;
		}

		void StartStream()
		{
			startTime = System.DateTime.Now;
			if(eventStreamAction == EventStreamAction.Record)
			{
				writer=new BinaryWriter(new MemoryStream());
				int seed = UnityRandomSeed;
				Camera cam = Camera.main;
				referenceScreenSize = new Vector2(cam.pixelWidth, cam.pixelHeight);
				time = Time.time;
				epochTimeOffset = (double)(System.DateTime.Now - epochStart).TotalSeconds - time;
				writer.Write(formatVersion);
				writer.Write(parameters);
				writer.Write(seed);
				writer.Write(referenceScreenSize.x);
				writer.Write(referenceScreenSize.y);
				UnityRandomSeed = seed;
				writer.Write(time);
				writer.Write(epochTimeOffset);
			}
			else
			if(eventStreamAction == EventStreamAction.Playback)
			{
				byte[] buffer = System.IO.File.ReadAllBytes(playbackPath);
				reader = new BinaryReader(new MemoryStream(buffer));
				reader.ReadInt32(); // formatVersion
				parameters = reader.ReadString();
				int seed = reader.ReadInt32();
				referenceScreenSize = new Vector2(reader.ReadSingle(), reader.ReadSingle());
				UnityRandomSeed = seed;
				time = reader.ReadSingle();
				epochTimeOffset = reader.ReadDouble();
			}
			startScene = SceneManager.GetActiveScene().name;
			fixedDeltaTime = 1f / Screen.currentResolution.refreshRate;
			lastTime = time;
			epochTime = time + epochTimeOffset;
		}

#pragma warning disable 618
		int UnityRandomSeed {
			get { return Random.seed; }
			set { Random.seed = value; }
		}
#pragma warning restore 618

		void OnDestroy()
		{
			SaveStream();
		}

		void SaveStream()
		{
			if(writer != null)
			{
				byte[] buffer = (writer.BaseStream as MemoryStream).GetBuffer();
				var timeSpan = System.DateTime.Now - startTime;
				if (buffer.Length > 0 && timeSpan.TotalSeconds > 1.0)
				{
					string path = string.Format("{0}/{1:dd.MM.yyyy HH.mm} ({5}.{6:d2}) {7} {8} {3}{4}V{2}.eventstream", Application.persistentDataPath, startTime, Application.version, exceptionCount > 0 ? exceptionCount + " Exceptions " : "", warningCount > 0 ? warningCount + " Warnings " : "", (int)timeSpan.TotalMinutes, (int)timeSpan.Seconds, startScene, parameters);
					System.IO.File.WriteAllBytes(path, buffer);
				}
			}
			time = 0;
			fixedDeltaTime = 0;
			lastTime = 0;
		}

		bool inputsProcessed = true;

		void ClearInputs()
		{
			if(inputsProcessed)
			{
				foreach(var inputMethod in this.inputs)
					inputMethod.Clear();
				inputsProcessed = false;
			}
		}

		public void Update()
		{
			#if Touch
				ClearInputs();
				if(writer != null) RecordInput(writer);
			#endif

		}

		public void FixedUpdate()
		{
			ClearInputs();
			#if VR
				if(writer != null) RecordInput(writer);
			#endif
			if(writer != null) RecordTime(writer); else
			if(reader != null) Playback(reader); else
				               time = Time.time;

			#if UNITY_EDITOR
				if(time < fastforwardTo) Time.timeScale = 15;
				else                     Time.timeScale = timeScale;
			#endif

			#if Touch
				inputModule.ProcessInput();
			#endif

			frame++;
			fixedDeltaTime = time - lastTime;
			lastTime = time;
			inputsProcessed = true;
			epochTime = time + epochTimeOffset;

			foreach(var updatable in updatables)
				updatable.Update();
		}

		void RecordInput(BinaryWriter stream)
		{
			foreach(var inputMethod in this.inputs)
				inputMethod.Record(stream);
		}

		void RecordTime(BinaryWriter stream)
		{
			time = Time.time;
			stream.Write(time);
		}

		void Playback(BinaryReader stream)
		{
			float value;
			try
			{
				while((value = stream.ReadSingle()) == float.MinValue)
				{
					byte typeByte = stream.ReadByte();
					EventType type = (EventType)(typeByte & 0xf);
					int index = typeByte >> 4;

					foreach(EventStreamInput inputMethod in this.inputs)
						inputMethod.Playback(type, index, stream);
				}
				time = value;
			}
			catch(EndOfStreamException)
			{
				Debug.Log("End of event stream");
				reader = null;
			}
		}
	}

	abstract class EventStreamInput
	{
		protected Engine engine;

		public EventStreamInput()
		{
			engine = GameObject.FindObjectOfType<Engine>();
		}

		public virtual void Clear() { }
		public abstract void Record(BinaryWriter stream);
		public abstract void Playback(EventType type, int index, BinaryReader stream);

		protected void WriteEventHeader(BinaryWriter stream, int frame, EventType type, int index)
		{
			stream.Write(float.MinValue);
			stream.Write((byte)(type + (index << 4)));
		}
	}
}
