using UnityEngine;
using Object = UnityEngine.Object;

namespace PuzzleCore
{
	static class Obj
	{
		public static GameObject CreateGameObject(GameObject prefab, GameObject parent)
		{
			var item = GameObject.Instantiate(prefab) as GameObject;
			item.transform.SetParent(parent.transform, worldPositionStays:false);
			return item;
		}

		public static GameObject CreateGameObject(GameObject prefab, GameObject parent, Vector3 localPosition, Quaternion localRotation = default(Quaternion))
		{
			var item = GameObject.Instantiate(prefab) as GameObject;
			item.transform.SetParent(parent.transform, worldPositionStays:false);
			item.transform.localPosition = localPosition;
			item.transform.localRotation = localRotation;
			return item;
		}

		public static T Create<T>(GameObject parent) where T:Component
		{
			var item = new GameObject(typeof(T).Name);
			item.transform.SetParent(parent.transform, worldPositionStays:false);
			return item.AddComponent<T>();
		}

		public static T Create<T>(T prefab, GameObject parent) where T:Component
		{
			var item = GameObject.Instantiate<T>(prefab);
			item.transform.SetParent(parent.transform, worldPositionStays:false);
			return item;
		}

		public static T Create<T>(T prefab, GameObject parent, Vector3 localPosition, Quaternion localRotation = default(Quaternion)) where T : MonoBehaviour
		{
			var item = GameObject.Instantiate<T>(prefab);
			item.transform.SetParent(parent.transform, worldPositionStays: false);
			item.transform.localPosition = localPosition;
			item.transform.localRotation = localRotation;
			return item;
		}

		public static void DestroyChildren(GameObject gameObject)
		{
			DestroyChildren(gameObject.transform);
		}

		public static void DestroyChildren(Transform transform)
		{
			foreach(Transform child in transform)
				Object.Destroy(child.gameObject);
		}

		public static void DestroyChildren(MonoBehaviour view)
		{
			foreach(Transform child in view.transform)
				Object.Destroy(child.gameObject);
		}

		public static void Destroy(GameObject gameObject)
		{
			if (gameObject != null)
			{
				Object.Destroy(gameObject);
			}
		}

		public static void Destroy(MonoBehaviour view)
		{
			if (view != null && view.gameObject != null)
			{
				Object.Destroy(view.gameObject);
			}
		}

		public static void DeleteChildren(GameObject parent)
		{
			foreach (Transform child in parent.transform)
				GameObject.Destroy(child.gameObject);
		}
	}
}