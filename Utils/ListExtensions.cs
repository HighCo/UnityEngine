using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;
using Random = UnityEngine.Random;

namespace PuzzleCore
{
	public static class ListExtensions
	{
	    public static T PickRandom<T>(this IList<T> source)
	    {
	        if (source.Count == 0)
	            return default(T);

	        int i = Random.Range(0, source.Count);
	        return source[i];
	    }

	    public static T PopRandom<T>(this IList<T> source)
	    {
	        if (source.Count == 0)
	            return default(T);

	        int i = Random.Range(0, source.Count);
	        T value = source[i];
	        source.RemoveAt(i);

	        return value;
	    }

	    public static bool RemoveFirst<T>(this IList<T> list, System.Func<T, bool> predicate)
	    {
	        for (int i = 0; i < list.Count; i++)
	        {
	            if (predicate(list[i]))
	            {
	                list.RemoveAt(i);
	                return true;
	            }
	        }

	        return false;
	    }

	    public static T Last<T>(this IList<T> source)
	    {
	        if (source.Count == 0)
	            return default(T);

	        return source[source.Count - 1];
	    }

		public static T First<T>(this IList<T> source, Func<T, bool> predicate)
		{
			if (source != null)
			{
				for (int i = 0; i < source.Count; i++)
				{
					if (predicate(source[i]))
					{
						return source[i];
					}
				}
			}
			return default(T);
		}

		public static T FindRandom<T>(this IList<T> source, Func<T, bool> predicate)
		{
			var copy = source.ToArray();
			copy.Shuffle();
			return copy.First(predicate);
		}


	    public static T FirstOrDefault<T>(this IList<T> source)
	    {
	        if (source.Count == 0)
	            return default(T);

	        return source[0];
	    }

		public static T FirstOrDefault<T>(this IList<T> source, Predicate<T> predicate)
		{
			for (int i = 0; i < source.Count; i++)
			{
				if (predicate(source[i]))
					return source[i];
			}
			return default(T);
		}

	    public static T PopFirst<T>(this IList<T> source)
	    {
	        if (source.Count == 0)
	            return default(T);

	        T value = source[0];
	        source.RemoveAt(0);

	        return value;
	    }

		public static T[] SubArray<T>(this T[] array, int from, int length)
		{
			if(from > 0 && array.Length >= length)
			{
				var copy = new T[length];
				Array.Copy(array, from, copy, 0, length);
				return copy;
			}
			throw new ArgumentOutOfRangeException(string.Format("Sub array from '{0}' size '{1}' does not fit array of size '{2}'", from, length, array.Length));
		}

	    public static T PopLast<T>(this IList<T> source)
	    {
	        if (source.Count == 0)
	            return default(T);

	        T value = source.Last();
	        source.RemoveAt(source.Count - 1);

	        return value;
	    }

	    public static T Pop<T>(this IList<T> source, int index)
	    {
	        if (source.Count <= index)
	            return default(T);

	        T value = source[index];
	        source.RemoveAt(index);

	        return value;
	    }

	    public static List<T> Clone<T>(this List<T> source)
	    {
	        return new List<T>(source);
	    }

		public static void Shuffle<T>(this IList<T> list)
		{
			int index = list.Count;
			while (index > 1)
			{
				index--;
				int newPos = Random.Range(0, index + 1);
				T temp = list[newPos];
				list[newPos] = list[index];
				list[index] = temp;
			}
		}

		public static void SetShuffled<T>(this List<T> target, IEnumerable<T> source)
		{
			target.Clear();
			target.AddRange(source);
			target.Shuffle();
		}

		public static List<T> SetFiltered<T>(this List<T> target, IEnumerable<T> source, Predicate<T> predicate)
		{
			target.Clear();
			foreach(T item in source)
				if(predicate(item))
					target.Add(item);
			return target;
		}

	    public static void SwitchPlaces<T>(this IList<T> list, int indexOne, int indexTwo)
	    {
	        if (indexOne < 0 || indexTwo < 0 || indexOne == indexTwo || indexOne >= list.Count || indexTwo >= list.Count)
	            return;

	        T temp = list[indexOne];
	        list[indexOne] = list[indexTwo];
	        list[indexTwo] = temp;
	    }
	}
}
