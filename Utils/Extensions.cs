using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using UnityEngine;
using System.Text;
using System.Text.RegularExpressions;

namespace PuzzleCore
{
	public static class Extensions
	{
		public static float Clamp(float value, float min, float max)
		{
			if(value < min) return min;
			if(value > max) return max;
			                return value;
		}

		public static int Clamp(int value, int min, int max)
		{
			if(value < min) return min;
			if(value > max) return max;
			                return value;
		}

		public static Vector3 Clamp(Vector3 value, Vector3 min, Vector3 max)
		{
			return new Vector3(
				Clamp(value.x, min.x, max.x),
				Clamp(value.y, min.y, max.y),
				Clamp(value.z, min.z, max.z)
			);
		}

		public static bool ContainsAll<T>(this IEnumerable<T> superset, IEnumerable<T> subset)
		{
			foreach(var item in subset)
				if(!superset.Contains(item))
					return false;
			return true;
		}

		public static bool ContainsAny<T>(this IEnumerable<T> superset, IEnumerable<T> subset)
		{
			foreach(var item in subset)
				if(superset.Contains(item))
					return true;
			return false;
		}

		public static T Random<T>(this IList<T> list)
		{
			if(list.Count > 0) return list[UnityEngine.Random.Range(0,list.Count)];
			else               return default(T);
		}

		static List<int> randomIndices = new List<int>();

		public static T RandomWhere<T>(this IList<T> list, Predicate<T> predicate)
		{
			randomIndices.Clear();
			for (int i = 0; i < list.Count; i++)
				if(predicate(list[i]))
					randomIndices.Add(i);

			if(randomIndices.Count == 1) return list[randomIndices[0]];
			if(randomIndices.Count > 0)  return list[randomIndices.Random()];
			else                         return default(T);
		}

		public static T RandomPopWhere<T>(this IList<T> list, Predicate<T> predicate)
		{
			randomIndices.Clear();
			for (int i = 0; i < list.Count; i++)
				if(predicate(list[i]))
					randomIndices.Add(i);

			if(randomIndices.Count > 0)
			{
				int index = randomIndices.Random();
				var item = list[index];
				list.RemoveAt(index);
				return item;
			}
			else
				return default(T);
		}

		public static void Ensure<T>(this IList<T> list, T item)
		{
			if(!list.Contains(item))
				list.Add(item);
		}

		public static T[,] Copy<T>(this T[,] source)
		{
			var len0 = source.GetLength(0);
			var len1 = source.GetLength(1);
			var target = new T[len0, len1];
			for(var a=0; a<len0; a++)
				for(var b=0; b<len1; b++)
					target[a,b] = source[a,b];
			return target;
		}

		public static List<T> Set<T>(this List<T> target, IEnumerable<T> source)
		{
			target.Clear();
			target.AddRange(source);
			return target;
		}

		public static T[] Clear<T>(this T[] source)
		{
			for(int i=0; i<source.Length; i++)
				source[i] = default(T);
			return source;
		}

		public static List<T> SetEntriesToDefault<T>(this List<T> source)
		{
			for(int i=0; i<source.Count; i++)
				source[i] = default(T);
			return source;
		}

		public static T[,] Clear<T>(this T[,] source)
		{
			var len0 = source.GetLength(0);
			var len1 = source.GetLength(1);
			for(var a=0; a<len0; a++)
				for(var b=0; b<len1; b++)
					source[a,b] = default(T);
			return source;
		}

		public static bool All(this bool[,] source)
		{
			var len0 = source.GetLength(0);
			var len1 = source.GetLength(1);
			for(var a=0; a<len0; a++)
				for(var b=0; b<len1; b++)
					if(!source[a,b])
						return false;
			return true;
		}

		public static int Count<T>(this T[,] source, Func<T, bool> condition)
		{
			int count = 0;
			var len0 = source.GetLength(0);
			var len1 = source.GetLength(1);
			for(var a=0; a<len0; a++)
				for(var b=0; b<len1; b++)
					if(condition(source[a,b]))
						count++;
			return count;
		}

		public static bool All<T>(this T[,] source, Func<T, bool> condition)
		{
			var len0 = source.GetLength(0);
			var len1 = source.GetLength(1);
			for (var a = 0; a < len0; a++)
				for (var b = 0; b < len1; b++)
					if (!condition(source[a, b]))
						return false;
			return true;
		}

		public static List<T> Find<T>(this T[,] source, Func<T, bool> condition)
		{
			var list = new List<T>();
			var len0 = source.GetLength(0);
			var len1 = source.GetLength(1);
			for(var a=0; a<len0; a++)
				for(var b=0; b<len1; b++)
				{
					T item = source[a,b];
					if(condition(item))
						list.Add(item);
				}
			return list;
		}

		public static bool Any<T>(this T[,] source, Func<T, bool> condition)
		{
			var len0 = source.GetLength(0);
			var len1 = source.GetLength(1);
			for(var a=0; a<len0; a++)
			for(var b=0; b<len1; b++)
			{
				T item = source[a,b];
				if (condition(item))
				{
					return true;
				}
			}
			return false;
		}

		public static IEnumerable<T> Where<T>(this T[,] source, Func<T, bool> condition)
		{
			var len0 = source.GetLength(0);
			var len1 = source.GetLength(1);
			for(var a=0; a<len0; a++)
			for(var b=0; b<len1; b++)
			{
				T item = source[a,b];
				if (condition(item))
				{
					yield return item;
				}
			}
		}

        public static List<T> Find<T>(this T[,] source, IntBound bound, Func<T, bool> condition)
	    {
	        var list = new List<T>();
	        var len0 = source.GetLength(0);
	        var len1 = source.GetLength(1);
	        for (var x = bound.min.x; x <= bound.max.x && x < len0; x++)
	        {
	            for (var y = bound.min.y; y <= bound.max.y && y < len1; y++)
	            {
	                var item = source[x, y];
	                if (condition(item))
	                    list.Add(item);
	            }
	        }
	        return list;
	    }

		public static int FindIndex<T>(this IEnumerable<T> source, Func<T, bool> condition)
		{
			int i = 0;
			foreach (T item in source)
			{
				if (condition(item))
					return i;
				i++;
			}
			return -1;
		}

		public static int FindIndex(this IEnumerable<int> source, int value)
		{
			int i = 0;
			foreach (int item in source)
			{
				if (item == value)
					return i;
				i++;
			}
			return -1;
		}

		public static int SumBy<T>(this IEnumerable<T> source, Func<T, int> selector)
		{
			int total = 0;
			using(var sourceIterator = source.GetEnumerator())
				while(sourceIterator.MoveNext())
					total += selector(sourceIterator.Current);
			return total;
		}

		public static TSource MinBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> selector) where TKey:IComparable
		{
			using(var sourceIterator = source.GetEnumerator())
			{
				if(!sourceIterator.MoveNext())
					return default(TSource);
				var min = sourceIterator.Current;
				var minProjected = selector(min);
				while(sourceIterator.MoveNext())
				{
					var candidate = sourceIterator.Current;
					var candidateProjected = selector(candidate);
					if(candidateProjected.CompareTo(minProjected) < 0)
					{
						min = candidate;
						minProjected = candidateProjected;
					}
				}
				return min;
			}
		}

		public static TSource MinBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> selector1, Func<TSource, TKey> selector2) where TKey:IComparable
		{
			using(var sourceIterator = source.GetEnumerator())
			{
				if(!sourceIterator.MoveNext())
					return default(TSource);
				var min = sourceIterator.Current;
				var minProjected1 = selector1(min);
				var minProjected2 = selector2(min);
				while(sourceIterator.MoveNext())
				{
					var candidate = sourceIterator.Current;
					var candidateProjected1 = selector1(candidate);
					var comparison1 = candidateProjected1.CompareTo(minProjected1);

					if(comparison1 <= 0)
					{
						var candidateProjected2 = selector2(candidate);

						if(comparison1 < 0 || candidateProjected2.CompareTo(minProjected2) < 0)
						{
							min = candidate;
							minProjected1 = candidateProjected1;
							minProjected2 = candidateProjected2;
						}
					}
				}
				return min;
			}
		}

		public static TSource MaxBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> selector) where TKey:IComparable
		{
			using(var sourceIterator = source.GetEnumerator())
			{
				if(!sourceIterator.MoveNext())
					return default(TSource);
				var min = sourceIterator.Current;
				var minProjected = selector(min);
				while(sourceIterator.MoveNext())
				{
					var candidate = sourceIterator.Current;
					var candidateProjected = selector(candidate);
					if(candidateProjected.CompareTo(minProjected) > 0)
					{
						min = candidate;
						minProjected = candidateProjected;
					}
				}
				return min;
			}
		}

		public static int MaxIndexBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> selector) where TKey:IComparable
		{
			using(var sourceIterator = source.GetEnumerator())
			{
				if (!sourceIterator.MoveNext())
					return -1;
				var minProjected = selector(sourceIterator.Current);
				int index = 0;
				int minIndex = 0;
				while(sourceIterator.MoveNext())
				{
					index++;

					var candidate = sourceIterator.Current;
					var candidateProjected = selector(candidate);
					if(candidateProjected.CompareTo(minProjected) > 0)
					{
						minIndex = index;
						minProjected = candidateProjected;
					}
				}
				return minIndex;
			}
		}

		public static int MaxIndex<T>(this IEnumerable<T> source) where T:IComparable
		{
			using(var sourceIterator = source.GetEnumerator())
			{
				if (!sourceIterator.MoveNext())
					return -1;
				var min = sourceIterator.Current;
				int index = 0;
				int minIndex = 0;
				while(sourceIterator.MoveNext())
				{
					index++;

					var candidate = sourceIterator.Current;
					if(candidate.CompareTo(min) > 0)
					{
						minIndex = index;
						min = candidate;
					}
				}
				return minIndex;
			}
		}

		public static int DirToX(this Direction dir)
		{
			return dir == Direction.Right ? 1 : dir == Direction.Left ? -1 : 0;
		}

		public static int DirToY(this Direction dir)
		{
			return dir == Direction.Up ? 1 : dir == Direction.Down ? -1 : 0;
		}

		public static IntVector2 DirToVector2(this Direction dir)
		{
			return new IntVector2(dir.DirToX(), dir.DirToY());
		}

	    public static bool InBounds<T>(this T[,] array, IntVector2 pos)
	    {
	        return InBounds<T>(array, pos.x, pos.y);
	    }

	    public static bool InBounds<T>(this T[,] array, int x, int y)
	    {
	        return x >= 0 && y >= 0 && x < array.GetLength(0) && y < array.GetLength(1);
	    }

		public static bool Iterate<T>(this T[,] array, Direction dir, Direction secondaryDir, IntBound bound, Func<int, int, Direction, bool> func)
	    {
	        bool ret = false;
	        switch (dir)
	        {
				case Direction.Down:
				default:
					if (secondaryDir == Direction.Right)
					{
						ret = IterateDownRight<T>(array, dir, bound, func);
					}
					else
					{
						ret = IterateDownLeft<T>(array, dir, bound, func);
					}
	                break;
				case Direction.Right:
					if (secondaryDir == Direction.Up)
					{
						ret = IterateRightUp<T>(array, dir, bound, func);
					}
					else
					{
						ret = IterateRightDown<T>(array, dir, bound, func);
					}
	                break;
				case Direction.Up:
					if (secondaryDir == Direction.Right)
					{
						ret = IterateUpRight<T>(array, dir, bound, func);
					}
					else
					{
						ret = IterateUpLeft<T>(array, dir, bound, func);
					}
	                break;
				case Direction.Left:
					if (secondaryDir == Direction.Up)
					{
						ret = IterateLeftUp<T>(array, dir, bound, func);
					}
					else
					{
						ret = IterateLeftDown<T>(array, dir, bound, func);
					}
	                break;
	        }
	        return ret;
	    }

		private static bool IterateDownRight<T>(T[,] array, Direction dir, IntBound bound, Func<int, int, Direction, bool> func)
	    {
	        bool ret = false;
	        for (int x = bound.min.x; x <= bound.max.x; x++)
	        {
	            for (int y = bound.min.y; y <= bound.max.y; y++)
	            {
	                var value = func(x, y, dir);
					ret = ret || value;
	            }
	        }
	        return ret;
	    }

		private static bool IterateDownLeft<T>(T[,] array, Direction dir, IntBound bound, Func<int, int, Direction, bool> func)
		{
			bool ret = false;
			for (int x = bound.max.x; x >= bound.min.x; x--)
			{
				for (int y = bound.min.y; y <= bound.max.y; y++)
				{
					var value = func(x, y, dir);
					ret = ret || value;
				}
			}
			return ret;
		}

		private static bool IterateRightUp<T>(T[,] array, Direction dir, IntBound bound, Func<int, int, Direction, bool> func)
	    {
	        bool ret = false;
	        for (int y = bound.min.y; y <= bound.max.y; y++)
	        {
	            for (int x = bound.min.x; x <= bound.max.x; x++)
	            {
	                var value = func(x, y, dir);
					ret = ret || value;
	            }
	        }
	        return ret;
	    }

		private static bool IterateRightDown<T>(T[,] array, Direction dir, IntBound bound, Func<int, int, Direction, bool> func)
		{
			bool ret = false;
			for (int y = bound.max.y; y >= bound.min.y; y--)
			{
				for (int x = bound.min.x; x <= bound.max.x; x++)
				{
					var value = func(x, y, dir);
					ret = ret || value;
				}
			}
			return ret;
		}

		private static bool IterateUpRight<T>(T[,] array, Direction dir, IntBound bound, Func<int, int, Direction, bool> func)
	    {
	        bool ret = false;
	        for (int x = bound.min.x; x <= bound.max.x; x++)
	        {
	            for (int y = bound.max.y; y >= bound.min.y; y--)
	            {
	                var value = func(x, y, dir);
					ret = ret || value;
	            }
	        }
	        return ret;
	    }

		private static bool IterateUpLeft<T>(T[,] array, Direction dir, IntBound bound, Func<int, int, Direction, bool> func)
		{
			bool ret = false;
			for (int x = bound.max.x; x >= bound.min.x; x--)
			{
				for (int y = bound.max.y; y >= bound.min.y; y--)
				{
					var value = func(x, y, dir);
					ret = ret || value;
				}
			}
			return ret;
		}

		private static bool IterateLeftUp<T>(T[,] array, Direction dir, IntBound bound, Func<int, int, Direction, bool> func)
	    {
	        bool ret = false;
	        for (int y = bound.min.y; y <= bound.max.y; y++)
	        {
	            for (int x = bound.max.x; x >= bound.min.x; x--)
	            {
	                var value = func(x, y, dir);
					ret = ret || value;
	            }
	        }
	        return ret;
	    }

		private static bool IterateLeftDown<T>(T[,] array, Direction dir, IntBound bound, Func<int, int, Direction, bool> func)
		{
			bool ret = false;
			for (int y = bound.max.y; y >= bound.min.y; y--)
			{
				for (int x = bound.max.x; x >= bound.min.x; x--)
				{
					var value = func(x, y, dir);
					ret = ret || value;
				}
			}
			return ret;
		}

	    public static List<T> GetValidElementsAround<T>(this T[,] array, IntVector2 pos)
	    {
	        var elements = new List<T>();
	        var xBound = array.GetLength(0);
	        var yBound = array.GetLength(1);
	        for (int i = -1; i < 2; i++)
	        {
	            for (int j = -1; j < 2; j++)
	            {
	                var isSelf = i == 0 && j == 0;
	                if (!isSelf)
	                {
	                    var x = pos.x + i;
	                    var y = pos.y + j;
	                    if (x > 0 && x < xBound && y > 0 && y < yBound)
	                    {
	                        var element = array[x, y];
	                        if (element != null)
	                        {
	                            elements.Add(element);
	                        }
	                    }
	                }
	            }
	        }
	        return elements;
	    }


		public static IEnumerable<T> SliceRow<T>(this T[,] array, int row)
		{
			if (row < array.GetLowerBound(1) || row > array.GetUpperBound(1))
			{
				yield break;
			}
			for (var x = array.GetLowerBound(0); x <= array.GetUpperBound(0); x++)
			{
				yield return array[x, row];
			}
		}

		public static IEnumerable<T> SliceColumn<T>(this T[,] array, int column)
		{
			if (column < array.GetLowerBound(0) || column > array.GetUpperBound(0))
			{
				yield break;
			}
			for (var y = array.GetLowerBound(1); y <= array.GetUpperBound(1); y++)
			{
				yield return array[column, y];
			}
		}

		public static IEnumerable<T> SliceDimension<T>(this T[,] array, bool horizontal, int index)
		{
			if (horizontal && index < array.GetLength(1))
			{
				for (var x = array.GetLowerBound(0); x <= array.GetUpperBound(0); x++)
				{
					yield return array[x, index];
				}
			}
			else if(index < array.GetLength(0))
			{
				for (var y = array.GetLowerBound(1); y <= array.GetUpperBound(1); y++)
				{
					yield return array[index, y];
				}
			}
		}

		public static IEnumerable<T> Flatten<T>(this T[,] array)
		{
			for (var i = array.GetLowerBound(0); i <= array.GetUpperBound(0); i++)
			{
				for (var j = array.GetLowerBound(1); j <= array.GetUpperBound(1); j++)
				{
					yield return array[i, j];
				}
			}
		}

		public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
		{
			foreach (T item in enumerable)
			{
				action(item);
			}
		}

		public static IEnumerable<T> ToEnumerable<T> (this T[,] array)
		{
			foreach (var item in array)
			{
				yield return item;
			}
		}

		public static IEnumerable<T> ToEnumerable<T> (this T item)
		{
			yield return item;
		}

		public static List<List<T>> Split<T>(this List<T> list, int chunkLength)
		{
			var listOfLists = new List<List<T>>();

			for (int i = 0; i < list.Count; i += chunkLength)
			{
				listOfLists.Add(list.GetRange(i, Math.Min(chunkLength, list.Count - i)));
			}

			return listOfLists;
		}

		public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		{
			HashSet<TKey> knownKeys = new HashSet<TKey>();
			foreach (TSource element in source)
				if (knownKeys.Add(keySelector(element)))
					yield return element;
		}

		public static bool IsNullOrEmptyCollection(this ICollection collection)
		{
			return collection == null || collection.Count == 0;
		}

		public static T[,] CreateResizedArrayAndCopyElements<T>(this T[,] original, int rows, int cols)
		{
			T[,] newArray = new T[rows, cols];
			int minX = Math.Min(original.GetLength(0), newArray.GetLength(0));
			int minY = Math.Min(original.GetLength(1), newArray.GetLength(1));

			for (int i = 0; i < minX; ++i)
			{
				Array.Copy(original, i * original.GetLength(1), newArray, i * newArray.GetLength(1), minY);
			}

			return newArray;
		}

		public static bool IsSmaller<T>(this T[,] array, IntVector2 size)
		{
			return array.GetLength(0) <= size.x || array.GetLength(1) <= size.y;
		}

		public static IEnumerable<T> AsEmptyIfNull<T> (this IEnumerable<T> source)
		{
			return source ?? Enumerable.Empty<T>();
		}

		public static T[] ToArray<T> (this IEnumerable<T> enumerable, T[] useIfNull)
		{
			return enumerable != null ? enumerable.ToArray() : useIfNull;
		}

		public static T[] Switch<T> (this T[] array, int aIndex, int bIndex)
		{
			var a = array[aIndex];
			array[aIndex] = array[bIndex];
			array[bIndex] = a;
			return array;
		}

		public static List<T> EnsureAmount<T>(this List<T> list, int count) where T : new()
		{
			for (int i = list.Count; i < count; i++)
				list.Add(new T());

			while(list.Count > count)
				list.RemoveAt(list.Count -1);

			return list;
		}

		public static T[] CreateExpandedArray<T> (this T[] array, int newLength, T assignWith)
		{
			var newArray = new T[newLength];
			if (array != null)
			{
				Array.Copy(array, newArray, array.Length);
			}
			for (int i = array != null ? array.Length : 0; i < newLength; i++)
			{
				newArray[i] = assignWith;
			}
			return newArray;
		}

		public static T[] CreateShrinkedArray<T> (this T[] array, int shrinkBy)
		{
			var newArray = new T[array.Length - shrinkBy];
			Array.Copy(array, newArray, newArray.Length);
			return newArray;
		}
	}

	public static class EnumUtil
	{
		public static T Parse<T>(string value, T defaultValue = default(T), bool ignoreCase = false)
		{
			if (!Enum.IsDefined(typeof(T), value))
				return defaultValue;

			return (T)Enum.Parse(typeof(T), value, ignoreCase);
		}

		public static IEnumerable<T> GetValues<T>() where T : struct, IConvertible
		{
			Type t = typeof(T);
			if (t.IsEnum)
			{
				return Enum.GetValues(t).Cast<T>();
			}
			throw new ArgumentException("<T> must be an enumerated type.");
		}
	}

	public static class Array<T>
	{
		public static readonly T[] Empty = new T[0];

		public static T[] Create (int length, T defaultValue)
		{
			var array = new T[length];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = defaultValue;
			}
			return array;
		}
	}

	public class NameOf<T>
	{
		public static string Property<TProp> (Expression<Func<T, TProp>> expression, bool addSpaceBeforeUppercaseLetter = true)
		{
			var body = expression.Body as MemberExpression;
			if (body == null)
			{
				throw new ArgumentException("'expression' should be a member expression");
			}

			if (addSpaceBeforeUppercaseLetter)
			{
				return Regex.Replace(body.Member.Name, "[A-Z]", " $0").Trim();
			}
			else
			{
				return body.Member.Name;
			}
		}
	}

	public static class ReflectionUtil
	{
		public static IEnumerable<string> GetConstantsNames (this Type type)
		{
			return GetConstantsFieldInfos(type).Select(fi => fi.Name).ToArray();
		}

		public static IEnumerable<object> GetConstantsValues (this Type type)
		{
			return GetConstantsFieldInfos(type).Select(fi => fi.GetValue(null));
		}

		public static IEnumerable<FieldInfo> GetConstantsFieldInfos (Type type)
		{
			return type.GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy)
				.Where(fi => fi.IsLiteral && !fi.IsInitOnly);
		}

	}

	public static class StringExtensions
	{
		public static string CapitalizeFirstLetter(this string str)
		{
			if(str.Length == 0) return "";
			if(str.Length == 1) return str.ToUpper();
			                    return char.ToUpper(str[0]) + str.Substring(1);
		}
	}
}
