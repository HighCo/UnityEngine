using System.Collections.Generic;

namespace PuzzleCore
{
	interface IPoolable
	{
		void Clear();
	}

	class Pool<T> where T:class, new()
	{
		static List<T> items = new List<T>();

		static public T Create()
		{
			return items.PopLast() ?? new T();
		}

		static public void Release(T item)
		{
			var i  = item as IPoolable;
			if(i != null)
				i.Clear();

			items.Add(item);
		}
	}

	class ListPool<T>
	{
		static List<List<T>> items = new List<List<T>>();

		static public List<T> Create()
		{
			return items.PopLast() ?? new List<T>();
		}

		static public void Release(List<T> item)
		{
			item.Clear();
			items.Add(item);
		}
	}
}