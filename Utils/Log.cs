﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace PuzzleCore
{
	public class Log : MonoBehaviour
	{
		[SerializeField] TextMeshPro textLabel;
		[SerializeField] GameObject panel;

		static Log instance;
		static string text;

		public static void Hide() => instance.panel.SetActive(false);
		public static void Show()
		{
			if(instance == null) instance = Object.FindObjectOfType<Log>();
			text = "";
			instance.panel.SetActive(true);
		}

		public static void Info(string message, params object[] arguments)
		{
			if(arguments.Length > 0) message = string.Format(message, arguments);
			text += message + "\n";
			instance.textLabel.text = text;
			Debug.Log(message);
		}

		public static void Error(string message, params object[] arguments)
		{
			if(instance == null) instance = Object.FindObjectOfType<Log>();
			if(arguments.Length > 0) message = string.Format(message, arguments);
			text += "ERROR: " + message + "\n";
			instance.textLabel.text = text;
			Debug.LogError(message);
		}

		void Update()
		{
			if(Input.GetKeyDown(KeyCode.Tab))
				panel.SetActive(!panel.activeSelf);
		}
	}
}
