using System.Collections;
using UnityEngine;

namespace PuzzleCore
{
	public static class CoroutineExtensions
	{
		public class CoroutineRunner : MonoBehaviour { }

		public static CoroutineRunner runner;

		public static IPromise RunCoroutine (this IEnumerator routine)
		{
			var promise = new Promise();

			if (runner == null)
			{
				var go = new GameObject();
				runner = go.AddComponent<CoroutineRunner>();
			}

			runner.StartCoroutine(Coroutine(routine, promise));
			return promise;
		}

		static IEnumerator Coroutine(IEnumerator routine, Promise promise)
		{
			yield return routine;
			promise.Resolve();
		}
	}
}