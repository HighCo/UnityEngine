﻿using System.Collections;
using System.Collections.Generic;

namespace PuzzleCore
{
	public struct Matrix<T> : IEnumerable<T>
	{
		T[,] entries;
		public IntVector2 size;

		public Matrix(int x, int y)
		{
			this.entries = new T[x,y];
			this.size = new IntVector2(x,y);
		}

		public Matrix(T[,] entities)
		{
			this.entries = entities;
			this.size = new IntVector2(entities.GetLength(0), entities.GetLength(1));
		}

		public void Clear()
		{
			entries.Clear();
		}

		public T this[int x, int y]
		{
			get { return entries[x, y]; }
			set { entries[x, y] = value; }
		}

		public T this[IntVector2 pos]
		{
			get { return entries[pos.x, pos.y]; }
			set { entries[pos.x, pos.y] = value; }
		}

		public static implicit operator Matrix<T>(T[,] value)
		{
			return new Matrix<T>(value);
		}

		public static implicit operator T[,](Matrix<T> value)
		{
			return value.entries;
		}

		public bool isSet
		{
			get { return entries != null; }
		}

		public IEnumerator<T> GetEnumerator()
		{
			for(int x = 0; x < size.x; x++)
			for(int y = 0; y < size.y; y++)
				yield return entries[x, y];
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public struct Entry
		{
			public T value;
			public IntVector2 index;
		}

		public IEnumerable<Entry> EntryAndIndex
		{
			get
			{
				for(int x = 0; x < size.x; x++)
				for (int y = 0; y < size.y; y++)
					yield return new Entry {value = entries[x, y], index = new IntVector2(x, y)};
			}
		}
	}

}