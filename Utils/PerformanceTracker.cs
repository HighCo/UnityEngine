using System;
using System.Diagnostics;

namespace PuzzleCore
{
	public class PerformanceTracker : IDisposable
	{
		Stopwatch stopwatch = new Stopwatch();
		string name;

		public PerformanceTracker(string name)
		{
			this.name = name;
			stopwatch.Start();
		}

		void IDisposable.Dispose()
		{
			stopwatch.Stop();
			var time = stopwatch.Elapsed.TotalMilliseconds;
			if(time > 10) UnityEngine.Debug.LogFormat("{0}: {1} ms", name, stopwatch.Elapsed.TotalMilliseconds);
		}
	}
}