﻿using System;
using UnityEngine;

namespace PuzzleCore
{
	[Serializable]
	public struct IntVector2
	{
		public IntVector2(int x, int y)
		{
			this.x = x;
			this.y = y;
		}
		public int x;
		public int y;

		public static implicit operator Vector2(IntVector2 a) { return new Vector2(a.x, a.y); }
		public static explicit operator IntVector2(Vector2 a) { return new IntVector2((int)a.x, (int)a.y); }

		public static implicit operator Vector3(IntVector2 a) { return new Vector3(a.x, a.y, 0); }
		public static explicit operator IntVector2(Vector3 a) { return new IntVector2((int)a.x, (int)a.y); }

		public static IntVector2 operator +(IntVector2 a, IntVector2 b) { return new IntVector2(a.x+b.x, a.y+b.y); }
		public static IntVector2 operator -(IntVector2 a, IntVector2 b) { return new IntVector2(a.x-b.x, a.y-b.y); }
		public static IntVector2 operator -(IntVector2 a) { return new IntVector2(-a.x, -a.y); }

		public static Vector2 operator +(IntVector2 a, Vector2 b) { return new Vector2(a.x+b.x, a.y+b.y); }
		public static Vector2 operator -(IntVector2 a, Vector2 b) { return new Vector2(a.x-b.x, a.y-b.y); }

		public static Vector2 operator +(Vector2 a, IntVector2 b) { return new Vector2(a.x+b.x, a.y+b.y); }
		public static Vector2 operator -(Vector2 a, IntVector2 b) { return new Vector2(a.x-b.x, a.y-b.y); }

		public static float operator *(IntVector2 a, IntVector2 b) { return a.x * b.x + a.y * b.y; }

		public static IntVector2 operator *(IntVector2 a, int value) { return new IntVector2(a.x*value, a.y*value); }
		public static IntVector2 operator /(IntVector2 a, int value) { return new IntVector2(a.x/value, a.y/value); }

		public static bool operator ==(IntVector2 a, IntVector2 b) { return a.x==b.x && a.y==b.y; }
		public static bool operator !=(IntVector2 a, IntVector2 b) { return a.x!=b.x || a.y!=b.y; }

		public static readonly IntVector2 none = new IntVector2(int.MinValue, int.MinValue);
		public static readonly IntVector2 zero = new IntVector2(0,0);
		public static readonly IntVector2 one = new IntVector2(1,1);

		public static readonly IntVector2 left = new IntVector2 (-1, 0);
		public static readonly IntVector2 right = new IntVector2 (1, 0);
		public static readonly IntVector2 down = new IntVector2 (0, -1);
		public static readonly IntVector2 up = new IntVector2 (0, 1);

		public IntVector2 NewWithDeltaX (int deltaX)
		{
			return new IntVector2(x + deltaX, y);
		}

		public IntVector2 NewWithDeltaY (int deltaY)
		{
			return new IntVector2(x, y + deltaY);
		}

		public int GetSquaredLength () { return x * x + y * y; }


		public int ManhattanDistance (IntVector2 other)
		{
			return Mathf.Abs(x - other.x) + Mathf.Abs(y - other.y);
		}

		public float magnitude {get { return Mathf.Sqrt(x*x + y*y);  } }

		public float sqrMagnitude { get { return x * x + y * y; } }

		/// <summary>
		/// Crude (int) distance between two points, or how many steps does it take
		/// to get from one point to another, when both straight and diagonal steps
		/// are allowed. (=> eg. CrudeDistance((0,0),(1,1)) == 1).
		/// </summary>
		/// <returns>The distance.</returns>
		/// <param name="other">Other IntVector.</param>
		public int CrudeDistance(IntVector2 other)
		{
			var deltaX = Mathf.Abs(x - other.x);
			var deltaY = Mathf.Abs(y - other.y);

			return Mathf.Max(deltaX, deltaY);
		}

		public override string ToString() { return string.Format("({0},{1})", x, y); }
		public override int GetHashCode() { return base.GetHashCode(); }

		public override bool Equals(object obj)
		{
			if(obj is IntVector2)
				return this == (IntVector2)obj;
			else
				return false;
		}
	}

	public struct IntVector3
	{
		public IntVector3(int x, int y, int z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}
		public int x;
		public int y;
		public int z;

		public static implicit operator Vector3(IntVector3 a) { return new Vector3(a.x, a.y, a.z); }
		public static explicit operator IntVector3(Vector3 a) { return new IntVector3((int)a.x, (int)a.y, (int)a.z); }

		public static explicit operator Vector2(IntVector3 a) { return new Vector2(a.x, a.y); }
		public static explicit operator IntVector3(Vector2 a) { return new IntVector3((int)a.x, (int)a.y, 0); }

		public static IntVector3 operator +(IntVector3 a, IntVector3 b) { return new IntVector3(a.x+b.x, a.y+b.y, a.z+b.z); }
		public static IntVector3 operator -(IntVector3 a, IntVector3 b) { return new IntVector3(a.x-b.x, a.y-b.y, a.z-b.z); }
		public static IntVector3 operator -(IntVector3 a) { return new IntVector3(-a.x, -a.y, -a.z); }

		public static Vector3 operator +(IntVector3 a, Vector3 b) { return new Vector3(a.x+b.x, a.y+b.y, a.z+b.z); }
		public static Vector3 operator -(IntVector3 a, Vector3 b) { return new Vector3(a.x-b.x, a.y-b.y, a.z-b.z); }

		public static Vector3 operator +(Vector3 a, IntVector3 b) { return new Vector3(a.x+b.x, a.y+b.y, a.z+b.z); }
		public static Vector3 operator -(Vector3 a, IntVector3 b) { return new Vector3(a.x-b.x, a.y-b.y, a.z-b.z); }

		public static IntVector3 operator *(IntVector3 a, int value) { return new IntVector3(a.x*value, a.y*value, a.z*value); }
		public static IntVector3 operator /(IntVector3 a, int value) { return new IntVector3(a.x/value, a.y/value, a.z/value); }

		public static bool operator ==(IntVector3 a, IntVector3 b) { return a.x==b.x && a.y==b.y && a.z==b.z; }
		public static bool operator !=(IntVector3 a, IntVector3 b) { return a.x!=b.x || a.y!=b.y || a.z!=b.z; }

		public static readonly IntVector3 none = new IntVector3(int.MinValue, int.MinValue, int.MinValue);
		public static readonly IntVector3 minValue = new IntVector3(int.MinValue, int.MinValue, int.MinValue);
		public static readonly IntVector3 maxValue = new IntVector3(int.MaxValue, int.MaxValue, int.MaxValue);

		public static readonly IntVector3 zero = new IntVector3(0,0,0);
		public static readonly IntVector3 one = new IntVector3(1,1,1);

		public static readonly IntVector3 right = new IntVector3(1,0,0);
		public static readonly IntVector3 left = new IntVector3(-1,0,0);
		public static readonly IntVector3 forward = new IntVector3(0,0,1);
		public static readonly IntVector3 back = new IntVector3(0,0,-1);

		public override string ToString() { return string.Format("({0},{1},{2})", x, y, z); }
		public override int GetHashCode() { return base.GetHashCode(); }

		public override bool Equals(object obj)
		{
			if(obj is IntVector3)
				return this == (IntVector3)obj;
			else
				return false;
		}
	}

	static class VectorExtensions
	{
		public static IntVector3 Round(this Vector3 source)
		{
			return new IntVector3(Mathf.RoundToInt(source.x), Mathf.RoundToInt(source.y), Mathf.RoundToInt(source.z));
		}

		public static IntVector2 RoundToIntVector2(this Vector3 source)
		{
			return new IntVector2(Mathf.RoundToInt(source.x), Mathf.RoundToInt(source.y));
		}
		public static IntVector2 Round(this Vector2 source)
		{
			return new IntVector2(Mathf.RoundToInt(source.x), Mathf.RoundToInt(source.y));
		}

		public static float GetMagnitudeXZ(this Vector3 source)
		{
			return Mathf.Sqrt(source.x*source.x + source.z*source.z);
		}
	}

	public struct IntBound
	{
	    public IntVector2 min;
	    public IntVector2 max;

	    public bool Contains(IntVector2 point)
	    {
	        return point.x >= min.x && point.x < max.x && point.y >= min.y && point.y < max.y;
	    }

	    public override string ToString()
	    {
	        return string.Format("[({0},{1}), ({2},{3})]", min.x, min.y, max.x, max.y);
	    }
	}
}
