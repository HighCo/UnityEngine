﻿using System;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Types;
using UnityEngine.Networking.Match;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace PuzzleCore
{
	class RelayNetwork : EventStreamInput
	{
		// Matchmaker related
		List<MatchInfoSnapshot> m_MatchList = new List<MatchInfoSnapshot>();

		bool m_MatchCreated;
		bool m_MatchJoined;
		MatchInfo m_MatchInfo;
		string m_MatchName = "NewRoom";
		NetworkMatch m_NetworkMatch;

		// Connection/communication related
		int m_HostId = -1;

		// On the server there will be multiple connections, on the client this will only contain one ID
		List<int> m_ConnectionIds = new List<int>();

		byte[] m_ReceiveBuffer;
		string m_NetworkMessage = "Hello world";
		string m_LastReceivedMessage = "";
		NetworkWriter m_Writer;
		NetworkReader m_Reader;
		bool m_ConnectionEstablished;

		const int k_ServerPort = 25000;
		const int k_MaxMessageSize = 65535;

		List<Message> messagePool = new List<Message>();

		public RelayNetwork()
		{
			Network.instance = this;

			var gameObject = new GameObject("Network Match");
			m_NetworkMatch = gameObject.AddComponent<NetworkMatch>();

			m_ReceiveBuffer = new byte[k_MaxMessageSize];
			m_Writer = new NetworkWriter();

			// While testing with multiple standalone players on one machine this will need to be enabled
			Application.runInBackground = true;
			Network.state = NetworkState.None;
		}

		public override void Close()
		{
			NetworkTransport.Shutdown();

			if(m_MatchCreated)
				m_NetworkMatch.DestroyMatch(m_MatchInfo.networkId, m_MatchInfo.domain, (success, info) =>
				{
					if(success) Log.Info("Close succesful");
					else        Log.Info("Close failed");

				});
		}

		public void OnConnectionDropped(bool success, string extendedInfo)
		{
			Log.Info("Connection has been dropped on matchmaker server");
			NetworkTransport.Shutdown();
			m_HostId = -1;
			m_ConnectionIds.Clear();
			m_MatchInfo = null;
			m_MatchCreated = false;
			m_MatchJoined = false;
			m_ConnectionEstablished = false;
		}

		public Task CreateMatch()
		{
			Log.Info("CreateMatch");
			Network.state = NetworkState.Connecting;

			var task = new TaskCompletionSource<bool>();
			m_NetworkMatch.CreateMatch(m_MatchName, 4, true, "", "", "", 0, 0,
			                           (bool success, string extendedInfo, MatchInfo matchInfo) =>
			                           {
				                           if(success)
				                           {
					                           Log.Info("Create match succeeded");
					                           Utility.SetAccessTokenForNetwork(matchInfo.networkId, matchInfo.accessToken);

					                           m_MatchCreated = true;
					                           m_MatchInfo = matchInfo;

					                           StartServer(matchInfo.address, matchInfo.port, matchInfo.networkId, matchInfo.nodeId);
					                           Network.state = NetworkState.Server;
					                           task.SetResult(true);
				                           }
				                           else
					                           throw new Exception("Create match failed. " + extendedInfo);
			                           });
			return task.Task;
		}

		public Task<List<MatchInfoSnapshot>> ListMatches()
		{
			Log.Info("ListMatches");
			var task = new TaskCompletionSource<List<MatchInfoSnapshot>>();
			m_NetworkMatch.ListMatches(0, 20, "", true, 0, 0,
			                           (bool success, string extendedInfo, List<MatchInfoSnapshot> matches) =>
			                           {
				                           if(success) task.SetResult(matches);
				                           else throw new Exception("Listing tasks failed. " + extendedInfo);
			                           });
			return task.Task;
		}

		public Task<bool> JoinMatch(MatchInfoSnapshot match)
		{
			Log.Info("JoinMatch");
			var task = new TaskCompletionSource<bool>();
			m_NetworkMatch.JoinMatch(match.networkId, "", "", "", 0, 0,
			 (bool success, string extendedInfo, MatchInfo matchInfo) =>
			 {
				 if(success)
				 {
					 Log.Info("Join match succeeded");
					 Utility.SetAccessTokenForNetwork(matchInfo.networkId, matchInfo.accessToken);

					 m_MatchJoined = true;
					 m_MatchInfo = matchInfo;

					 Log.Info("Connecting to Address:" + matchInfo.address +
							   " Port:" + matchInfo.port +
							   " NetworKID: " + matchInfo.networkId +
							   " NodeID: " + matchInfo.nodeId);
					 ConnectThroughRelay(matchInfo.address, matchInfo.port, matchInfo.networkId,
										 matchInfo.nodeId);

					 Network.state = NetworkState.Client;
					 task.SetResult(true);
				 }
				 else
					 throw new Exception("Join match failed. " + extendedInfo);
			 });
			return task.Task;
		}

		void SetupHost(bool isServer)
		{
			Log.Info("Initializing network transport");
			NetworkTransport.Init();
			var config = new ConnectionConfig();
			config.DisconnectTimeout = 10000;
			config.AddChannel(QosType.ReliableFragmentedSequenced);
			//config.AddChannel(QosType.Reliable);
			var topology = new HostTopology(config, 4);
			if(isServer)
				m_HostId = NetworkTransport.AddHost(topology, k_ServerPort);
			else
				m_HostId = NetworkTransport.AddHost(topology);
		}

		void StartServer(string relayIp, int relayPort, NetworkID networkId, NodeID nodeId)
		{
			SetupHost(true);

			byte error;
			NetworkTransport.ConnectAsNetworkHost(
			                                      m_HostId, relayIp, relayPort, networkId, Utility.GetSourceID(), nodeId,
			                                      out error);
		}

		void ConnectThroughRelay(string relayIp, int relayPort, NetworkID networkId, NodeID nodeId)
		{
			SetupHost(false);

			byte error;
			NetworkTransport.ConnectToNetworkPeer(
			                                      m_HostId, relayIp, relayPort, 0, 0, networkId, Utility.GetSourceID(), nodeId,
			                                      out error);
		}

		void Update()
		{
			if(m_HostId == -1) return;

			NetworkEventType networkEvent = NetworkEventType.Nothing;
			messagePool.AddRange(Network.incomingMessages);
			Network.incomingMessages.Clear();
			Network.incommingConnections.Clear();

			Message msg = messagePool.PopLast() ?? new Message();

			do
			{
				int hostId, channelId;
				byte error;
				networkEvent = NetworkTransport.ReceiveFromHost(m_HostId, out msg.connection, out channelId,
				                                                msg.content, msg.content.Length, out msg.length,
				                                                out error);
				if((NetworkError) error != NetworkError.Ok)
				{
					Log.Info("Error while receiveing network message: " + (NetworkError) error);
				}

				//networkEvent = NetworkTransport.Receive(out hostId, out msg.connection, out channelId, msg.content, Network.maxMessageSize, out msg.length, out error);

				switch (networkEvent)
				{
					case NetworkEventType.ConnectEvent:
						/*
						Log.Info("ConnectEvent id:"+msg.connection+" local:"+this.localConnection);

						if(msg.connection == this.localConnection)
						{
							Network.state = NetworkState.Client;
						}
						else
						{
						*/
							Log.Info("Adding "+msg.connection+" to connections");
							Network.incommingConnections.Add(msg.connection);
							Network.connections.Ensure(msg.connection);
						//}
						break;

					case NetworkEventType.DataEvent:
						Network.incomingMessages.Add(msg);
						msg = messagePool.PopLast() ?? new Message();
						break;

					case NetworkEventType.DisconnectEvent:
						//Log.Show();
						Log.Info("DisconnectEvent. "+((NetworkError)error));
						Network.connections.Remove(msg.connection);

						if(Network.state == NetworkState.Server)
							NetworkTransport.ConnectAsNetworkHost(m_HostId, m_MatchInfo.address, m_MatchInfo.port, m_MatchInfo.networkId, Utility.GetSourceID(), m_MatchInfo.nodeId, out error);
						else
							NetworkTransport.ConnectToNetworkPeer(m_HostId, m_MatchInfo.address, m_MatchInfo.port, 0, 0, m_MatchInfo.networkId, Utility.GetSourceID(), m_MatchInfo.nodeId, out error);

						break;
				}
			}
			while(networkEvent != NetworkEventType.Nothing);
		}
		/*
		void Update()
		{
			if(m_HostId == -1)
				return;

			var networkEvent = NetworkEventType.Nothing;
			int connectionId;
			int channelId;
			int receivedSize;
			byte error;

			// Get events from the relay connection
			networkEvent = NetworkTransport.ReceiveRelayEventFromHost(m_HostId, out error);
			if(networkEvent == NetworkEventType.ConnectEvent)
				Log.Info("Relay server connected");
			if(networkEvent == NetworkEventType.DisconnectEvent)
				Log.Info("Relay server disconnected");

			do
			{
				// Get events from the server/client game connection
				networkEvent = NetworkTransport.ReceiveFromHost(m_HostId, out connectionId, out channelId,
				                                                m_ReceiveBuffer, (int) m_ReceiveBuffer.Length, out receivedSize,
				                                                out error);
				if((NetworkError) error != NetworkError.Ok)
				{
					Log.InfoError("Error while receiveing network message: " + (NetworkError) error);
				}

				switch(networkEvent)
				{
					case NetworkEventType.ConnectEvent:
					{
						Log.Info("Connected through relay, ConnectionID:" + connectionId +
						          " ChannelID:" + channelId);
						m_ConnectionEstablished = true;
						m_ConnectionIds.Add(connectionId);
						break;
					}
					case NetworkEventType.DataEvent:
					{
						Log.Info("Data event, ConnectionID:" + connectionId +
						          " ChannelID: " + channelId +
						          " Received Size: " + receivedSize);
						m_Reader = new NetworkReader(m_ReceiveBuffer);
						m_LastReceivedMessage = m_Reader.ReadString();
						break;
					}
					case NetworkEventType.DisconnectEvent:
					{
						Log.Info("Connection disconnected, ConnectionID:" + connectionId);
						break;
					}
					case NetworkEventType.Nothing:
						break;
				}
			} while(networkEvent != NetworkEventType.Nothing);
		}
		*/

		public override void Record(BinaryWriter stream)
		{
			Update();
		}

		public override void Playback(EventType type, int index, BinaryReader stream)
		{
		}

		public void SendToConnection(int connection, byte[] msg, int length)
		{
			if(msg.Length == 3)
				Log.Info("Sending "+msg[0]+", "+msg[1]+", "+msg[2]+" to "+connection);
			else
				Log.Info("Sending length "+length+" to "+connection);

			byte error;
			NetworkTransport.Send (m_HostId, connection, 0, msg, length, out error);
			if (error != (byte)NetworkError.Ok) {

				Log.Info("SendToConnection failed: " + error);

			} else {
				Log.Info("SendToConnection successful");
			}
		}
	}
}
