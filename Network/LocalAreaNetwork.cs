using System;
using System.Collections.Generic;
using UnityEngine.Networking;
using System.IO;
using UnityEngine;
using Random = UnityEngine.Random;

namespace PuzzleCore
{
	enum NetworkState
	{
		None,
		Discovering,
		Connecting,
		Client,
		Server,
	}

	class Message
	{
		public byte[] content = new byte[Network.maxMessageSize];
		public int length;
		public int connection;
	}


	class LocalAreaNetwork : EventStreamInput
	{
		// Constants
		const int discoveryPort = 39010;
		const int port = 39011;
		const int broadcastKey = 1000;
		const int broadcastVersion = 1;
		const int broadcastSubVersion = 1;

		int discoveryHost = -1;
		int host = -1;
		int localConnection = -1;
		float startServerTime;
		List<Message> messagePool = new List<Message>();

		static byte[] GetBytes(string str)
		{
			byte[] bytes = new byte[str.Length * sizeof(char)];
			Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
			return bytes;
		}

		static string GetString(byte[] bytes)
		{
			char[] chars = new char[bytes.Length / sizeof(char)];
			Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
			return new string(chars);
		}

		public LocalAreaNetwork()
		{
			if(!NetworkTransport.IsStarted)
				NetworkTransport.Init();

			ConnectionConfig discoveryConfig = new ConnectionConfig();
			discoveryConfig.AddChannel(QosType.Unreliable);
			discoveryHost = NetworkTransport.AddHost(new HostTopology(discoveryConfig, 1), discoveryPort);

			byte error;
			NetworkTransport.SetBroadcastCredentials(discoveryHost, broadcastKey, broadcastVersion, broadcastSubVersion, out error);
			startServerTime = Time.time + 5f;
			Network.state = NetworkState.Discovering;
			Log.Info("Discovery: Seaching for server");
		}

		void StartDiscoveryServer()
		{
			NetworkTransport.RemoveHost(discoveryHost);
			ConnectionConfig cc = new ConnectionConfig();
			cc.AddChannel(QosType.Unreliable);
			discoveryHost = NetworkTransport.AddHost(new HostTopology(cc, 1), 0);

			byte err;
			byte[] msgOutBuffer = new byte[]{ 0, 0, 0 };
			if(!NetworkTransport.StartBroadcastDiscovery(discoveryHost, discoveryPort, broadcastKey, broadcastVersion,
														 broadcastSubVersion, msgOutBuffer, msgOutBuffer.Length, 1000, out err))
			{
				Log.Info("Discovery failed: " + err);
				return;
			}

			Network.state = NetworkState.Server;
			Log.Info("Discovery: Starting server");

			// Standard server socket
			ConnectionConfig config = new ConnectionConfig();
			config.AddChannel(QosType.ReliableFragmented);
			host = NetworkTransport.AddHost(new HostTopology(config, 4), port);
		}

		void OnDestroy()
		{
			if(Network.state == NetworkState.Server) NetworkTransport.StopBroadcastDiscovery();
			NetworkTransport.RemoveHost(discoveryHost);
			discoveryHost = -1;
		}

		void Update()
		{
			NetworkEventType networkEvent = NetworkEventType.Nothing;
			messagePool.AddRange(Network.incomingMessages);
			Network.incomingMessages.Clear();
			Network.incommingConnections.Clear();

			if(Network.state == NetworkState.Discovering && Time.time > startServerTime)
				StartDiscoveryServer();

			Message msg = messagePool.PopLast() ?? new Message();

			do
			{
				int hostId, channelId;
				byte error;
				networkEvent = NetworkTransport.Receive(out hostId, out msg.connection, out channelId, msg.content, Network.maxMessageSize, out msg.length, out error);

				switch (networkEvent)
				{
					case NetworkEventType.ConnectEvent:
						Log.Info("ConnectEvent id:"+msg.connection+" local:"+this.localConnection);

						if(msg.connection == this.localConnection)
						{
							Network.state = NetworkState.Client;
						}
						else
						{
							Log.Info("Adding "+msg.connection+" to connections");
							Network.incommingConnections.Add(msg.connection);
							Network.connections.Ensure(msg.connection);
						}
						break;

					case NetworkEventType.DataEvent:
						Network.incomingMessages.Add(msg);
						msg = messagePool.PopLast() ?? new Message();
						break;

					case NetworkEventType.DisconnectEvent:
						Log.Info("remote client event disconnected");
						Network.connections.Remove(msg.connection);
						break;

					case NetworkEventType.BroadcastEvent:
						NetworkTransport.GetBroadcastConnectionMessage(discoveryHost, msg.content, Network.maxMessageSize, out msg.length, out error);

						string senderAddr;
						int senderPort;
						NetworkTransport.GetBroadcastConnectionInfo(discoveryHost, out senderAddr, out senderPort, out error);

						ConnectToServer(senderAddr, GetString(msg.content));
						break;
				}
			} while(networkEvent != NetworkEventType.Nothing);
		}

		void SendToConnection(int connection, byte[] msg, int length)
		{
			if(msg.Length == 3)
				Log.Info("Sending "+msg[0]+", "+msg[1]+", "+msg[2]+" to "+connection);
			else
				Log.Info("Sending length "+length+" to "+connection);

			byte error;
			NetworkTransport.Send (host, connection, 0, msg, length, out error);
			if (error != (byte)NetworkError.Ok) {

				Log.Info("SendToConnection failed: " + error);

			} else {
				Log.Info("SendToConnection successful");
			}
		}

		void ConnectToServer(string fromAddress, string data)
		{
			Log.Info("Got broadcast from " + fromAddress);
			startServerTime = float.PositiveInfinity;
			if(Network.state != NetworkState.Discovering) return;

			NetworkTransport.RemoveHost(discoveryHost);

			// Standard client socket
			ConnectionConfig config = new ConnectionConfig();
			config.AddChannel(QosType.ReliableFragmented);
			host = NetworkTransport.AddHost(new HostTopology(config, 4), 0);

			byte error;
			localConnection = NetworkTransport.Connect(host, fromAddress, port, 0, out error);
			Network.connections.Add(localConnection);

			if (error != (byte)NetworkError.Ok) {

				Log.Info("ConnectToServer failed: " + localConnection);

			} else {
				Network.state = NetworkState.Connecting;
				Log.Info("Connecting to server. ConnectionID: " + localConnection);
			}
		}

		public override void Record(BinaryWriter stream)
		{
			Update();
		}

		public override void Playback(EventType type, int index, BinaryReader stream)
		{
		}
	}

}
