﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine.Networking.Match;

namespace PuzzleCore
{
	class Network
	{
		public const int maxMessageSize = 1024*32;

		public static RelayNetwork instance;

		public static List<Message> incomingMessages = new List<Message>();
		public static List<int> incommingConnections = new List<int>();
		public static List<int> connections = new List<int>();
		public static NetworkState state = NetworkState.None;

		static byte[] outgoingBuffer = new byte[Network.maxMessageSize];

		public static void Send(int connection, byte[] bytes)
		{
			Log.Info("Network.Send len:"+bytes.Length+" connection:"+connection);
			instance.SendToConnection(connection, bytes, bytes.Length);
		}

		public static void Send(params byte[] bytes)
		{
			Log.Info("Network.Send len:"+bytes.Length+" connections.Count:"+connections.Count);
			for(int i = 0; i < connections.Count; i++)
				instance.SendToConnection(connections[i], bytes, bytes.Length);
		}

		public static void Send(byte type, ArraySegment<byte> msg)
		{
			Log.Info("Network.Send len:"+msg.Count+" connections.Count:"+connections.Count);
			outgoingBuffer[0] = type;
			Array.Copy(msg.Array, msg.Offset, outgoingBuffer, 1, msg.Count);
			for(int i = 0; i < connections.Count; i++)
				instance.SendToConnection(connections[i], outgoingBuffer,  msg.Count+1);
		}

		public static void Send(int connection, byte type, ArraySegment<byte> msg)
		{
			Log.Info("Network.Send len:"+msg.Count+" to connection "+connection);
			outgoingBuffer[0] = type;
			Array.Copy(msg.Array, msg.Offset, outgoingBuffer, 1, msg.Count);
			instance.SendToConnection(connection, outgoingBuffer,  msg.Count+1);
		}

		public static void SendToEverybodyElse(Message msg)
		{
			for(int i = 0; i < connections.Count; i++)
			{
				var connection = connections[i];
				if(connection != msg.connection)
					instance.SendToConnection(connections[i], msg.content, msg.length);
			}
		}

		public static Task CreateMatch() => instance.CreateMatch();
		public static Task<List<MatchInfoSnapshot>> ListMatches() => instance.ListMatches();
		public static Task<bool> JoinMatch(MatchInfoSnapshot match) => instance.JoinMatch(match);
	}
}
