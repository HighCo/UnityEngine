﻿using System.Collections.Generic;

namespace PuzzleCore
{
	public class UpdateManager
	{
		static List<IUpdatable> updatables = new List<IUpdatable>();
		static List<IUpdatable> updatablesToRemove = new List<IUpdatable>();
		static List<IUpdatable> updatablesToAdd = new List<IUpdatable>();

		public static void Update()
		{
			if (updatablesToRemove.Count > 0)
			{
				for (int i = 0; i < updatablesToRemove.Count; i++)
					updatables.Remove(updatablesToRemove[i]);
				updatablesToRemove.Clear();
			}

			if (updatablesToAdd.Count > 0)
			{
				for (int i = 0; i < updatablesToAdd.Count; i++)
					updatables.Add(updatablesToAdd[i]);
				updatablesToAdd.Clear();
			}

			for (int i = 0; i < updatables.Count; i++)
				updatables[i].Update();
		}

		public static void AddUpdatable (IUpdatable updatable)
		{
			updatablesToAdd.Add(updatable);
		}

		public static void RemoveUpdatable (IUpdatable updatable)
		{
			updatablesToRemove.Add(updatable);
		}

		public static void ClearUpdatables (bool immediately)
		{
			if (immediately)
				updatables.Clear();
			else
				updatablesToRemove.AddRange(updatables);
		}
	}
}
