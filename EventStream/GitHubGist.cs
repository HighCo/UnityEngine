﻿using System.Text;
using UnityEngine;

namespace PuzzleCore
{
	public static class GitHubGist
	{
		public class Response
		{
			public string html_url;
		}
		
		public static string GetRawUrlForText(string text, string name)
		{
			string result = null;
			
			var requestDataJson = string.Format(
				"{{" +
				"	\"public\":false," +
				"	\"files\":{{" +
				"		\"{0}\":{{" +
				"			\"content\":\"{1}\"" +
				"		}}" +
				"	}}" +
				"}}",
				name, EncodeStringForJson(text));

			System.Net.ServicePointManager.ServerCertificateValidationCallback = (xsender, xcertificate, xchain, xerrors) => true;
			using (var client = new System.Net.WebClient())
			{
				try
				{
					client.Headers["User-Agent"] = "PuzzleCoreClient";
					var responseString = client.UploadString("https://api.github.com/gists", requestDataJson);

					var response = JsonUtility.FromJson<Response>(responseString);
					result = response.html_url;
				}
				catch (System.Exception e)
				{
					Debug.LogError("Failed to create a gist: " + e);
				}
			}

			if (!string.IsNullOrEmpty(result))
			{
				result = result.Replace("https://gist.github.com/", "https://gist.githubusercontent.com/anonymous/");
				result += "/raw";
			}
			
			return result;
		}
		
		static string EncodeStringForJson(string value)
		{
			if (string.IsNullOrEmpty(value))
				return string.Empty;

			int len = value.Length;
			bool needEncode = false;
			char c;
			for (int i = 0; i < len; i++)
			{
				c = value[i];

				if (c >= 0 && c <= 31 || c == 34 || c == 39 || c == 60 || c == 62 || c == 92)
				{
					needEncode = true;
					break;
				}
			}

			if (!needEncode)
				return value;

			var sb = new StringBuilder();

			for (int i = 0; i < len; i++)
			{
				c = value[i];
				if (c >= 0 && c <= 7 || c == 11 || c >= 14 && c <= 31 || c == 39 || c == 60 || c == 62)
					sb.AppendFormat("\\u{0:x4}", (int)c);
				else switch ((int)c)
				{
					case 8:
						sb.Append("\\b");
						break;

					case 9:
						sb.Append("\\t");
						break;

					case 10:
						sb.Append("\\n");
						break;

					case 12:
						sb.Append("\\f");
						break;

					case 13:
						sb.Append("\\r");
						break;

					case 34:
						sb.Append("\\\"");
						break;

					case 92:
						sb.Append("\\\\");
						break;

					default:
						sb.Append(c);
						break;
				}
			}

			return sb.ToString();
		}
	}
}