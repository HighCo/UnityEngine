﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

public class TimelinePlayToolbarHelper
{
    Hashtable hashedPlayButtons = new Hashtable();
    Hashtable timelinePlayButtons = new Hashtable();

    Hashtable guiContents;
    
    public TimelinePlayToolbarHelper()
    {
        guiContents = (Hashtable) typeof(EditorGUIUtility)
            .GetField("s_IconGUIContents",
                BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static)
            .GetValue(null);
    }
    
    public void SetPlaybackMode(bool enable)
    {
        var t = (GUIContent) guiContents[(object) "PlayButton"];

        if (t == null || t.image == null) return;

        var hasPlaybackIcons = t.image.name == "PlaybackPlayButton";

        var buttons = new List<string>
        {
            "PlayButton",
            "PlayButton On",
            "PauseButton",
            "PauseButton On",
            "StepButton",
            "StepButton On",
        };
    
        // Hash normal play buttons if not done before
        if (hashedPlayButtons.Count == 0)
            foreach (var button in buttons)
                hashedPlayButtons[button] = guiContents[button];

        if (timelinePlayButtons.Count == 0)
        {
            var playButton = GetContent("PlaybackPlayButton");
            timelinePlayButtons["PlayButton"] = playButton;
            timelinePlayButtons["PlayButton On"] = playButton;
    
            var pauseButton = GetContent("PlaybackPauseButton");
            timelinePlayButtons["PauseButton"] = pauseButton;
            timelinePlayButtons["PauseButton On"] = pauseButton;
    
            var stepButton = GetContent("PlaybackStepButton");
            timelinePlayButtons["StepButton"] = stepButton;
            timelinePlayButtons["StepButton On"] = stepButton;
        }

        var sync = false;

        if (!hasPlaybackIcons && enable)
        {
            foreach (var button in buttons)
                guiContents[button] = timelinePlayButtons[button];
            sync = true;
        }

        if (hasPlaybackIcons && !enable)
        {
            foreach (var button in buttons)
                guiContents[button] = hashedPlayButtons[button];

            sync = true;
        }

        if (sync)
        {
            var staticToolbar = new InternalClassWrapper();
            staticToolbar.InitStatic("UnityEditor.Toolbar");
            var playIcons = (GUIContent[]) staticToolbar.GetField("s_PlayIcons");

            playIcons[0] = (GUIContent) guiContents["PlayButton"];
            playIcons[4] = (GUIContent) guiContents["PlayButton On"];
    
            playIcons[1] = (GUIContent) guiContents["PauseButton"];
            playIcons[5] = (GUIContent) guiContents["PauseButton On"];
    
            playIcons[2] = (GUIContent) guiContents["StepButton"];
            playIcons[6] = (GUIContent) guiContents["StepButton On"];
            
            staticToolbar.InvokeMethod("RepaintToolbar");
        }
    }
    
    
    GUIContent GetContent(string name)
    {
        var eventStreamWindow = AssetDatabase.FindAssets("EventStreamWindow");
        var basePath = AssetDatabase.GUIDToAssetPath(eventStreamWindow[0]);
        return new GUIContent(AssetDatabase.LoadAssetAtPath<Texture>(basePath + "/Icons/" + name + ".png"));
    }
}