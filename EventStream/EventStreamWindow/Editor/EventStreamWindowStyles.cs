﻿using UnityEditor;
using UnityEngine;

public partial class EventStreamWindow
{
	class EventStreamWindowStyles
	{
		public GUIStyle layerInfoStyle;
		public GUIStyle timeObjectLabelStyle;
		public GUIContent onlineIcon;
		public GUIContent fileIcon;
		public GUIContent timelineIcon;
		public GUIStyle onlineIconStyle;
		public GUIStyle fileIconStyle;
		public GUIStyle descriptionTextAreaStyle;


		// Indexed by TimelineMark.Type
		public Color[] markColors =
		{
			new Color(0, 0.64f, 0.61f, 0.8f),
			new Color(1, 0.44f, 0.48f, 0.8f),
			new Color(.93f, 0.99f, 0.65f, 0.8f),
		};

		public Color noteBackgroundColor = new Color(0.1137255f, 0.1254902f, 0.1294118f);
		public Color playheadColor = new Color(.8f, .8f, .8f, 1);

		public Texture2D iconWarningSmall = (Texture2D) EditorGUIUtility.Load("console.warnicon.sml");

		public GUIStyle noteBackgroundStyle = new GUIStyle("sequenceClip");
		public GUIStyle noteFontStyle = new GUIStyle("tinyFont");

		public GUIStyle blockSelectedStyle = new GUIStyle("flow node 0 on");
		public GUIStyle blockStyle = new GUIStyle("flow node 0");

		public GUIStyle headerCursorStyle = new GUIStyle("Icon.TimeCursor");

		public EventStreamWindowStyles()
		{
			var s = new GUIStyle("MeTimeLabel");

			timeObjectLabelStyle = new GUIStyle
			{
				font = s.font,
				alignment = TextAnchor.MiddleLeft,
				fontSize = 10,
				clipping = TextClipping.Clip
			};
			timeObjectLabelStyle.normal.textColor = new Color(1, 1, 1, .7f);

			layerInfoStyle = new GUIStyle(timeObjectLabelStyle);
			layerInfoStyle.fontSize = 11;
			onlineIcon = GetContent("internet");
			fileIcon = GetContent("file");
			timelineIcon = GetContent("timeline");

			onlineIconStyle = new GUIStyle(EditorStyles.toolbarButton);
			onlineIconStyle.padding = new RectOffset(4,4,4,4);
			fileIconStyle = new GUIStyle(EditorStyles.toolbarButton);
			fileIconStyle.padding = new RectOffset(2,2,2,2);

			descriptionTextAreaStyle = new GUIStyle("CN Box");
			descriptionTextAreaStyle.focused.background = null;
			descriptionTextAreaStyle.padding = new RectOffset(2,2,2,2);
			descriptionTextAreaStyle.wordWrap = true;
		}

		GUIContent GetContent(string name)
		{
			var eventStreamWindow = AssetDatabase.FindAssets("EventStreamWindow");
			var basePath = AssetDatabase.GUIDToAssetPath(eventStreamWindow[0]);
			var texture = AssetDatabase.LoadAssetAtPath<Texture>(basePath + "/Icons/" + name + ".png");
			return new GUIContent(texture);
		}
	}
}