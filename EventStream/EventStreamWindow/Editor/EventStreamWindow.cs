﻿using System;
using System.Collections.Generic;
using System.Linq;
using PuzzleCore;
using PuzzleCoreEditor;
using UnityEditor;
using UnityEngine;
using UnityEditor.IMGUI.Controls;

public partial class EventStreamWindow : EditorWindow
{
	enum DragState
	{
		PlayHead,
		TimeArea,
		Mark,
		None
	};

	[Serializable]
	class TimelineSettings
	{
		public bool locked;
		public float visionWidth = 12;
	}

	const float maxZoomInScale = 1200;
	const int layerHeight = 40;

	public TimelineInstance timelineInstance = new TimelineInstance();

	TimelineInstance previousTimelineInstance;

	Rect blocksAreaRect;

	float playHeadAtTime = 0;
	DragState dragState;
	TimelineSettings settings = new TimelineSettings();
	int controlId;
	float minTime = 0;
	float maxTime = 60;
	Engine engine = null;

	TimeObject selectedTimeObject;
 	float lastRefreshTime = 0;
	float playTime;

	TimelinePlayToolbarHelper toolbarHelper;

	EventStreamWindowStyles styles;
	Color[] logColors = new[] { new Color(0,.7f,.7f,1), new Color(0,.7f,0,1), new Color(.7f,0,0,1), new Color(0,0,.7f,1), new Color(.7f,.7f,.7f,1)  };

	[SerializeField] SplitterWrapper horizontalSplitter;
	[SerializeField] SplitterWrapper verticalSplitter;
	[SerializeField] TimeAreaWrapper timeArea;

	SearchField searchField;
	[SerializeField] string searchQuery;

	bool wasPaused = false;
	TimelineMark selectedMark = null;


	[MenuItem("Puzzle Core/EventStream/Open Window %#e")]
	static void Init()
	{
		var consoleType = typeof(EditorGUILayout).Assembly.GetType("UnityEditor.ConsoleWindow");
		var window = GetWindow<EventStreamWindow>(consoleType);
		window.position = new Rect(0f, 0f, 700, 150);
		window.Show();
	}

	void Awake()
	{
		EditorPrefs.SetString(EnginePrefs.EnginePrefsKey, "");
	}

	void OnEnable()
	{
		EditorApplication.update += OnUpdate;
		EditorApplication.playModeStateChanged += OnPlaymodeChanged;

		this.controlId = GUIUtility.GetControlID(FocusType.Passive);

		// Only recreate the time area if needed, otherwise just refresh it
		RecreateTimeArea(force:false);

		if (horizontalSplitter == null)
		{
			horizontalSplitter = new SplitterWrapper();
			horizontalSplitter.Init(new[] { 20f, 80f}, new[] { 180, 100});
		}
		horizontalSplitter.RefreshState();

		if (verticalSplitter == null)
		{
			verticalSplitter = new SplitterWrapper();
			verticalSplitter.Init(new[] { 80f, 20f}, new[] { 30, 30});
		}
		verticalSplitter.RefreshState();

		lastRefreshTime = 0;

		toolbarHelper = new TimelinePlayToolbarHelper();
		Repaint();
	}

	void OnDestroy()
	{
		EnginePrefs.instance.recordMode = true;
		toolbarHelper.SetPlaybackMode(false);
		EnginePrefs.Save();
	}

	void RecreateTimeArea(bool force = true)
	{
		// If in playmode, ensure that the scale stays the same as we create a new time area
		var hadTimeArea = timeArea != null && !EnginePrefs.instance.recordMode;
		var scale = hadTimeArea ? timeArea.scale : Vector2.zero;

		if (timeArea == null || force)
		{
			timeArea = new TimeAreaWrapper();
			timeArea.Init(minTime, maxTime, 5, maxZoomInScale);
		}

		timeArea.RefreshState();

		if(hadTimeArea)
			timeArea.SetTransform(timeArea.translation, scale);

		selectedMark = null;
		Repaint();
	}

	void OnDisable()
	{
		EditorApplication.update -= OnUpdate;
		EditorApplication.playModeStateChanged -= OnPlaymodeChanged;
	}

	void OnPlaymodeChanged(PlayModeStateChange change)
	{
		var enteringPlaymode = EditorApplication.isPlaying && !EditorApplication.isPaused && !wasPaused;
		if (enteringPlaymode)
			settings.locked = true;

		// Don't lock when not playing
		if (!EditorApplication.isPlaying || EditorApplication.isPaused)
			settings.locked = false;

		wasPaused = EditorApplication.isPaused;
	}

	void OnUpdate()
	{
		// Only repaint when the data is constantly changing
		if ((EditorApplication.isPlaying && !EditorApplication.isPaused) || EditorApplication.isPlayingOrWillChangePlaymode)
		{
			if (Time.realtimeSinceStartup - lastRefreshTime > 1f/60)
			{
				Repaint();
				lastRefreshTime = Time.realtimeSinceStartup;
			}
		}

		toolbarHelper.SetPlaybackMode(!EnginePrefs.instance.recordMode);
	}

	void OnGUI()
	{
		// Keep playtime on whatever the last value was, untill
		if(Engine.time != 0)
			playTime = Engine.time;

		// The app is playing, but user input not enabled yet - set playTime to Time.time for the sake
		// of visualization in the timeline
		if (Application.isPlaying && Engine.time == 0)
			playTime = Time.time;

		if(styles == null)
		{
			styles = new EventStreamWindowStyles();
			titleContent = styles.timelineIcon;
			titleContent.text = "Event Stream";
		}

		if(Application.isPlaying && engine == null)
		{
			engine = FindObjectOfType<Engine>();
			if(engine != null) timelineInstance = engine.timeline;
		}

		Draw();
	}

	bool HasAnythingToDraw()
	{
		return timelineInstance.layers.Count > 0;
	}

	[MenuItem("Puzzle Core/EventStream/Toggle mode %#m")]
	static void ToggleMode() {
		GetWindow<EventStreamWindow>().Toggle();
	}

	bool toggledMode = false;

	public void Toggle()
	{
		toggledMode = true;
		Repaint();
	}

	void DrawTopToolbar()
	{
		var enginePrefs = EnginePrefs.instance;
		EditorGUI.BeginChangeCheck();
		GUILayout.BeginHorizontal("ToolBar");

		var playContent = EditorGUIUtility.IconContent("Animation.Play", "|Play an eventstream.");
		var recordContent = EditorGUIUtility.IconContent("Animation.Record", "|Record the game for the eventstream.");

		var wasRecording = enginePrefs.recordMode;

		var recordToggleResult = GUILayout.Toggle(enginePrefs.recordMode, recordContent, EditorStyles.toolbarButton, GUILayout.ExpandWidth(false));
		var playToggleResult = !GUILayout.Toggle(!enginePrefs.recordMode, playContent, EditorStyles.toolbarButton, GUILayout.ExpandWidth(false));

		DrawNonderminismWarning();

		GUILayout.FlexibleSpace();
		if(GUILayout.Button(styles.fileIcon, styles.fileIconStyle, GUILayout.Width(25)))
		{
			enginePrefs.playbackPath = EditorUtility.OpenFilePanel("Select EventStream", Application.persistentDataPath, "eventstream");
			if(!string.IsNullOrEmpty(enginePrefs.playbackPath))
			{
				var reader = Engine.OpenFileStream(enginePrefs.playbackPath);
				timelineInstance = new TimelineInstance(reader);
				enginePrefs.recordMode = false;
			}
		}
		if(GUILayout.Button(styles.onlineIcon, styles.onlineIconStyle, GUILayout.Width(25)))
		{
			var rect = GUILayoutUtility.GetLastRect();
			PopupWindow.Show(rect, new EnterStringPopup(url =>
			{
				var reader = Engine.OpenUrlStream(url);
				timelineInstance = new TimelineInstance(reader);

				enginePrefs.playbackURL = url;
				enginePrefs.recordMode = false;
				EnginePrefs.Save();
			}, "Paste eventstream URL here", "URL", "Open"));
		}
		var playChanged = playToggleResult != wasRecording || (wasRecording && toggledMode);
		var recordChanged = recordToggleResult != wasRecording || (!wasRecording && toggledMode);


		if (playChanged || recordChanged)
		{
			if (playChanged)
			{
				enginePrefs.recordMode = false;

				if(timelineInstance != null)
				{
					enginePrefs.playbackPath = timelineInstance.path;
				}
				else
				if (!string.IsNullOrEmpty(enginePrefs.playbackPath))
				{
					var reader = Engine.OpenFileStream(enginePrefs.playbackPath);
					timelineInstance = new TimelineInstance(reader);
				}
				else
					enginePrefs.recordMode = true;

			}
			else if(recordChanged)
			{
				enginePrefs.recordMode = true;
				enginePrefs.playbackPath = "";
				enginePrefs.timeScale = 1;
				enginePrefs.fastforwardTo = 0;
				timelineInstance = new TimelineInstance();
			}
			Repaint();
		}

		GUILayout.EndHorizontal();

		if (EditorGUI.EndChangeCheck() || toggledMode)
			EnginePrefs.Save();

		toggledMode = false;
	}

	void DrawNonderminismWarning()
	{
		var warningCount = timelineInstance.nonDeterminismWarnings.Count;
		if (warningCount > 0)
		{
			var nondeterminismWarnings = timelineInstance.nonDeterminismWarnings.Aggregate((x, y) => x + "\n" + y);
			if (GUILayout.Button(new GUIContent(warningCount.ToString(), styles.iconWarningSmall,
				"Detected non-deterministic behaviour, press to output log."), EditorStyles.toolbarButton))
			{
				Debug.Log(nondeterminismWarnings);
			}
		}
	}

	void DrawBottomToolbar()
	{
		GUILayout.BeginHorizontal("ToolBar");

		if(searchField == null)
			searchField = new SearchField();

		var rect = GUILayoutUtility.GetRect(1, 1, GUILayout.ExpandHeight(true), GUILayout.MinWidth(80),
			GUILayout.MaxWidth(Mathf.Min(GetLayerInfoWidth() * .6f, 250)));
		searchQuery = searchField.OnToolbarGUI(rect, searchQuery);

		GUILayout.FlexibleSpace();

		settings.locked = GUILayout.Toggle(settings.locked, "Lock", "ToolbarButton", GUILayout.Width(50));

		GUILayout.EndHorizontal();
	}

	void Draw()
	{
		if (settings.locked)
			EnforceLock();

		bool recreate = false;

		//Setup default view in case the minTime hasn't be updated yet with eventStreamStartedTime
		if(engine != null && minTime != engine.eventStreamStartedTime)
		{
			minTime = engine.eventStreamStartedTime;
			recreate = true;
		}

		//Setup default view in case we have a new instance
		if(previousTimelineInstance == null || timelineInstance.timestamp != previousTimelineInstance.timestamp)
			recreate = true;

		if(recreate)
		{
			SetupForNewInstance();
			RecreateTimeArea();
			previousTimelineInstance = timelineInstance;
		}

		var showDescription = ShouldShowDescription();

		horizontalSplitter.BeginHorizontal();

			GUILayout.BeginHorizontal();
				GUILayout.BeginVertical();

					DrawTopToolbar();
					if (showDescription)
						verticalSplitter.BeginVertical();

					var layerInfoRect = GUILayoutUtility.GetRect(0,0, GUILayout.MinWidth(150), GUILayout.ExpandHeight(true));
						DrawLayerInfo(layerInfoRect);

					GUILayout.BeginVertical();
						DrawBottomToolbar();
						if(showDescription)
						{
							var descriptionRect = GUILayoutUtility.GetRect(0, 300, GUILayout.MinWidth(150));
							DrawDescription(descriptionRect);
						}
					GUILayout.EndVertical();

					if (showDescription)
						verticalSplitter.EndVertical();

				GUILayout.EndVertical();
			GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();

		DrawContent();

		GUILayout.EndHorizontal();

		horizontalSplitter.EndHorizontal();

		// Draw lines
		var width = GetLayerInfoWidth();

		//shadow
		EditorLineDrawing.DrawLine(new Vector2(width+2, 0), new Vector2(width+2, this.position.height),
			new Color(.16f, .16f, .16f, .5f), 3, true);
		//line
		EditorLineDrawing.DrawLine(new Vector2(width, 0), new Vector2(width, this.position.height),
			new Color(.16f, .16f, .16f, 1), 3, false);
	}

	bool ShouldShowDescription()
	{
		return selectedTimeObject != null && !string.IsNullOrEmpty(selectedTimeObject.text);
	}

	float GetLayerInfoWidth()
	{
		return horizontalSplitter.GetSplittedSizes()[0];
	}

	float GetTimeObjectsWidth()
	{
		return horizontalSplitter.GetSplittedSizes()[1];
	}

	void SetupForNewInstance()
	{
		if(timelineInstance == null || timelineInstance.layers.Count == 0) return;

		// Find the highest time object, plus a little for buffer
		maxTime = timelineInstance.layers.Max(a => a.Value.timeObjects.Max(t => t.time)) + 10;
		searchQuery = "";
		Repaint();
	}

	void DrawDescription(Rect r)
	{
		Handles.color = new Color(.16f, .16f, .16f, 1);
		Handles.DrawLine(new Vector2(r.min.x, r.min.y), new Vector2(r.max.x, r.min.y));
		Handles.color = Color.white;

		GUI.backgroundColor = new Color(0.5f, 0.5f, 0.5f, .6f);
		GUI.Box(r, "", "TextField");
		GUI.backgroundColor = Color.white;
		GUI.TextArea(r, selectedTimeObject.description, styles.descriptionTextAreaStyle);
	}

	void DrawLayerInfo(Rect rect)
	{
		GUI.Box(rect, "");

		GUI.BeginClip(rect);

		var translation = timeArea.translation;
		var scale = timeArea.scale;

		int i = 0;
		foreach(var timelineLayer in GetActiveLayers())
		{
			rect.y = i++ * 40 + (translation.y / scale.y);
			rect.height = 38;
			GUI.Box(rect, "", "flow node 0");
			GUI.Label(rect, "  " + timelineLayer.Value.name, styles.layerInfoStyle);
		}
		GUI.EndClip();
	}

	float GetTotalLayersHeight()
	{
		return timelineInstance.layers.Count * layerHeight + 20 + layerHeight;
	}

	void EnforceLock()
	{
		const float lookAheadPercentage = .3f;

		float xMin;
		float xMax;
		GetTimeBounds(out xMin, out xMax);

		float diffViewTime = xMax - xMin;

		var left = playTime - diffViewTime * (1-lookAheadPercentage);

		var translation = timeArea.translation;
		var scale = timeArea.scale;

		translation.x = -(left * scale.x);

		timeArea.SetTransform(translation, scale);
	}

	void GetTimeBounds(out float xMin, out float xMax)
	{
		var translation = timeArea.translation;
		var scale = timeArea.scale;

		xMin = -translation.x / scale.x;
		var width = GetTimeObjectsWidth();

		xMax = timeArea.PixelToTime(timeArea.TimeToPixel(xMin) + width);
	}

	void DrawContent()
	{
		var rect = GUILayoutUtility.GetRect(GetTimeObjectsWidth(), 300);
		EditorGUI.BeginDisabledGroup(!HasAnythingToDraw());

		var layerInfoWidth = GetLayerInfoWidth();

		blocksAreaRect = rect;
		blocksAreaRect.height = position.height; // ignore description, handle clipping ourselves
		blocksAreaRect.y += 20; // adjusting it to ignore the time ruler
		blocksAreaRect.height -= 20;
		// Only update when repainting, otherwise in layout, it's not set and (mouse) input won't work
		if (Event.current.type == UnityEngine.EventType.Repaint)
			timeArea.rect = blocksAreaRect;

		playHeadAtTime = playTime - 0.01f;

		if (Application.isPlaying)
		{
			// Always allow to look forward for a second
			if(EnginePrefs.instance.recordMode)
				maxTime = Engine.time + 60;
			else
				maxTime = timelineInstance.length + 10;
		}

		timeArea.UpdateAvailableHorizontalRange(minTime + maxTime);
		timeArea.UpdateAvailableVerticalRange(GetTotalLayersHeight());

		//Ensure the left side always starts on zero
		var t = timeArea.translation;
		if (t.x > 0)
		{
			var scale = timeArea.scale;
			t.x = 0;

			var aimedPixelWidth = Mathf.Max(scale.x * maxTime);

			if(aimedPixelWidth < blocksAreaRect.width)
				scale.x =  blocksAreaRect.width / maxTime;

			timeArea.SetTransform(t, scale);
		}

		timeArea.BeginViewGUI();

		blocksAreaRect.y -= 20; // decrease to adjust back for the time ruler
		blocksAreaRect.height += 20; // decrease to adjust back for the time ruler
		var headerClipRect = blocksAreaRect;
		GUI.BeginClip(headerClipRect); // Set origin to (0,0)

		// Actual drawing area
		blocksAreaRect = new Rect(0,0, position.width - layerInfoWidth, position.height);
		timeArea.SetVisualArea(blocksAreaRect);

		var header = blocksAreaRect;
		header.y = 0;
		header.height = 20;

		GUI.Box(header, "", "MeTransitionHead");

		var playHeadPixel = timeArea.TimeToPixel(playHeadAtTime);
		var ticksRect = blocksAreaRect;
		ticksRect.y -= 1;
		timeArea.DrawMajorTicks(ticksRect, 60f);

		ticksRect.height = 20;
		timeArea.TimeRuler(ticksRect, 60f);

		GUI.EndClip();

		var contentClipRect = headerClipRect;
		contentClipRect.y += 20;

		GUI.BeginClip(contentClipRect);

		var current = Event.current;

		var mousePosition = current.mousePosition;

		if ((Event.current.type != UnityEngine.EventType.Layout && Event.current.type != UnityEngine.EventType.Repaint) &&
			(
				(current.rawType == UnityEngine.EventType.MouseDown && ticksRect.Contains(mousePosition)) // Relocating the playHead
				//|| (Event.current.type != UnityEngine.EventType.MouseDrag && current.delta != Vector2.zero) // Zooming in/out
			))
			settings.locked = false;

		var touchRect = blocksAreaRect;
		touchRect.y -= 20;
		bool activeMarkDoubleClicked = false;

		if (current.rawType == UnityEngine.EventType.MouseDown)
			selectedMark = null;

		if (current.rawType == UnityEngine.EventType.MouseDown && touchRect.Contains(mousePosition))
		{
			GUIUtility.hotControl = controlId;

			foreach (var mark in timelineInstance.marks)
			{
				var markRect = GetMarkRect(mark);
				markRect.y -= 20;

				// Make it easier to select
				markRect.x -=5;
				markRect.width += 10;

				if(current.rawType == UnityEngine.EventType.MouseDown && markRect.Contains(current.mousePosition))
				{
					current.Use();
					selectedMark = mark;
					dragState = DragState.Mark;

					if(current.clickCount > 1)
						activeMarkDoubleClicked = true;
				}
			}

			if (mousePosition.y < 0 && selectedMark == null)
			{
				// Right click for context menu, or double click for fast-forward directly

				var mousePos = mousePosition;
				if(current.button == 1)
					ShowMarkContextMenu(timeArea.PixelToTime(mousePos.x));
				else if(current.clickCount > 1)
					AddMark(timeArea.PixelToTime(mousePos.x), TimelineMark.Type.Fastforward);

				current.Use();

			}
		}
		else if (current.rawType == UnityEngine.EventType.MouseDown && touchRect.Contains(mousePosition))
		{
			GUIUtility.hotControl = controlId;

			dragState = DragState.TimeArea;
		}

		if (current.rawType == UnityEngine.EventType.MouseUp)
			if(GUIUtility.hotControl == this.controlId)
			{
				dragState = DragState.None;
				GUIUtility.hotControl = 0;
				Repaint();
			}

		if (Event.current.type == UnityEngine.EventType.MouseDrag)
		{
			switch (dragState)
			{
				case DragState.PlayHead:
					break;
				case DragState.Mark:

					selectedMark.time = timeArea.PixelToTime(mousePosition.x);

					if (current.mousePosition.x >= blocksAreaRect.x + blocksAreaRect.width)
						selectedMark.time = maxTime;

					if (current.mousePosition.x <= blocksAreaRect.x)
						selectedMark.time = minTime;
					Repaint();
					break;
				case DragState.TimeArea:
					var translation = timeArea.translation;
					var scale = timeArea.scale;

					translation.x += current.delta.x;
					translation.y += current.delta.y;

					timeArea.SetTransform(translation, scale);
					break;
			}
		}

		if (Event.current.type == UnityEngine.EventType.KeyDown && Event.current.keyCode == KeyCode.Delete)
			RemoveMark(selectedMark);

		var hasAnythingToDraw = HasAnythingToDraw();

		if (hasAnythingToDraw)
			DrawTimeObjects();


		GUI.EndClip();

		if(hasAnythingToDraw && playTime != 0 && EditorApplication.isPlaying)
			DrawCursor(headerClipRect, playHeadPixel, styles.playheadColor);

		foreach (var mark in timelineInstance.marks)
		{
			var pixel = timeArea.TimeToPixel(mark.time);
			Color color = styles.markColors[(int) mark.type];

			if(mark == selectedMark)
				color *= 1.2f;

			var drawnRect = DrawCursor(headerClipRect, pixel, color, mark.text);
			drawnRect.x += layerInfoWidth;

			if(mark.type == TimelineMark.Type.Note)
			{
				if(selectedMark == mark && activeMarkDoubleClicked)
				{
					PopupWindow.Show(drawnRect, new EnterStringPopup(text =>
					{
						mark.text = text;
						SaveTimelineInstance();
					}, mark.text, "Note", "Ok"));
				}
			}
		}

		// This will draw the sliders on top of everything
		timeArea.EndViewGUI();

		EditorGUI.EndDisabledGroup();
	}

	void RemoveMark(TimelineMark mark)
	{
		timelineInstance.marks.Remove(mark);
		selectedMark = null;
		SaveTimelineInstance();
	}

	void ShowMarkContextMenu(float time)
	{
		GenericMenu menu = new GenericMenu();
		menu.AddItem(new GUIContent("Annotate"), false, () => { AddMark(time, TimelineMark.Type.Note); });
		menu.AddItem(new GUIContent("Create fast-forward point"), false, () => { AddMark(time, TimelineMark.Type.Fastforward); });
		menu.AddItem(new GUIContent("Create breakpoint"), false, () => { AddMark(time, TimelineMark.Type.Breakpoint); });

		if(timelineInstance.marks.Count == 0)
			menu.AddDisabledItem(new GUIContent("Clear all"));
		else
			menu.AddItem(new GUIContent("Clear all"), false, () =>
			{
				timelineInstance.marks.Clear();
				SaveTimelineInstance();
			});

		menu.ShowAsContext();
	}

	void AddMark(float time, TimelineMark.Type type, string text = "")
	{
		timelineInstance.marks.Add(new TimelineMark
		{
			time = time,
			type = type,
			text = text
		});
		SaveTimelineInstance();
	}

	void SaveTimelineInstance()
	{
		var enginePrefs = EnginePrefs.instance;
		if(!(enginePrefs.recordMode && (EditorApplication.isPlaying || EditorApplication.isPaused)))
		{
			// If we're changing the timeline when still in record mode but not playing, the user probably wants to playback.
			// So let's just switch to playback.
			enginePrefs.playbackPath = timelineInstance.path;
			enginePrefs.recordMode = false;
			EnginePrefs.Save();
			Engine.UpdateTimelineInstanceInFile(enginePrefs.playbackPath, timelineInstance);
		}
		Repaint();
	}

	Rect DrawCursor(Rect headerClipRect, float xPosition, Color color, string text = null)
	{
		GUI.BeginClip(headerClipRect);
		GUI.color = color;
		var playHeadRect = new Rect(xPosition - 5f, 0f, 15f, 15f); // todo fix width height
		GUI.Box(playHeadRect, "", styles.headerCursorStyle);
		GUI.color = Color.white;
		Color prevHandlesColor = Handles.color;
		Handles.color = color;
		var yMax = blocksAreaRect.height;
		Handles.DrawLine(new Vector3(xPosition, 19f, 0f),
			new Vector3(xPosition, blocksAreaRect.height, 0f));
		Handles.color = prevHandlesColor;

		if(!string.IsNullOrEmpty(text))
		{
			var bounds = new Rect();

			var padding = 6;
			var content = new GUIContent(text);
			Vector2 vector2 = styles.noteFontStyle.CalcSize(content);
			bounds.width = vector2.x + 2f * padding;
			bounds.height = vector2.y + 2f;

			bounds.y = yMax - bounds.height * 2f;
			bounds.x = xPosition + 1;

			GUI.color = styles.noteBackgroundColor;
			GUI.Label(bounds, GUIContent.none, styles.noteBackgroundStyle);
			GUI.color = Color.white;

			bounds.x += padding;
			bounds.width -= padding;

			GUI.Label(bounds, text, styles.noteFontStyle);
		}
		GUI.EndClip();

		return playHeadRect;
	}

	Rect GetMarkRect(TimelineMark mark)
	{
		return new Rect(timeArea.TimeToPixel(mark.time) - 5f, 0f, 15f, 15f); // todo fix width height
	}

	IEnumerable<KeyValuePair<string, TimelineLayer>> GetActiveLayers()
	{
		if(string.IsNullOrEmpty(searchQuery)) return timelineInstance.layers;

		var searchQueries = searchQuery.Split(',');
		return timelineInstance.layers.Where(x => searchQueries
			.Any(t => !string.IsNullOrEmpty(t.Trim()) && x.Value.name.ToLower().Contains(t.Trim().ToLower())));
	}

	void DrawTimeObjects()
	{
		float timeViewXMin;
		float timeViewXMax;
		GetTimeBounds(out timeViewXMin, out timeViewXMax);

		int layerIndex = 0;
		var clickedDescriptionAway = Event.current.type == UnityEngine.EventType.MouseDown;

		var translation = timeArea.translation;
		var scale = timeArea.scale;

		foreach (var timelineLayerPair in GetActiveLayers())
		{
			var timelineLayer = timelineLayerPair.Value;

			if (timelineLayer.timeObjects.Count > 0)
			{
				for(int i=0; i<timelineLayer.timeObjects.Count; i++)
				{
					var timeObject = timelineLayer.timeObjects[i];

					// Don't visualize empty logs
					if(string.IsNullOrEmpty(timeObject.text)) continue;

					float endTime;
					if(i+1 < timelineLayer.timeObjects.Count) endTime = timelineLayer.timeObjects[i+1].time; else
					if(engine == null || Engine.isPlayback)   endTime = timelineInstance.length; else
					                                          endTime = playTime;

					var inView = endTime > timeViewXMin && timeObject.time < timeViewXMax;
					if (!inView || endTime < timeObject.time + .05f)
						continue;

					var fromPixel = timeArea.TimeToPixel(timeObject.time);
					var endPixel = timeArea.TimeToPixel(endTime);

					var boxRect = new Rect
					{
						y = layerIndex * layerHeight + translation.y / scale.y,
						xMin = fromPixel,
						xMax = endPixel,
						height = 36 // Igorning scale.y as the height should always be the same
					};

					var leftBox = boxRect;
					leftBox.width -= 2;
					GUI.backgroundColor = new Color(.8f, .8f, .8f, 1f);
					GUI.Box(leftBox, "", selectedTimeObject == timeObject ? styles.blockSelectedStyle : styles.blockStyle);
					GUI.backgroundColor = Color.white;

					if (Event.current.type == UnityEngine.EventType.MouseDown &&
					    (boxRect.Contains(Event.current.mousePosition)))
					{
						if(Event.current.button == 1)
						{
							ShowMarkContextMenu(timeObject.time);
						}
						else if(Event.current.clickCount > 1)
						{
							selectedTimeObject = timeObject;
							Focus(timeObject.time, endTime);
							settings.locked = false;
						}

						selectedTimeObject = timeObject;

						clickedDescriptionAway = false;
						Event.current.Use();
					}


					if(boxRect.width > 10)
					{
						var labelRect = boxRect;
						labelRect.x += 6;
						labelRect.xMax -= 10;


						GUI.Label(labelRect, timeObject.text, styles.timeObjectLabelStyle);

						float minWidth;
						float maxWidth;
						styles.timeObjectLabelStyle.CalcMinMaxWidth(timeObject.textGUIContent, out minWidth, out maxWidth);

						var color = logColors[Mathf.Clamp((int)timeObject.color, 0, logColors.Length-1)];
						var xLineMin = Mathf.Max(0, labelRect.xMin);
						var lineLength = Mathf.Min(labelRect.xMax, maxWidth + 5);

						var xLineMax = labelRect.xMax - 5;
						Handles.color = color;

						if(lineLength> -(labelRect.xMax - 5) && xLineMin < xLineMax)
							Handles.DrawAAPolyLine(2, new Vector2(xLineMin, labelRect.yMax - 4),
								new Vector2(xLineMax, labelRect.yMax - 4));

						Handles.color = Color.white;
					}
				}

				layerIndex++;
			}
		}
		if (clickedDescriptionAway)
		{
			selectedTimeObject = null;
			Repaint();
		}
	}

	void Focus(float from, float to)
	{
		var translation = timeArea.translation;
		var scale = timeArea.scale;


		var diff = to - from;
		from -= diff * .3f;
		to += diff * .3f;

		var scaleX = blocksAreaRect.width / (to - from);
		scaleX = Mathf.Min(scaleX, maxZoomInScale);

		var translationX = -(from * scaleX);

		timeArea.SetTransform(new Vector2(translationX, translation.y), new Vector2(scaleX, scale.y));
	}
}
