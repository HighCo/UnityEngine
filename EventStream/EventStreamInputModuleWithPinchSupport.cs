using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace PuzzleCore
{
	public class EventStreamInputModuleWithPinchSupport : EventStreamInputModule
	{
#if Touch
		private PinchEventData _pinchData;

		private enum PinchMode
		{
			Idle,
			Began,
			Pinching
		}

		private PinchMode _pinchMode = PinchMode.Idle;

		private Vector2 _prevVector = Vector2.zero;
		private float _stationaryTime;

		public bool detectPinches = true;

		public float minPinchThreshold = 10;
		public float stationaryTimeThreshold = 1;
		public float mouseScrollMultiplier = -5;

		public event Action onScreenTapped;

		protected override void Start ()
		{
			_pinchData = new PinchEventData(eventSystem);
		}

		public override void ProcessInput ()
		{
			ProcessGlobalInputs();

			if (TouchInput.touches.Count == 1)
			{
				if (detectPinches && TouchInput.touches[0].scrollDelta != Vector2.zero)
				{
					ProcessScroll();
				}
				else
				{
					base.ProcessInput();
				}
			}
			else if (detectPinches && TouchInput.touches.Count == 2)
			{
				ProcessPinch();
			}
		}

		protected void ProcessScroll ()
		{
			bool pressed0, released0;
			var touch0 = TouchInput.touches[0];
			var touchData0 = GetTouchPointerEventDataFromEventStream(touch0, out pressed0, out released0);
			eventSystem.RaycastAll(touchData0, m_RaycastResultCache);
			var firstHit0 = FindFirstRaycast(m_RaycastResultCache);

			touchData0.scrollDelta = touch0.scrollDelta * mouseScrollMultiplier;
			ExecuteEvents.ExecuteHierarchy(ExecuteEvents.GetEventHandler<IScrollHandler>(firstHit0.gameObject), touchData0, ExecuteEvents.scrollHandler);
		}

		protected void ProcessPinch ()
		{
			bool pressed0, released0;
			var touch0 = TouchInput.touches[0];
			var touchData0 = GetTouchPointerEventDataFromEventStream(touch0, out pressed0, out released0);
			eventSystem.RaycastAll(touchData0, m_RaycastResultCache);
			var firstHit0 = FindFirstRaycast(m_RaycastResultCache);

			var touch1 = TouchInput.touches[1];
			bool pressed1, released1;

			var touchData1 = GetTouchPointerEventDataFromEventStream(touch1, out pressed1, out released1);

			eventSystem.RaycastAll(touchData1, m_RaycastResultCache);
			var firstHit1 = FindFirstRaycast(m_RaycastResultCache);

			if (touch0.phase == TouchPhase.Began || touch1.phase == TouchPhase.Began)
			{
				_prevVector = touch1.position - touch0.position;
				_pinchMode = PinchMode.Began;
			}

			if (touch0.phase == TouchPhase.Moved || touch1.phase == TouchPhase.Moved)
			{
				if (firstHit0.gameObject != null && firstHit1.gameObject != null)
				{
					if (DetectPinchMotion(touch0, touch1))
					{
						if (_pinchMode == PinchMode.Pinching)
						{
							_pinchData.data[0] = touchData0;
							_pinchData.data[1] = touchData1;

							if (_pinchData != null && _pinchData.IsValid)
							{
								ExecuteEvents.ExecuteHierarchy(firstHit0.gameObject, _pinchData, PinchModuleEvents.pinchHandler);
							}
						}
					}
				}
			}

			//check for ended or cancelled touched fingers and set the mode back to "idle".

			if (touch0.phase == TouchPhase.Ended || touch1.phase == TouchPhase.Ended ||
			    touch0.phase == TouchPhase.Canceled || touch1.phase == TouchPhase.Canceled)
			{

				_pinchMode = PinchMode.Idle;
				_prevVector = Vector2.zero;

				if (_pinchData != null && _pinchData.IsValid)
				{
					ExecuteEvents.ExecuteHierarchy(firstHit0.gameObject, _pinchData, PinchModuleEvents.pinchEndHanlder);
				}
			}


			//check for stationary fingers and set the mode back to "Began" if above the threshold.

			if (touch0.phase == TouchPhase.Stationary || touch1.phase == TouchPhase.Stationary)
			{
				_stationaryTime += Time.deltaTime;

				if (_stationaryTime > stationaryTimeThreshold)
				{
					_pinchMode = PinchMode.Began;
					_prevVector = touch1.position - touch0.position;
					_stationaryTime = 0;
				}
			}
		}

		bool DetectPinchMotion (TouchItem touch0, TouchItem touch1)
		{
			if (_pinchMode == PinchMode.Began)
			{
				Vector2 currentVector = touch1.position - touch0.position;

				// check for pinch threshold

				Debug.Log("Above Threshold: " + (Mathf.Abs(currentVector.magnitude - _prevVector.magnitude) > minPinchThreshold));

				if (Mathf.Abs(currentVector.magnitude - _prevVector.magnitude) > minPinchThreshold)
				{
					_pinchMode = PinchMode.Pinching;
					_prevVector = currentVector;
				}

				return false;

			}

			if (_pinchMode == PinchMode.Pinching)
			{
				Vector2 currentVector = touch1.position - touch0.position;

				_pinchData.pinchDelta = currentVector.magnitude - _prevVector.magnitude;
				_pinchData.pinchDeltaVector = currentVector - _prevVector;

				_prevVector = currentVector;

				return true;
			}

			return false;
		}

		void ProcessGlobalInputs()
		{
			var count = TouchInput.touches.Count;
			for (int i = 0; i < count; i++)
			{
				if (TouchInput.touches[i].phase == TouchPhase.Ended)
				{
					if (onScreenTapped != null)
					{
						onScreenTapped.Invoke();
						break;
					}
				}
			}
		}

	}

	public class PinchEventData : BaseEventData
	{
		public PointerEventData[] data = new PointerEventData[2];
		public float pinchDelta;
		public Vector2 pinchDeltaVector;

		public bool IsValid {
			get { return data != null && data[0] != null && data[1] != null; }
		}

		public Vector2 position {
			get { return Vector2.Lerp(data[0].position, data[1].position, 0.5f); }
		}


		public Vector2 PositionDelta {
			get { return (data[0].delta + data[1].delta) * 0.5f; }
		}

		public PinchEventData(EventSystem eventSystem) : base(eventSystem) { }
	}

	public interface IPinchHandler : IEventSystemHandler
	{
		void OnPinch (PinchEventData data);
	}

	public interface IPinchEndHandler : IEventSystemHandler
	{
		void OnPinchEnd (PinchEventData data);
	}

	public static class PinchModuleEvents
	{
		private static void Execute (IPinchHandler handler, BaseEventData eventData)
		{
			handler.OnPinch(ExecuteEvents.ValidateEventData<PinchEventData>(eventData));
		}

		private static void Execute (IPinchEndHandler handler, BaseEventData eventData)
		{
			handler.OnPinchEnd(ExecuteEvents.ValidateEventData<PinchEventData>(eventData));
		}

		public static ExecuteEvents.EventFunction<IPinchHandler> pinchHandler {
			get { return Execute; }
		}

		public static ExecuteEvents.EventFunction<IPinchEndHandler> pinchEndHanlder {
			get { return Execute; }
		}
	#endif
	}
}
