using System;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace PuzzleCore
{
	public enum Control {None, LeftStick, RightStick, DPad, DigitalLeftStickOrDPad, AnalogLeftStickOrDPad, Action1, Action2, Action3, Action4, LeftTrigger, RightTrigger, LeftBumper, RightBumper, Start, Back };
	public enum EventStreamAction {Record, Playback};
	public enum Direction {None, Left, Right, Up, Down};
	public enum LogColor {Cyan, Green, Red, Blue, Gray};
	enum EventType {Controller, Acceleration, Key, AddController, TouchBegan, TouchMoved, TouchEnded, TouchCanceled, MouseScroll, VRController, VRHead};

	[Serializable]
	public class EnginePrefs
	{
		public const string EnginePrefsKey = "EnginePrefs";

		static EnginePrefs _instance;

		public static EnginePrefs instance
		{
			get
			{
				#if UNITY_EDITOR
					var str = EditorPrefs.GetString(EnginePrefsKey);
					var obj = JsonUtility.FromJson<EnginePrefs>(str);
					return _instance ?? (_instance =  JsonUtility.FromJson<EnginePrefs>(EditorPrefs.GetString(EnginePrefsKey)) ?? new EnginePrefs());
				#else
					return null;
				#endif
			}
		}

		public string playbackPath;
		public string playbackURL;
		public float timeScale = 1f;
		public bool recordMode = true;
		public float fastforwardTo;

		public static void Save()
		{
#if UNITY_EDITOR
			EditorPrefs.SetString(EnginePrefs.EnginePrefsKey, JsonUtility.ToJson(instance));
#endif
		}
	}

	public partial class Engine : MonoBehaviour
	{
		const int formatVersion = 3;

		public static float deltaTime;
		public static double epochTime;
		public static float time;
		public static int frame = 0;
		public static Engine instance;
		public static DateTime goToBackgroundTime;
		public static bool isPlayback { get { return instance != null && instance.eventStreamAction == EventStreamAction.Playback; } }

		public static void DisableEventStreamForLoading() { instance.eventStreamEnabled = false; }
		public static void EnableEventStreamAfterLoading() { instance.eventStreamEnabled = true; }

		public bool visualizeInput;
		public string parameters;
		public float eventStreamStartedTime {get; private set;}

		[HideInInspector] public EnginePrefs enginePrefs;
		[HideInInspector] public EventStreamAction eventStreamAction;
		[HideInInspector] public Vector2 referenceScreenSize;
		[HideInInspector] public TimelineInstance timeline = new TimelineInstance();
		[HideInInspector] public EventStreamInputModule inputModule;

		public BinaryReader reader;
        public BinaryWriter writer;
		DateTime startTime;
		string startScene;
		int exceptionCount=0, warningCount=0;
		bool initialStatup = true;
		float lastTime;
		double epochTimeOffset;
		string fileName;
		int playbackFormatVersion;
		List<EventStreamInput> inputs;
		bool eventStreamEnabled = true;

		static readonly System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);

		public void Awake()
		{
			#if UNITY_EDITOR
				enginePrefs = EnginePrefs.instance;
			#endif

			if (enginePrefs == null)
				enginePrefs = new EnginePrefs();

			eventStreamAction = enginePrefs.recordMode ? EventStreamAction.Record : EventStreamAction.Playback;

			DontDestroyOnLoad(this);
			instance = this;

			inputs = new List<EventStreamInput>();

			#if Touch
				inputs.Add(new TouchInput());
			#endif

			if (inputModule == null)
				inputModule = FindObjectOfType<EventStreamInputModule>();

			#if Controller
				inputs.Add(new ControllerInput());
			#endif

			#if VR
				inputs.Add(new VRInput());
			#endif

			#if Pointer || Mouse
				inputs.Add(new PointerInput());
			#endif

			#if LocalAreaNetwork
				inputs.Add(new LocalAreaNetwork());
			#endif

			#if RelayNetwork
				inputs.Add(new RelayNetwork());
			#endif

			StartStream();
		}

		public int CreateNewStream(int parameter)
		{
			SaveStream();
			if (!initialStatup || string.IsNullOrEmpty(this.parameters))
				this.parameters = parameter.ToString();
			initialStatup = false;
			StartStream();
			return !string.IsNullOrEmpty(this.parameters) ? int.Parse(this.parameters) : 0;
		}

		public static void LogEnd(string layer) { Log(LogColor.Cyan, layer, "", ""); }
		public static void Log(string text) { Log(LogColor.Cyan, "", text, ""); }
		public static void Log(string layer, string text, string description = "", params object[] args) { Log(LogColor.Cyan, layer, text, description, args); }
		public static void Log(LogColor color, string layer, string text, string description = "", params object[] args)
		{
			if(instance == null) return;

			if(args.Length > 0) description = string.Format(description, args);

			if(instance.eventStreamAction == EventStreamAction.Record)
				instance.timeline.AddItem(color, layer, text, description);
			else
				instance.timeline.CheckForDeterminism(color, layer, text, description);
		}

		void StartStream()
		{
			startTime = System.DateTime.Now;
			if(eventStreamAction == EventStreamAction.Record)
			{
				writer=new BinaryWriter(new MemoryStream());
				var randomState = UnityRandomState;
				referenceScreenSize = new Vector2(Screen.width, Screen.height);
				time = Time.time;
				eventStreamStartedTime = time;
				epochTimeOffset = (DateTime.Now - epochStart).TotalSeconds - time;
				timeline.Clear();
				writer.Write(parameters);
				writer.Write(randomState.Length);
				writer.Write(randomState);
				writer.Write(referenceScreenSize.x);
				writer.Write(referenceScreenSize.y);
				writer.Write(time);
				writer.Write(epochTimeOffset);
			}
			else
			{
				if(!string.IsNullOrEmpty(enginePrefs.playbackPath)) reader = OpenFileStream(enginePrefs.playbackPath); else
				if(!string.IsNullOrEmpty(enginePrefs.playbackURL))  reader = OpenUrlStream(enginePrefs.playbackURL);

				timeline = new TimelineInstance(reader);
				timeline.path = enginePrefs.playbackPath;
				timeline.url = enginePrefs.playbackURL;
				parameters = reader.ReadString();
				var randomStateLength = reader.ReadInt32();
				UnityRandomState = reader.ReadBytes(randomStateLength);
				referenceScreenSize = new Vector2(reader.ReadSingle(), reader.ReadSingle());
				time = reader.ReadSingle();
				epochTimeOffset = reader.ReadDouble();

				eventStreamStartedTime = time;

				var refAspectRatio = referenceScreenSize.x / referenceScreenSize.y;
				var aspectRatio = Screen.width / (float)Screen.height;
				if (refAspectRatio - aspectRatio > 0.01f)
					Debug.LogError("Playback: Aspect ratio doesn't match with stream! Stream aspect ratio: " + AspectRationToString(refAspectRatio));
			}

			startScene = SceneManager.GetActiveScene().name;
			deltaTime = 1f / Screen.currentResolution.refreshRate;
			lastTime = time;
			epochTime = time + epochTimeOffset;
		}

		public static void UpdateTimelineInstanceInFile(string path, TimelineInstance timelineInstance)
		{
			string tempFileName;
			int tempFormatVersion;
			byte[] fileBytes;
			using (var reader = OpenFileStream(path))
			{
				reader.BaseStream.Position = 0;
				tempFormatVersion = reader.ReadInt32();
				tempFileName = reader.ReadString();

				new TimelineInstance(reader);
				var length = reader.BaseStream.Length - reader.BaseStream.Position;
				fileBytes = new byte[length];
				reader.Read(fileBytes, 0, fileBytes.Length);
			}


			using (var fileWriter = new BinaryWriter(System.IO.File.OpenWrite(path)))
			{
				fileWriter.Write(tempFormatVersion);
				fileWriter.Write(tempFileName);
				timelineInstance.Write(fileWriter);
				fileWriter.Write(fileBytes);
			}
		}

		byte[] UnityRandomState
		{
			get {
				BinaryFormatter bf = new BinaryFormatter();
				using (var ms = new MemoryStream())
				{
					bf.Serialize(ms, Random.state);
					return ms.ToArray();
				}
			}
			set {
				BinaryFormatter bf = new BinaryFormatter();
				using (var ms = new MemoryStream(value))
				{
					Random.state = (Random.State)bf.Deserialize(ms);
				}
			}
		}

		void OnApplicationQuit()
		{
			foreach(EventStreamInput input in inputs)
				input.Close();
			SaveStream();
			Injector.ClearAll();
		}

		void SaveStream()
		{
			if(writer != null)
			{
				byte[] buffer = (writer.BaseStream as MemoryStream).GetBuffer();
				var timeSpan = System.DateTime.Now - startTime;
				if (buffer.Length > 0 && timeSpan.TotalSeconds > 1.0)
				{
					var newFileName = CreateFileName();
					var path = GetPathForFile(newFileName);

					timeline.length = Engine.time;
					var fileWriter = new BinaryWriter(System.IO.File.OpenWrite(path));
					fileWriter.Write(formatVersion);
					fileWriter.Write(newFileName);
					timeline.Write(fileWriter);
					fileWriter.Write(buffer);
					fileWriter.Close();

					timeline.path = path;
				}
			}
			time = 0;
			deltaTime = 0;
			lastTime = 0;
		}

		string CreateFileName()
		{
			var timeSpan = System.DateTime.Now - startTime;

			return string.Format("{0:yyyy.MM.dd HH.mm} ({4}.{5:d2}) {6} {7} {2}{3}V{1}.eventstream",
				startTime, Application.version,
				exceptionCount > 0 ? exceptionCount + " Exceptions " : "", warningCount > 0 ? warningCount + " Warnings " : "",
				(int) timeSpan.TotalMinutes, (int) timeSpan.Seconds, startScene, parameters);
		}

		static string GetPathForFile(string fileName)
		{
			return string.Format("{0}/{1}", Application.persistentDataPath, fileName);
		}

		public static BinaryReader OpenFileStream(string filename)
		{
			var bytes = System.IO.File.ReadAllBytes(filename);
			var reader = CreateReaderFromBytes(bytes);
			ReadFormatVersion(reader);
			ReadFileName(reader);
			return reader;
		}

		public static BinaryReader OpenUrlStream(string url)
		{
			string bytesString;
			System.Net.ServicePointManager.ServerCertificateValidationCallback = (xsender, xcertificate, xchain, xerrors) => true;
			using (var client = new System.Net.WebClient())
			{
				bytesString = client.DownloadString(url);
			}
			var bytes = Convert.FromBase64String(bytesString);
			var reader = CreateReaderFromBytes(bytes);

			ReadFormatVersion(reader);
			var fileName = ReadFileName(reader);
			var path = string.Format("{0}/{1}", Application.persistentDataPath, fileName);
			EnginePrefs.instance.playbackURL = "";
			EnginePrefs.instance.playbackPath = path;

			using (var writer = new BinaryWriter(System.IO.File.OpenWrite(path)))
				writer.Write(bytes);

			return reader;
		}

		static BinaryReader CreateReaderFromBytes(byte[] buffer)
		{
			var reader = new BinaryReader(new MemoryStream(buffer));
			return reader;
		}

		static string ReadFileName(BinaryReader reader)
		{
			var fileName = reader.ReadString();
			if (instance != null)
				instance.fileName = fileName;
			return fileName;
		}

		static int ReadFormatVersion(BinaryReader reader)
		{
			var formatVersion = reader.ReadInt32();
			if (instance != null)
				instance.playbackFormatVersion = formatVersion;
			return formatVersion;
		}

		public void Update()
		{
			if(!eventStreamEnabled) return;

			#if UNITY_EDITOR
				var activeMark = timeline.marks.Where(mark => mark.type != TimelineMark.Type.Note && time < mark.time).OrderBy(mark => mark.time).FirstOrDefault();
			#endif

			// Record
			foreach(var inputMethod in this.inputs)
				inputMethod.Clear();
			if (writer != null)
			{
				RecordInput(writer);
				RecordTime(writer);
				time = Time.time;
				epochTime = (DateTime.Now - epochStart).TotalSeconds;
			}
			do
			{
				// Playback
				if (reader != null)
					Playback(reader);

				// Process input
				if (inputModule) inputModule.ProcessInput();

				// Update
				frame++;
				deltaTime = time - lastTime;
				lastTime = time;

				#if UNITY_EDITOR
				// We're about to pass the frame, so break.
				if(activeMark != null && activeMark.type == TimelineMark.Type.Breakpoint && time + deltaTime >= activeMark.time)
				{
					Assert.IsTrue(Time.timeScale == 1, string.Format("Stopped at breakpoint with a time scale of {0}, ideally it is 1", Time.timeScale));
					Debug.Break();
				}
				#endif

				UpdateManager.Update();
			} while(!enginePrefs.recordMode && reader != null && time < (Time.time - eventStreamStartedTime));

			// Time scale
			#if UNITY_EDITOR
			if(activeMark != null && time < activeMark.time)
			{
				var estimatedFps = 60;
				var frameTime = 1000/estimatedFps;

				var distance = activeMark.time - Time.time;

				var idealFastForwardSpeed = distance/frameTime * estimatedFps / 2;

				Time.timeScale = Mathf.Max(idealFastForwardSpeed, 1);
				//const float fastForwardSpeed = 30;
				//if(Time.timeScale == 1)
				//	Time.timeScale = fastForwardSpeed;
				//var diff = activeMark.time - time;
//
				//// Getting close to the fast-foward point (with the fast-forward speed we would pass in a couple frames), so slow down.
				//if(diff < fastForwardSpeed/10)
				//	Time.timeScale = Mathf.Max(1, diff);
			}
			else
				Time.timeScale = enginePrefs.timeScale;
			#endif
		}

		void RecordInput(BinaryWriter stream)
		{
			foreach(var inputMethod in this.inputs)
				inputMethod.Record(stream);
		}

		// Persist time and epochTime indepandantly, because we want epochTime to continue to run when the game is in the background, but time shouldn't
		void RecordTime(BinaryWriter stream)
		{
			time = Time.time;
			stream.Write(time);
			stream.Write(epochTime);
		}

		void Playback(BinaryReader stream)
		{
			float value;
			try
			{
				while((value = stream.ReadSingle()) == float.MinValue)
				{
					byte typeByte = stream.ReadByte();
					EventType type = (EventType)(typeByte & 0xf);
					int index = typeByte >> 4;

					foreach(EventStreamInput inputMethod in this.inputs)
						inputMethod.Playback(type, index, stream);
				}
				time = value;
				if(playbackFormatVersion >= 3) epochTime = stream.ReadDouble();
			}
			catch(EndOfStreamException)
			{
				Debug.Log("End of event stream");
				reader = null;
			}
		}

		string AspectRationToString(float ratio)
		{
			float epsilon = 0.001f;

			if (ratio - 16f / 9f < epsilon)  return "16:9";
			if (ratio - 9f / 16f < epsilon)  return "9:16";
			if (ratio - 16f / 10f < epsilon) return "16:10";
			if (ratio - 10f / 16f < epsilon) return "10:16";
			if (ratio - 4f / 3f < epsilon)   return "4:3";
			if (ratio - 3f / 4f < epsilon)   return "3:4";

			return string.Format("{0:00}:1", ratio);
		}
	}
}
