﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace PuzzleCore
{
	[Serializable]
	public class TimelineInstance : ISerializationCallbackReceiver
	{
		public Dictionary<string, TimelineLayer> layers = new Dictionary<string, TimelineLayer>();
		public List<TimelineMark> marks = new List<TimelineMark>();
		public float length;
		public string timestamp;
		public string path;
		public string url;
		public long byteSizeLastTimeRead;

		[SerializeField] List<TimelineLayer> layersSerialized;

		[NonSerialized] public List<string> nonDeterminismWarnings = new List<string>();

		public TimelineInstance() { }
		public TimelineInstance(BinaryReader reader)
		{
			var startPosition = reader.BaseStream.Position;

			timestamp = reader.ReadString();
			length = reader.ReadSingle();
			var layersCount = reader.ReadInt32();
			for(int i=0; i<layersCount; i++)
			{
				var layer = new TimelineLayer(reader);
				layers[layer.name] = layer;
			}

			var marksCount = reader.ReadInt32();
			for (var i = 0; i < marksCount; i++)
				marks.Add(new TimelineMark(reader));

			byteSizeLastTimeRead = reader.BaseStream.Position - startPosition;

			nonDeterminismWarnings.Clear();
		}

		public void Write(BinaryWriter writer)
		{
			writer.Write(DateTime.Now.ToString());
			writer.Write(length);
			writer.Write(layers.Count);

			foreach(var layer in layers.Values)
				layer.Write(writer);

			writer.Write(marks.Count);
			foreach(var mark in marks)
				mark.Write(writer);
		}

		public void AddItem(LogColor color, string layerName, string text, string description)
		{
			TimelineLayer layer;
			if(!layers.TryGetValue(layerName, out layer))
				layers[layerName] = layer = new TimelineLayer{ name = layerName };

			layer.timeObjects.Add(new TimeObject{ text = text, description = description, time = Engine.time, color = color });
		}

		public void Clear()
		{
			layers.Clear();
			length = 0;
		}

		public void OnBeforeSerialize()
		{
			layersSerialized = layers.Values.ToList();
		}

		public void OnAfterDeserialize()
		{
			layers = layersSerialized.ToDictionary(x => x.name);
			layersSerialized.Clear();
		}

		public void CheckForDeterminism(LogColor color, string layer, string text, string description)
		{
			if (!layers.ContainsKey(layer) || !layers[layer].timeObjects.Exists(x => x.description == description && x.text == text && Math.Abs(x.time - Engine.time) < .01f))
				nonDeterminismWarnings.Add("Log mismatch for " + text + " at " + Engine.time);
		}
	}


	[Serializable]
	public class TimelineLayer
	{
		public string name;
		public List<TimeObject> timeObjects = new List<TimeObject>();

		public TimelineLayer() { }
		public TimelineLayer(BinaryReader reader)
		{
			name = reader.ReadString();
			var count = reader.ReadInt32();
			for(int i=0; i<count; i++)
				timeObjects.Add(new TimeObject(reader));
		}

		public void Write(BinaryWriter writer)
		{
			writer.Write(name);
			writer.Write(timeObjects.Count);
			foreach(var item in timeObjects)
				item.Write(writer);
		}
	}

	[Serializable]
	public class TimeObject
	{
		public float time;
		public string text;
		public LogColor color;
		public string description = "";

		GUIContent _textGUIContent;
		public GUIContent textGUIContent
		{
			get
			{
				if(_textGUIContent == null) _textGUIContent = new GUIContent(text);
				return _textGUIContent;
			}
		}

		public TimeObject() { }
		public TimeObject(BinaryReader reader)
		{
			time = reader.ReadSingle();
			text = reader.ReadString();
			color = (LogColor) reader.ReadInt32();
			description = reader.ReadString();
		}

		public void Write(BinaryWriter writer)
		{
			writer.Write(time);
			writer.Write(text);
			writer.Write((int)color);
			writer.Write(description);
		}
	}


	[Serializable]
	public class TimelineMark
	{
		public enum Type { Fastforward, Breakpoint, Note}

		public Type type;
		public float time;
		public string text = "";

		public TimelineMark() { }
		public TimelineMark(BinaryReader reader)
		{
			type = (Type) reader.ReadInt32();
			time = reader.ReadSingle();
			text = reader.ReadString();
		}

		public void Write(BinaryWriter writer)
		{
			writer.Write((int) type);
			writer.Write(time);
			writer.Write(text);
		}
	}
}