using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

namespace PuzzleCore
{
	public class StandardInjector
	{
		protected readonly Type injectType = typeof(Inject);
		protected Dictionary<Type, object> objects = new Dictionary<Type, object>();
		protected Dictionary<Type, UnityEngine.Object> prefabs = new Dictionary<Type, UnityEngine.Object>();
		protected HashSet<Type> globalTypes = new HashSet<Type>();
		protected Dictionary<Type, Type> bindings = new Dictionary<Type, Type>();
		protected HashSet<Type> registerAsMonobehaviours;
		protected HashSet<Type> eventsInjections = new HashSet<Type>();

		public static bool useInjectableTag = true;



		public StandardInjector()
		{
			registerAsMonobehaviours = new HashSet<Type>();
			var types = Assembly.GetExecutingAssembly().GetTypes();
			foreach (var t in types)
			{
				if (t.IsSubclassOf(typeof(Behaviour)))
				{
					var registerAs = Attribute.GetCustomAttribute(t, typeof(RegisterAs), inherit: false) as RegisterAs;
					if (registerAs != null)
					{
						registerAsMonobehaviours.Add(registerAs.type);
					}
				}
			}
		}

		public void Bind(Type key, Type value)
		{
			bindings[key] = value;
		}

		protected void FillFieldsList(Type t, List<FieldInfo> fields)
		{
			if (t == null) return;

			BindingFlags flags = BindingFlags.Public
			                     | BindingFlags.NonPublic
			                     | BindingFlags.Instance
			                     | BindingFlags.DeclaredOnly;

			fields.AddRange(t.GetFields(flags));
			FillFieldsList(t.BaseType, fields);
		}

		public virtual void Inject(object obj)
		{
			var type = obj.GetType();
			var fields = new List<FieldInfo>();
			FillFieldsList(type, fields);
			InjectWithFields(obj, fields);
		}

		protected void InjectWithFields(object obj, List<FieldInfo> fields)
		{
			var type = obj.GetType();

			var registerAs = Attribute.GetCustomAttribute(type, typeof(RegisterAs), inherit: false) as RegisterAs;
			if (registerAs != null)
			{
				Assert.IsTrue(registerAs.type.IsAssignableFrom(type),
				              string.Format("RegisterAsType Error: {1} cannot be cast to {0}", registerAs.type, type));
				objects[registerAs.type] = obj;
			}
			objects[type] = obj;
			foreach (var field in fields)
				if (field.IsDefined(injectType, inherit:false))
					field.SetValue(obj, Get(field.FieldType));
		}

		public bool IsFeatureInEventsClass<T>()
		{
			return eventsInjections.Contains(typeof(T));
		}

		public object Get(Type type)
		{
			object obj = null;

			Type targetType;
			if(bindings.TryGetValue(type, out targetType))
				type = targetType;

			if(!objects.TryGetValue(type, out obj))
			{
				if (type.IsSubclassOf(typeof(Behaviour)) || registerAsMonobehaviours.Contains(type))
				{
					var roots = GetRootGameObjects();
					foreach(var root in roots)
					{
						obj = root.GetComponentInChildren(type, includeInactive:true);
						if(obj != null) break;
						if (obj == null && !useInjectableTag)
						{
							//useInjectableTag == false means we only got all root gameobjects that are _not_ marked as DontDestroyOnLoad
							//therefore we do a slow find object of type to get results from everywhere now.
							obj = GameObject.FindObjectOfType(type);
						}
					}
				}
#if UNITY_EDITOR
				else if (type.IsSubclassOf(typeof(ScriptableObject)))
				{
					var resourcesObjects = Resources.FindObjectsOfTypeAll(type);
					if(resourcesObjects.Length > 0)
						obj = resourcesObjects[0];
					else
						obj = ScriptableObject.CreateInstance(type);
				}
#endif
				else
					obj = Activator.CreateInstance(type);

				if(obj != null) Set(type, obj);
			}

			return obj;
		}

		public IEnumerable<T> GetAll<T>()
		{
			foreach (var objKvp in objects)
			{
				if (objKvp.Value is T) yield return (T) objKvp.Value;
			}
		}

		IEnumerable<GameObject> GetRootGameObjects ()
		{
			return useInjectableTag ? GetGameObjectsWithInjectableTag() : GetAllRootGameObjects();
		}

		IEnumerable<GameObject> GetGameObjectsWithInjectableTag ()
		{
			return GameObject.FindGameObjectsWithTag("Injectable");
		}

		IEnumerable<GameObject> GetAllRootGameObjects ()
		{
			for (int i = 0; i < SceneManager.sceneCount; i++)
			{
				var scene = SceneManager.GetSceneAt(i);
				if (scene.IsValid())
				{
					var roots = scene.GetRootGameObjects();
					for (int j = 0; j < roots.Length; j++)
					{
						yield return roots[i];
					}
				}
			}
		}

		public void ClearLocals()
		{
			foreach(var key in new List<Type>(objects.Keys))
			{
				if (!globalTypes.Contains(key))
				{
					RemoveObject(objects[key]);
					objects.Remove(key);
				}
			}
		}

		public void ClearAll()
		{
			foreach(var key in new List<Type>(objects.Keys))
			{
				RemoveObject(objects[key]);
				objects.Remove(key);
			}
		}

		public void Remove(Type type)
		{
			RemoveObject(objects[type]);
			objects.Remove(type);
			globalTypes.Remove(type);
		}

		void RemoveObject(object obj)
		{
			var updateable = obj as IUpdatable;
			if (updateable != null) UpdateManager.RemoveUpdatable(updateable);
		}

		public object Set(Type type, object value)
		{
			var globalAttribute = type.GetCustomAttributes(typeof(Global), inherit: false);
			if(globalAttribute.Length != 0) globalTypes.Add(type);
			objects[type] = value;
			return value;
		}

		public bool Has(Type type)
		{
			return objects.ContainsKey(type);
		}

		public void AssertFeaturesRequirements()
		{
			foreach (var type in objects.Keys)
			{
				var requires = Attribute.GetCustomAttribute(type, typeof(RequiresFeatures), inherit: false) as RequiresFeatures;
				if (requires != null)
				{
					foreach (var requirement in requires.requirements)
					{
						Assert.IsTrue(objects.ContainsKey(requirement), string.Format("Feature '{0}' requires feature '{1}' to work properly", type, requirement));
					}
				}
			}
		}
	}
}
