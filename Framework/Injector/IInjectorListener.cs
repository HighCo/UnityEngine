﻿namespace PuzzleCore
{
    public interface IInjectorListener
    {
        void RemovedFromInjector();
    }
}