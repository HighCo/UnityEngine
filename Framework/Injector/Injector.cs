﻿using System;
using System.Collections.Generic;
using UnityEngine;


namespace PuzzleCore
{
	public partial class Injector
	{
		private const string NAMESPACE = "PuzzleCore.";
		
		public static StandardInjector instance = new StandardInjector();

		public static void Inject(object obj)
		{
			instance.Inject(obj);
		}

		public static void Bind<Key, Value>()
		{
			instance.Bind(typeof(Key), typeof(Value));
		}

		public static T Get<T>()
		{
			return (T)instance.Get(typeof(T));
		}

		public static object Get(Type type)
		{
			return instance.Get(type);
		}
		
		public static IEnumerable<T> GetAll<T>()
		{
			return instance.GetAll<T>();
		}

		public static T Create<T>()
		{
			return (T)instance.Get(typeof(T));
		}

		public static T Set<T>(T value)
		{
			var type = typeof(T);
			return (T)instance.Set(type, value);
		}

		public static bool Has<T>()
		{
			return instance.Has(typeof(T));
		}

		public static bool Has(string typeName, out Type type)
		{
			var assembly = typeof(Injector).Assembly;
			type = assembly.GetType(NAMESPACE + typeName);
			return instance.Has(type);
		}
		
		public static bool EventsHas<T>() where T : Feature
		{
			return instance.IsFeatureInEventsClass<T>();
		}

		public static void Remove<T>()
		{
			instance.Remove(typeof(T));
		}

		public static void ClearLocals()
		{
			instance.ClearLocals();
		}

		public static void ClearAll()
		{
			instance.ClearAll();
		}

		public static void AssertFeaturesRequirements()
		{
			instance.AssertFeaturesRequirements();
		}
	}

	public interface IUpdatable
	{
		void Update();
	}

	public interface IPausable
	{
		void Pause(bool isPaused);
	}

	public class Inject : Attribute
	{
	}

	public class Global : Attribute
	{
	}

	[AttributeUsage(AttributeTargets.Class)]
	public class RegisterAs : Attribute
	{
		public Type type;

		public RegisterAs (Type type)
		{
			this.type = type;
		}
	}

	[AttributeUsage(AttributeTargets.Class, Inherited = false)]
	public class RequiresFeatures : Attribute
	{
		public Type[] requirements;

		public RequiresFeatures (params Type[] types)
		{
			this.requirements = types;
		}
	}
}