﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace PuzzleCore
{
	public class TimeslicedInjector : StandardInjector
	{
		HashSet<Type> collection = new HashSet<Type>();
		static bool isCollecting = false;
		bool isInjecting = false;

		public static void SetCollectionEnabled(bool enabled)
		{
			isCollecting = enabled;
		}

		public override void Inject(object obj)
		{
			var type = obj.GetType();

			if (isInjecting && collection.Contains(type))
				return;

			if (isCollecting)
			{
				collection.Add(type);
				return;
			}
			base.Inject(obj);
		}

		public IPromise InjectCollection()
		{
			return TimeSlicedInjectCollection().RunCoroutine();
		}

		IEnumerator TimeSlicedInjectCollection()
		{
			var tillTime = Time.realtimeSinceStartup + .005f;

			var routine = DoInjectCollection();

			while (routine.MoveNext())
			{
				if (Time.realtimeSinceStartup > tillTime)
				{
					yield return null;
					tillTime = Time.realtimeSinceStartup + .005f;
				}
			}
		}


		IEnumerator DoInjectCollection()
		{
			isInjecting = true;
			isCollecting = false;

			Queue<Type> typeQueue = new Queue<Type>();

			foreach (var type in collection)
				typeQueue.Enqueue(type);

			var allFields = new List<FieldInfo>();
			var validFields = new List<FieldInfo>();

			while (typeQueue.Count != 0)
			{
				var currentType = typeQueue.Dequeue();

				allFields.Clear();
				FillFieldsList(currentType, allFields);

				var obj = Get(currentType);
				validFields.Clear();

				foreach (var field in allFields)
				{
					if (!field.IsDefined(injectType, false)) continue;

					validFields.Add(field);
					var fieldType = field.FieldType;

					var exists = objects.ContainsKey(fieldType);

					var val = Get(fieldType);

					if (!exists)
						yield return null;

					if (val == null) continue; //todo is null with level editor related things

					if (!collection.Contains(fieldType))
					{
						typeQueue.Enqueue(fieldType);
						collection.Add(fieldType);
					}
				}

				InjectWithFields(obj, validFields);
				yield return null;
			}

			isInjecting = false;
		}
	}
}