using UnityEngine;

namespace PuzzleCore
{
	public class View : MonoBehaviour
	{
		protected virtual void Awake()
		{
			Injector.Inject(this);
		}
	}

	public class Service : View
	{
	}
}