using Library;
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public static class Injector
{
	static Dictionary<Type, object> objects = new Dictionary<Type, object>();
	static Dictionary<Type, UnityEngine.Object> prefabs = new Dictionary<Type, UnityEngine.Object>();
	static HashSet<Type> globalTypes = new HashSet<Type>();

	public static void Inject(object obj)
	{
		objects[obj.GetType()] = obj;

		var fields = obj.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy);
		foreach(var field in fields)
		{
			var attr = field.GetCustomAttributes(typeof(Inject), inherit:false);
			if(attr.Length > 0)
			{
				var value = Injector.Get(field.FieldType);
				field.SetValue(obj, value);
			}
		}
	}

	public static object Get(Type type)
	{
		object obj = null;

		if(!objects.TryGetValue(type, out obj))
		{
			if (type.IsSubclassOf(typeof(MonoBehaviour)))
			{
				var roots = GameObject.FindGameObjectsWithTag("Injectable");
				foreach(var root in roots)
				{
					obj = root.GetComponentInChildren(type, includeInactive:true);
					if(obj != null) break;
				}
			}
			else
			{
				obj = Activator.CreateInstance(type);
			}

			if(obj != null) Set(type, obj);
		}

		return obj;
	}

	public static T Get<T>()
	{
		return (T)Get(typeof(T));
	}

	public static T Create<T>()
	{
		return (T)Get(typeof(T));
	}

	public static T Set<T>(T value)
	{
		return (T)Set(typeof(T), value);
	}

	static object Set(Type type, object value)
	{
		var globalAttribute = type.GetCustomAttributes(typeof(Library.Global), inherit: false);
		if(globalAttribute.Length != 0) globalTypes.Add(type);
		objects[type] = value;
		return value;
	}

	public static bool Has<T>()
	{
		Type type = typeof(T);
		return objects.ContainsKey(type);
	}

	public static void ClearView()
	{
		foreach(var obj in GameObject.FindObjectsOfType<GameObject>())
		{
			if (!obj.CompareTag("Static") && !obj.CompareTag("MainCamera"))
				GameObject.DestroyImmediate(obj);
		}
	}

	public static void ClearAll()
	{
		Engine.updatables.Clear();
		objects.Clear();
		prefabs.Clear();
		globalTypes.Clear();
	}

	public static void ClearLocals()
	{
		foreach(var key in new List<Type>(objects.Keys))
			if(!globalTypes.Contains(key))
			{
				var updateable = objects[key] as IUpdatable;
				if(updateable != null) Engine.updatables.Remove(updateable);
				objects.Remove(key);
			}
	}

    public static void Remove<T>()
    {
        Type type = typeof(T);
        objects.Remove(type);
        globalTypes.Remove(type);
    }
}

namespace Library
{
	public interface IUpdatable
	{
		void Update();
	}

	public class Inject : Attribute
	{
	}

	public class Global : Attribute
	{
	}
}
