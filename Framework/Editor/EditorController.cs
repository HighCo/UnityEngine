﻿using UnityEngine;

namespace PuzzleCore
{
	public class EditorController : PersistentEditorObject
	{
	}

	public class PersistentEditorObject : ScriptableObject
	{
		protected virtual void Awake()
		{
			hideFlags = HideFlags.DontSave;
			Injector.Inject(this);
		}
	}
}