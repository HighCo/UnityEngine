
namespace PuzzleCore
{
	public class Controller
	{
		public Controller()
		{
			var updatable = this as IUpdatable;
			if(updatable != null) UpdateManager.AddUpdatable(updatable);
			Injector.Inject(this);
		}
	}

    public class TestFixture : Controller
    {
    }
}
