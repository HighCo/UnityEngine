using System;
using UnityEngine.Events;

namespace PuzzleCore
{
	public class Feature : Controller
	{
	}

	[Serializable]
	public struct DynamicFeatureGroup
	{
		public string name;
		public string[] options;
		public UnityAction<int> setValue;
		public Func<int> getValue;
	}

	[AttributeUsage(AttributeTargets.Method)]
	public class PriorityAttribute : Attribute
	{
		public int value;

		public PriorityAttribute(int value)
		{
			this.value = value;
		}
	}

	[AttributeUsage(AttributeTargets.Method)]
	public class DefaultAttribute : Attribute
	{
		public object value;

		public DefaultAttribute(object value)
		{
			this.value = value;
		}
	}
}