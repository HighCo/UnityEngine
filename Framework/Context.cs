using UnityEngine;

namespace PuzzleCore
{
	public class Context : MonoBehaviour
	{
		void OnDestroy()
		{
			Injector.ClearLocals();
		}
	}
}