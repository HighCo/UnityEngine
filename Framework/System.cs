namespace PuzzleCore
{
	public abstract class EntitySystem : IUpdatable
	{
		public EntitySystem()
		{
			UpdateManager.AddUpdatable(this);
			Injector.Inject(this);
		}

		public abstract void Update();
	}
}
